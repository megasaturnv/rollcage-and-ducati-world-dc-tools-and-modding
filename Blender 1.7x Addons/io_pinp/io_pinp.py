# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#  Original code: orgyia @ http://forum.recaged.net 2014
#  Pinp importer: orgyia @ http://forum.recaged.net 2015 02 07
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "PINP",
    "author": "orgyia",
    "version": (1, 0, 0),
    "blender": (2, 72, 0),
    "location": "3D View > Properties > PINP",
    "description": "Import pinp files.",
    "wiki_url": "",
    "category": "Import-Export"
}

# ----------------------------------------------------------------
# Header

import bpy
import os
import struct

from bpy_extras.object_utils import AddObjectHelper, object_data_add
from bpy.props import FloatVectorProperty
from bpy.types import Operator, Panel
from mathutils import Vector

# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Operator

class OT_IMPORT_PINP(Operator):
    bl_idname = "scene.import_pinp"
    bl_label = "Import PINP"
    bl_description = "Import PINP File"
    bl_options = {'REGISTER', 'UNDO'}

    PINP_file = bpy.props.StringProperty(default="FILENAME")

    def execute(self, context):

        # ----------------------------------------------------------------
        # Read file, may be bug with other oses.

        dir = os.path.dirname(bpy.data.filepath)

        f = open(dir+'\\'+self.PINP_file, 'rb')
        data = f.read()
        f.close()

        # ----------------------------------------------------------------
        # PINP check

        pinp_ok = 0

        if data[0] == 80 and data[1] == 73 and data[2] == 78 and data[3] == 80:
            pinp_ok = 1
        else:
            pinp_ok = 0
            self.report({"Error"},"No PINP ID at address 0.")

        # ----------------------------------------------------------------
        # Broom Broom

        if pinp_ok:

            PEN_unknown = int.from_bytes((data[4], data[5]), byteorder='little', signed=False)
            POA_pallet = int.from_bytes((data[6], data[7]), byteorder='little', signed=False)
            POA_model = int.from_bytes((data[8], data[9]), byteorder='little', signed=False)
            POA_uvs = int.from_bytes((data[10], data[11]), byteorder='little', signed=False)
            PEN_uvs = int.from_bytes((data[12], data[13]), byteorder='little', signed=False)
            PSZ_model = int.from_bytes((data[14], data[15]), byteorder='little', signed=False)
            POA_texture = POA_pallet+768

            # ----------------------------------------------------------------
            # Texture

            texture_name = context.scene.pinp_filename
            PNG = bpy.data.images.new(texture_name+'.PNG',128,160,True,False)
            PNG_data = []

            t=0
            c=0
            while(t < (128*160)):
                c = data[POA_texture+t]
                r = data[POA_pallet+(c*3)]
                g = data[POA_pallet+(c*3)+1]
                b = data[POA_pallet+(c*3)+2]
                PNG_data.append(b/255)
                PNG_data.append(g/255)
                PNG_data.append(r/255)
                PNG_data.append(1.0)
                t+=1
            PNG.pixels=PNG_data
            PNG.pack(as_png=True)

            # ----------------------------------------------------------------
            # Model A

            MAEN_verts = int.from_bytes((data[POA_model+4], data[POA_model+5]), byteorder='little', signed=False)
            MAEN_faces = int.from_bytes((data[POA_model+6], data[POA_model+7]), byteorder='little', signed=False)
            MAOA_faces = int.from_bytes((data[POA_model+12], data[POA_model+13], data[POA_model+14], data[POA_model+15]), byteorder='little', signed=False)
            MAOA_verts = int.from_bytes((data[POA_model+64], data[POA_model+65], data[POA_model+66], data[POA_model+67]), byteorder='little', signed=False)

            bverts = []
            bfaces = []
            bedges = []

            # Faces

            for f in range(MAEN_faces):
                if data[POA_model+MAOA_faces+(f*8)+3] == 0:
                    bfaces.append([data[POA_model+MAOA_faces+(f*8)+2]-1, data[POA_model+MAOA_faces+(f*8)+0]-1, data[POA_model+MAOA_faces+(f*8)+1]-1])
                else:
                    bfaces.append([data[POA_model+MAOA_faces+(f*8)+2]-1, data[POA_model+MAOA_faces+(f*8)+0]-1, data[POA_model+MAOA_faces+(f*8)+1]-1, data[POA_model+MAOA_faces+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(MAEN_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=context.scene.pinp_filename)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            # ----------------------------------------------------------------
            # UV A

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            MAOA_uv_list = int.from_bytes((data[POA_model+20], data[POA_model+21]), byteorder='little', signed=False)

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[POA_model+MAOA_uv_list+uvid+0], data[POA_model+MAOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[POA_model+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[POA_model+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[POA_model+MAOA_uv_list+uvid+0], data[POA_model+MAOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(MAEN_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # ----------------------------------------------------------------
            # Model B

            MAOA_modelb = POA_model + int.from_bytes((data[POA_model+28], data[POA_model+29]), byteorder='little', signed=False)

            MBEN_verts = int.from_bytes((data[MAOA_modelb+4], data[MAOA_modelb+5]), byteorder='little', signed=False)
            MBEN_faces = int.from_bytes((data[MAOA_modelb+6], data[MAOA_modelb+7]), byteorder='little', signed=False)
            MBOA_faces = int.from_bytes((data[MAOA_modelb+12], data[MAOA_modelb+13], data[MAOA_modelb+14], data[MAOA_modelb+15]), byteorder='little', signed=False)

            bverts = []
            bfaces = []
            bedges = []

            # Faces

            for f in range(MBEN_faces):
                if data[MAOA_modelb+MBOA_faces+(f*8)+3] == 0:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1])
                else:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1, data[MAOA_modelb+MBOA_faces+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(MBEN_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=context.scene.pinp_filename)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            context.active_object.location[2] += 40

            # ----------------------------------------------------------------
            # UV B

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            MBOA_uv_list = int.from_bytes((data[MAOA_modelb+20], data[MAOA_modelb+21]), byteorder='little', signed=False)

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[POA_model+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[POA_model+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(MBEN_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # ----------------------------------------------------------------
            # Model C
            # changed MAOA_modelb offset (should be modelc)

            MAOA_modelb = POA_model + int.from_bytes((data[POA_model+30], data[POA_model+31]), byteorder='little', signed=False)

            MBEN_verts = int.from_bytes((data[MAOA_modelb+4], data[MAOA_modelb+5]), byteorder='little', signed=False)
            MBEN_faces = int.from_bytes((data[MAOA_modelb+6], data[MAOA_modelb+7]), byteorder='little', signed=False)
            MBOA_faces = int.from_bytes((data[MAOA_modelb+12], data[MAOA_modelb+13], data[MAOA_modelb+14], data[MAOA_modelb+15]), byteorder='little', signed=False)

            bverts = []
            bfaces = []
            bedges = []

            # Faces

            for f in range(MBEN_faces):
                if data[MAOA_modelb+MBOA_faces+(f*8)+3] == 0:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1])
                else:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1, data[MAOA_modelb+MBOA_faces+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(MBEN_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=context.scene.pinp_filename)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            context.active_object.location[2] += 80

            # ----------------------------------------------------------------
            # UV C

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            MBOA_uv_list = int.from_bytes((data[MAOA_modelb+20], data[MAOA_modelb+21]), byteorder='little', signed=False)

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[POA_model+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[POA_model+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(MBEN_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # ----------------------------------------------------------------

        return {'FINISHED'}

# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Operator

class OT_EXPORT_PINP(Operator):
    bl_idname = "scene.export_pinp"
    bl_label = "Export PINP"
    bl_description = "Export PINP File"
    bl_options = {'REGISTER', 'UNDO'}

    PINP_file = bpy.props.StringProperty(default="FILENAME")

    def execute(self, context):

        me = context.active_object

        # ----------------------------------------------------------------
        # PINP Header

        PEN_unknown = 40960
        POA_pallet = 16
        POA_model = 21264
        POA_uvs = 0             # calculate after mesh
        PEN_uvs = 0             # calculate after mesh
        PSZ_model = 0           # calculate after mesh

        # ----------------------------------------------------------------
        # Calculate Mesh A

        # MODL
        MA_EN_verts = 0
        MA_EN_faces = 0

        MA_version = 1
        MA_OA_faces = 80
        MA_OA_xx1 = 0
        MA_OA_uv_list = 0
        MA_OA_xx2 = 0

        MA_OA_lod1 = 0
        MA_OA_lod2 = 0

        MA_SZ_32 = 0
        MA_SZ_16 = 0

        MA_xa = 0
        MA_xb = 0
        MA_xc = 0
        MA_xd = 0
        MA_xe = 0
        MA_xf = 0

        MA_xg = 0
        MA_xh = 0
        MA_xi = 0
        MA_xj = 0
        MA_xk = 0
        MA_xl = 0
        MA_xm = 0
        MA_xn = 0

        MA_OA_verts = 0
        MA_OA_xx3 = 0
        MA_unknown = 0
        MA_OA_points = 0

        # --------

        for v in me.data.vertices:
            MA_EN_verts+=1

        for p in me.data.polygons:
            MA_EN_faces+=1

        # --------

        # Export PINP Header
        # Export Pallet
        # Export Texture
        # Calculate MODL Header
        # Export MODL Header

        # ----------------------------------------------------------------
        # Write File

        f = open('test_'+str(self.PINP_file)+'.txt', 'wb')

        # 80 and data[1] == 73 and data[2] == 78 and data[3] == 80:

        f.write(struct.pack("B", 80))   # PINP
        f.write(struct.pack("B", 73))
        f.write(struct.pack("B", 78))
        f.write(struct.pack("B", 80))
        f.write(struct.pack("H", PEN_unknown))		# done
        f.write(struct.pack("H", POA_pallet))		# done
        f.write(struct.pack("H", POA_model))		# done

        # cant write next set of data till model data is calculated
        # How to export LODS
        #   The lods use the same vertex and uv id numbers
        #   It may be easier to have a 'face list'
        #     The user would select the desired faces to use for the LOD
        #   The faces require uv textures aswell.
        #     Maybe a duplicate mesh would be a better option
        #     The user could delete then make new faces.
        #     The old verts and faces should retain the previous order.
        #     The New faces would require uv entries, like how it works.
        #   For this to work (import-export):
        #     The importer should import all verts for each model.
        #     Then only export verts with faces.

        f.write(struct.pack("H", POA_uvs))		# calculate models
        f.write(struct.pack("H", PEN_uvs))
        f.write(struct.pack("H", PSZ_model))

        f.write(struct.pack("B", 255))  # Pallet
        f.write(struct.pack("B", 255))
        f.write(struct.pack("B", 255))
        f.write(struct.pack("B", 255))

        f.write(struct.pack("B", 255))  # Texture
        f.write(struct.pack("B", 255))
        f.write(struct.pack("B", 255))
        f.write(struct.pack("B", 255))

        f.write(struct.pack("B", 77))   # MODL
        f.write(struct.pack("B", 79))
        f.write(struct.pack("B", 68))
        f.write(struct.pack("B", 76))

        f.write(struct.pack("H", MA_EN_verts))
        f.write(struct.pack("H", MA_EN_faces))

        f.write(struct.pack("I", MA_version))

        f.write(struct.pack("I", MA_OA_faces))
        f.write(struct.pack("I", MA_OA_xx1))
        f.write(struct.pack("I", MA_OA_uv_list))
        f.write(struct.pack("I", MA_OA_xx2))

        f.write(struct.pack("H", MA_OA_lod1))
        f.write(struct.pack("H", MA_OA_lod2))

        f.write(struct.pack("H", MA_SZ_32))
        f.write(struct.pack("H", MA_SZ_16))

        f.write(struct.pack("H", MA_xa))
        f.write(struct.pack("H", MA_xb))
        f.write(struct.pack("H", MA_xc))
        f.write(struct.pack("H", MA_xd))
        f.write(struct.pack("H", MA_xe))
        f.write(struct.pack("H", MA_xf))

        f.write(struct.pack("H", MA_xg))
        f.write(struct.pack("H", MA_xh))
        f.write(struct.pack("H", MA_xi))
        f.write(struct.pack("H", MA_xj))
        f.write(struct.pack("H", MA_xk))
        f.write(struct.pack("H", MA_xl))
        f.write(struct.pack("H", MA_xm))
        f.write(struct.pack("H", MA_xn))

        f.write(struct.pack("I", MA_OA_verts))
        f.write(struct.pack("I", MA_OA_xx3))
        f.write(struct.pack("I", MA_unknown))
        f.write(struct.pack("I", MA_OA_points))

        f.close()

        # ----------------------------------------------------------------

        return {'FINISHED'}

# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Panel

class PT_VIEW3D_PINP(Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "PINP"

    def draw(self, context):

        layout = self.layout

        col = layout.column()
        col.prop(context.scene,"pinp_filename",text="")
        col.operator("scene.import_pinp").PINP_file=context.scene.pinp_filename
        #col.operator("scene.export_pinp").PINP_file=context.scene.pinp_filename

# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Register

def register():
    bpy.types.Scene.pinp_filename = bpy.props.StringProperty(default="FILENAME")
    bpy.utils.register_class(OT_IMPORT_PINP)
    bpy.utils.register_class(OT_EXPORT_PINP)
    bpy.utils.register_class(PT_VIEW3D_PINP)

def unregister():
    bpy.utils.unregister_class(PT_VIEW3D_PINP)
    bpy.utils.unregister_class(OT_EXPORT_PINP)
    bpy.utils.unregister_class(OT_IMPORT_PINP)
    del bpy.types.Scene.pinp_filename

if __name__ == "__main__":
    register()

# ----------------------------------------------------------------




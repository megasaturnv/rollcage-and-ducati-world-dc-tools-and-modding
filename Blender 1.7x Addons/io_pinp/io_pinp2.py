# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "PINP Tool",
    "author": "orgyia",
    "version": (1, 0, 2),
    "blender": (2, 72, 0),
    "location": "3D View -> Properties -> PINP Tool",
    "description": "Misc Tools for pinp files",
    "wiki_url": "",
    "category": "Import-Export"
}

import bpy
import struct

class OT_IMPORT_CLUT(bpy.types.Operator):
    bl_idname = "scene.import_clut"
    bl_label = "Import CLUT"
    bl_description = "Import CLUT"
    bl_options = {'REGISTER', 'UNDO'}

    im_name = bpy.props.StringProperty(default="FILENAME")

    pinp = bpy.props.BoolProperty(default=False)
    in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="in_file")
    out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="out_file")
    out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")

    def execute(self, context):

        f = open(self.in_file, 'rb')
        data = f.read()
        f.close()

        error = 0

        if error == 0:

            OA_pallet = 0

            if self.pinp == True:
                OA_pallet = 16

            OA_texture = OA_pallet+768

            PNG = bpy.data.images.new(self.in_file+'.tex',128,160,True,False)
            PNG_data = []

            t=0
            c=0

            while(t < (128*160)):
                c = data[OA_texture+t]
                r = data[OA_pallet+(c*3)]
                g = data[OA_pallet+(c*3)+1]
                b = data[OA_pallet+(c*3)+2]
                PNG_data.append(b/255)
                PNG_data.append(g/255)
                PNG_data.append(r/255)
                PNG_data.append(1.0)
                t+=1

            PNG.pixels=PNG_data
            PNG.pack(as_png=True)

        return {'FINISHED'}

class OT_EXPORT_CLUT(bpy.types.Operator):
    bl_idname = "scene.export_clut"
    bl_label = "Export CLUT"
    bl_description = "Export CLUT"
    bl_options = {'REGISTER', 'UNDO'}

    im_name = bpy.props.StringProperty(default="IMAGENAME")

    pinp = bpy.props.BoolProperty(default=False)
    in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="in_file")
    out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="out_file")
    out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")

    def execute(self, context):

        error = 0

        if self.out_text == "":
            error = 1
            print('error: No Image Texture selected')
            self.report({"ERROR"},"No Image Texture selected, select a blender image to export.")

        if error == 0:

            im = bpy.data.images[self.out_text]
            px = im.pixels[:]
            check = [0] * 61440
            color = [0] * 61440
            point = [0] * 20480

            colors = 0
            t=0

            t = 0
            while(t < (128*160)):

                check[(t*3)]   = int(px[(t*4)+2]*255)
                check[(t*3)+1] = int(px[(t*4)+1]*255)
                check[(t*3)+2] = int(px[(t*4)]*255)

                t += 1

            t = 0
            while(t < ((128*160)*3)):

                a = check[(t)]
                a &= 128
                b = check[(t)]
                b &= 4

                if a == 0 and b == 4:
                    check[(t)] -= 4

                if a == 128 and b == 0:
                    check[(t)] += 4

                t += 1

            t = 0
            while(t < (128*160)):

                r = check[(t*3)]
                g = check[(t*3)+1]
                b = check[(t*3)+2]

                new_color = 1
                scan = 0

                while(scan < 256 and scan < colors):

                    if color[(scan*3)] == r and color[(scan*3)+1] == g and color[(scan*3)+2] == b:
                        new_color = 0
                        scan = 256

                    scan += 1

                if colors > 256:
                    error = 2
                    t = 20480
                    print('check error: too many colors')
                    self.report({"ERROR"},"Too many colors, 256 max, try reducing colors.")

                if new_color == 1 and error == 0:
                    color[((colors)*3)] = r
                    color[((colors)*3)+1] = g
                    color[((colors)*3)+2] = b
                    colors += 1

                t += 1

            if error == 2:
                print('Too many colors.')
                self.report({"ERROR"},"Too many colors, 256 max, try reducing colors.")

            if error == 0:

                t = 0
                while(t < (128*160)):

                    r = check[(t*3)]
                    g = check[(t*3)+1]
                    b = check[(t*3)+2]

                    for i in range(colors):
                        if color[(i*3)] == r and color[(i*3)+1] == g and color[(i*3)+2] == b:
                            point[t] = i

                    t += 1

            if error == 0 and self.pinp == False:

                f = open(self.out_file, 'wb')

                for i in range(256*3):
                    f.write(struct.pack("B", color[i]))

                for i in range(128*160):
                    f.write(struct.pack("B", point[i]))

                f.close()

            if error == 0 and self.pinp == True:

                f = open(self.in_file, 'rb')
                data = f.read()
                f.close()
                xxxx = list(data)

                for i in range(256*3):
                    xxxx[16+i] = color[i]

                for i in range(128*160):
                    xxxx[784+i] = point[i]

                f = open(self.out_file+'.pinp', 'wb')
                f.write(bytearray(xxxx))
                f.close()

        return {'FINISHED'}

class PT_PINP_Tool(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "PINP Tool"

    def draw(self, context):

        layout = self.layout

        col = layout.column(align=True)
        col.prop(context.scene,"pinp_in_file",text="")
        col.prop(context.scene,"pinp_out_file",text="")
        col.prop_search(context.scene, "pinp_out_text", bpy.data, "images", text='')

        col = layout.column(align=True)
        p = col.operator("scene.import_clut", text="Import Texture")
        p.in_file = context.scene.pinp_in_file
        p.out_file = context.scene.pinp_out_file
        p.out_text = context.scene.pinp_out_text
        p.pinp = False

        p = col.operator("scene.export_clut", text="Export Texture")
        p.in_file = context.scene.pinp_in_file
        p.out_file = context.scene.pinp_out_file
        p.out_text = context.scene.pinp_out_text
        p.pinp = False

        col = layout.column(align=True)
        p = col.operator("scene.import_clut", text="Import PINP Texture")
        p.in_file = context.scene.pinp_in_file
        p.out_file = context.scene.pinp_out_file
        p.out_text = context.scene.pinp_out_text
        p.pinp = True

        p = col.operator("scene.export_clut", text="Update PINP Texture")
        p.in_file = context.scene.pinp_in_file
        p.out_file = context.scene.pinp_out_file
        p.out_text = context.scene.pinp_out_text
        p.pinp = True

def register():
    bpy.types.Scene.pinp_in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="Read_File_Name")
    bpy.types.Scene.pinp_out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="Write_File_Name")
    bpy.types.Scene.pinp_out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")
    bpy.utils.register_module(__name__)

def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.pinp_in_file
    del bpy.types.Scene.pinp_out_file
    del bpy.types.Scene.pinp_out_text

if __name__ == "__main__":
    register()


# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# ----------------------------------------------------------------
# Header

bl_info = {
    "name": "Export PINP",
    "author": "orgyia",
    "version": (1, 0, 3),
    "blender": (2, 72, 0),
    "location": "3D View -> Properties -> PINP Tool",
    "description": "Misc Tools for pinp files",
    "wiki_url": "",
    "category": "Import-Export"
}

import bpy
import struct

from bpy_extras.object_utils import AddObjectHelper, object_data_add
from bpy.props import FloatVectorProperty
from mathutils import Vector

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# UI Properties

class PP_props(bpy.types.PropertyGroup):
    i_text      = bpy.props.StringProperty(name="Texture", description="Blender Texture to export (Max 256 colors).", default='')
    i_moda      = bpy.props.StringProperty(name="Model", description="Mesh Object to export (Requires UV Mapping).", default='')
    i_modb      = bpy.props.StringProperty(name="MIP 1", description="Mip 1 Mesh Object to export (Must use duplicated verts from Model).", default='')
    i_modc      = bpy.props.StringProperty(name="MIP 2", description="Mip 2 Mesh Object to export (Must use duplicated verts from Model).", default='')
    i_pnt1	= bpy.props.StringProperty(name="Test Point", description="Lights, Flames.", default='')
    o_file      = bpy.props.StringProperty(name="Save As", description="Name for the pinp file.", default='filename')

# ----------------------------------------------------------------






# ----------------------------------------------------------------
# Import PINP

class OT_IM_PINP(bpy.types.Operator):
    bl_idname = "scene.im_pinp"
    bl_label = "Import PINP"
    bl_description = "Import PINP"
    bl_options = {'REGISTER', 'UNDO'}

    #im_name = bpy.props.StringProperty(default="FILENAME")
    #pinp = bpy.props.BoolProperty(default=False)
    in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="in_file")

    #out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="out_file")
    #out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")

    def execute(self, context):

        # --------------------------------
        # Read File

        f = open(self.in_file, 'rb')
        data = f.read()
        f.close()

        error = 0

        if error == 0:

            '''
            # --------------------------------
            # Header

            PEN_unknown = int.from_bytes((data[4], data[5]), byteorder='little', signed=False)
            POA_pallet = int.from_bytes((data[6], data[7]), byteorder='little', signed=False)
            POA_model = int.from_bytes((data[8], data[9]), byteorder='little', signed=False)
            POA_uvs = int.from_bytes((data[10], data[11]), byteorder='little', signed=False)
            PEN_uvs = int.from_bytes((data[12], data[13]), byteorder='little', signed=False)
            PSZ_model = int.from_bytes((data[14], data[15]), byteorder='little', signed=False)
            POA_texture = POA_pallet+768
            '''

            # --------------------------------
            # Pinp Data

            #ph_id           = [0x50,0x49,0x4E,0x50]         # PINP
            #ph_zero         = 0                             # 0
            #ph_tex_height   = int.from_bytes((data[5]), byteorder='little', signed=False)
            ph_tex_height   = data[5]
            ph_aa_pallet    = int.from_bytes((data[6], data[7]), byteorder='little', signed=False)
            ph_aa_moda      = int.from_bytes((data[8], data[9]), byteorder='little', signed=False)
            ph_aa_uvs       = int.from_bytes((data[10], data[11]), byteorder='little', signed=False)
            ph_en_uvs       = int.from_bytes((data[12], data[13]), byteorder='little', signed=False)
            #ph_ob_size      = 0                             # not required

            aa_texture   = ph_aa_pallet+768

            print('ph_en_uvs: '+str(ph_en_uvs))

            # --------------------------------
            # Load Texture

            PNG = bpy.data.images.new(self.in_file+'.tex',128,160,True,False)
            PNG_data = []

            t=0
            c=0

            while(t < (128*160)):
                c = data[aa_texture+t]
                r = data[ph_aa_pallet+(c*3)]
                g = data[ph_aa_pallet+(c*3)+1]
                b = data[ph_aa_pallet+(c*3)+2]
                PNG_data.append(b/255)
                PNG_data.append(g/255)
                PNG_data.append(r/255)
                PNG_data.append(1.0)
                t+=1

            PNG.pixels=PNG_data
            PNG.pack(as_png=True)

            # K.
            # The textures loaded.
            # Now we need to load the models as one object.
            # They all use the same shared verts, but the face come from each modl
            # Some faces are the same on the different models so we need to check for duplicates.
            # The ID numbers look like they are arranged the same way for each modl.
            # moda v1 v2 v3 v4 use the same vert order modb v1 v2 v3 v4.

            # Anyway
            # loop moda faces
            #   add face, add model id, 2 arrays
            # loop modb faces
            #   add face, add model id, 2 arrays
            # loop modc faces
            #   add face, add model id, 2 arrays

            # ----

            # load all faces to 3 lists
            # faces in moda are all unique, add all to faces and 'face list'

            # loop modb
            #   if face in moda
            #     dont add to faces, add moda face id to 'face list'
            #   else
            #     add to faces, add face id to face list

            # loop modc
            #   if face in moda or modb (may be best to check new faces list)
            #     dont add to faces, add modx face id to 'face list'
            #   else
            #     add to faces, add face id to face list

            # faces = faces for model
            # face list = a list of faces use for each model

            # check below how faces are added
            # you have to add 3 ids for tris, or 4 for quads, also ids -= 1
            # it will probably be best to add all faces to a list with 4th being 0 for tris, and normal +1 ids
            # this will make it easier to compare new faces

            # OK

            # --------------------------------
            # Header

            ma_faces        = int.from_bytes((data[ph_aa_moda+6], data[ph_aa_moda+7]), byteorder='little', signed=False)
            ma_verts        = int.from_bytes((data[ph_aa_moda+4], data[ph_aa_moda+5]), byteorder='little', signed=False)
            ma_points       = 0
            ma_uvs          = 0
            ma_texts        = 0

            ma_face_list    = int.from_bytes((data[ph_aa_moda+12], data[ph_aa_moda+13], data[ph_aa_moda+14], data[ph_aa_moda+15]), byteorder='little', signed=False)
            ma_vert_list    = int.from_bytes((data[ph_aa_moda+64], data[ph_aa_moda+65], data[ph_aa_moda+66], data[ph_aa_moda+67]), byteorder='little', signed=False)
            ma_point_list   = 0
            ma_uv_list      = int.from_bytes((data[ph_aa_moda+20], data[ph_aa_moda+21]), byteorder='little', signed=False)

            ma_modb         = int.from_bytes((data[ph_aa_moda+28], data[ph_aa_moda+29]), byteorder='little', signed=False)
            ma_modc         = int.from_bytes((data[ph_aa_moda+30], data[ph_aa_moda+31]), byteorder='little', signed=False)
            ma_xx1          = 0
            ma_xx2          = 0
            ma_xx3          = 0


            mb_faces        = int.from_bytes((data[ph_aa_moda+ma_modb+6], data[ph_aa_moda+ma_modb+7]), byteorder='little', signed=False)
            mb_verts        = int.from_bytes((data[ph_aa_moda+ma_modb+4], data[ph_aa_moda+ma_modb+5]), byteorder='little', signed=False)
            mb_points       = 0
            mb_uvs          = 0
            mb_texts        = 0

            # vert list offset probably different for B and C
            # check later

            mb_face_list    = int.from_bytes((data[ph_aa_moda+ma_modb+12], data[ph_aa_moda+ma_modb+13], data[ph_aa_moda+ma_modb+14], data[ph_aa_moda+ma_modb+15]), byteorder='little', signed=False)
            mb_vert_list    = int.from_bytes((data[ph_aa_moda+ma_modb+64], data[ph_aa_moda+ma_modb+65], data[ph_aa_moda+ma_modb+66], data[ph_aa_moda+ma_modb+67]), byteorder='little', signed=False)
            mb_point_list   = 0
            mb_uv_list      = int.from_bytes((data[ph_aa_moda+ma_modb+20], data[ph_aa_moda+ma_modb+21]), byteorder='little', signed=False)

            mb_modb         = 0
            mb_modc         = 0
            mb_xx1          = 0
            mb_xx2          = 0
            mb_xx3          = 0


            mc_faces        = int.from_bytes((data[ph_aa_moda+ma_modc+6], data[ph_aa_moda+ma_modc+7]), byteorder='little', signed=False)
            mc_verts        = int.from_bytes((data[ph_aa_moda+ma_modc+4], data[ph_aa_moda+ma_modc+5]), byteorder='little', signed=False)
            mc_points       = 0
            mc_uvs          = 0
            mc_texts        = 0

            mc_face_list    = int.from_bytes((data[ph_aa_moda+ma_modc+12], data[ph_aa_moda+ma_modc+13], data[ph_aa_moda+ma_modc+14], data[ph_aa_moda+ma_modc+15]), byteorder='little', signed=False)
            mc_vert_list    = int.from_bytes((data[ph_aa_moda+ma_modc+64], data[ph_aa_moda+ma_modc+65], data[ph_aa_moda+ma_modc+66], data[ph_aa_moda+ma_modc+67]), byteorder='little', signed=False)
            mc_point_list   = 0
            mc_uv_list      = int.from_bytes((data[ph_aa_moda+ma_modc+20], data[ph_aa_moda+ma_modc+21]), byteorder='little', signed=False)

            mc_modb         = 0
            mc_modc         = 0
            mc_xx1          = 0
            mc_xx2          = 0
            mc_xx3          = 0

            print('mb_uv_list: '+hex(ma_uv_list))
            print('mb_uv_list: '+hex(mb_uv_list))
            print('mb_uv_list: '+hex(mc_uv_list))

            # --------------------------------
            # Face Lists

            modu_faces = []          # All unique faces

            moda_faces = []          # A faces
            modb_faces = []          # B faces
            modc_faces = []          # C faces

            # lol, some unique faces use same uv coords
            # so the unique face size is unknown
            # this can only be determined when stacking faces
            # use +100 pad for now

            moda_upoly = [0] * (ph_en_uvs+100)          # A modu face and uv ids
            modb_upoly = [0] * (ph_en_uvs+100)          # B modu face and uv ids
            modc_upoly = [0] * (ph_en_uvs+100)          # C modu face and uv ids

            # add faces to moda
            # add faces to modu
            # add polys to moda

            # --------------------------------
            # Face Check

            '''
            print('modu_faces')
            print(modu_faces)

            print('moda_faces')
            print(moda_faces)

            print('moda_upoly')
            print(moda_upoly)
            '''
            # Test import
            # Works

            # append to modb_faces
            # append to unique

            # scan check

            # loop modb faces
            #   loop compare moda faces
            #     if same face, skip to end, add id to upoly
            #     if new face, append face, add id to upoly

            # if previous color, skip
            # if new color, add color

            # side by side color while loop

            # --------------------------------
            # Append Faces

            ob_faces = 0

            for f in range(ma_faces):
                modu_faces.append([data[ph_aa_moda+ma_face_list+(f*8)], data[ph_aa_moda+ma_face_list+(f*8)+1], data[ph_aa_moda+ma_face_list+(f*8)+2], data[ph_aa_moda+ma_face_list+(f*8)+3]])
                moda_faces.append([data[ph_aa_moda+ma_face_list+(f*8)], data[ph_aa_moda+ma_face_list+(f*8)+1], data[ph_aa_moda+ma_face_list+(f*8)+2], data[ph_aa_moda+ma_face_list+(f*8)+3]])
                moda_upoly[f] = f+1
                ob_faces += 1

            for f in range(mb_faces):
                modb_faces.append([data[ph_aa_moda+ma_modb+mb_face_list+(f*8)], data[ph_aa_moda+ma_modb+mb_face_list+(f*8)+1], data[ph_aa_moda+ma_modb+mb_face_list+(f*8)+2], data[ph_aa_moda+ma_modb+mb_face_list+(f*8)+3]])

            for f in range(mc_faces):
                modc_faces.append([data[ph_aa_moda+ma_modc+mc_face_list+(f*8)], data[ph_aa_moda+ma_modc+mc_face_list+(f*8)+1], data[ph_aa_moda+ma_modc+mc_face_list+(f*8)+2], data[ph_aa_moda+ma_modc+mc_face_list+(f*8)+3]])

            # --------------------------------
            # Check and Stack Unique B Faces

            # Case 1: Same Face
            # Case 2: New Face

            # its 0: face not used for modb
            # value: face id for modb

            # upoly is the blender polygon/uvlayer id
            # the list containts the model face ids, blender face -> rollcage modl + face
            # this is used later to find a correct uv offset for the blender polygon

            #print(modu_faces)

            BID = 1

            # Loop B Faces
            for fb in range(mb_faces):

                fb0 = modb_faces[fb][0]
                fb1 = modb_faces[fb][1]
                fb2 = modb_faces[fb][2]
                fb3 = modb_faces[fb][3]

                new_face = 1
                scan = 0

                # Loop Unique Faces
                while(scan < ob_faces):

                    fu0 = modu_faces[scan][0]
                    fu1 = modu_faces[scan][1]
                    fu2 = modu_faces[scan][2]
                    fu3 = modu_faces[scan][3]

                    # Same Face found, Add BID (modb face ID)
                    if fb0 == fu0 and fb1 == fu1 and fb2 == fu2 and fb3 == fu3:

                        modb_upoly[scan] = BID
                        #BID += 1

                        new_face = 0
                        scan = ob_faces

                    scan += 1

                # New Face, Add BID (modb face ID) and Face
                if new_face == 1:

                    modb_upoly[ob_faces] = BID
                    #BID += 1

                    modu_faces.append([fb0, fb1, fb2, fb3])
                    ob_faces += 1

                BID += 1

            #print(modu_faces)

            # --------------------------------
            # Check and Stack Unique C Faces

            #print(modu_faces)

            CID = 1

            # Loop C Faces
            for fc in range(mc_faces):

                fc0 = modc_faces[fc][0]
                fc1 = modc_faces[fc][1]
                fc2 = modc_faces[fc][2]
                fc3 = modc_faces[fc][3]

                new_face = 1
                scan = 0

                # Loop Unique Faces
                while(scan < ob_faces):

                    fu0 = modu_faces[scan][0]
                    fu1 = modu_faces[scan][1]
                    fu2 = modu_faces[scan][2]
                    fu3 = modu_faces[scan][3]

                    # Same Face found, Add CID (modc face ID)
                    if fc0 == fu0 and fc1 == fu1 and fc2 == fu2 and fc3 == fu3:

                        modc_upoly[scan] = CID
                        #CID += 1

                        new_face = 0
                        scan = ob_faces

                    scan += 1

                # New Face, Add CID (modc face ID) and Face
                if new_face == 1:

                    modc_upoly[ob_faces] = CID
                    #CID += 1

                    modu_faces.append([fc0, fc1, fc2, fc3])
                    ob_faces += 1

                CID += 1

            #print('')
            #print('modu_faces')
            #print(modu_faces)

            '''
            print('')
            print('moda_upoly')
            print(moda_upoly)

            print('')
            print('modb_upoly')
            print(modb_upoly)

            print('')
            print('modc_upoly')
            print(modc_upoly)
            '''

            # Making progress
            # May be able to add verts then import
            # I could verify data, but I dunno

            # K, lets assume all the faces are correct for modu_faces and ob_faces
            # Add shared verts then add mesh object

            '''
            #ma_vert_list

            for v in range(ma_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            '''

            # --------------------------------
            # Add mesh

            bverts = []
            bfaces = []
            bedges = []

            # Verts

            for v in range(ma_verts):
                vec1 = int.from_bytes((data[ph_aa_moda+ma_vert_list+(v*8)], data[ph_aa_moda+ma_vert_list+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[ph_aa_moda+ma_vert_list+(v*8)+2], data[ph_aa_moda+ma_vert_list+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[ph_aa_moda+ma_vert_list+(v*8)+4], data[ph_aa_moda+ma_vert_list+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))

            # Faces

            for f in range(ob_faces):
                if modu_faces[f][3] == 0:
                    bfaces.append([modu_faces[f][2]-1, modu_faces[f][0]-1, modu_faces[f][1]-1])
                else:
                    bfaces.append([modu_faces[f][2]-1, modu_faces[f][0]-1, modu_faces[f][1]-1, modu_faces[f][3]-1])

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            # OK
            # Time to test lol

            # For some reason the Face Vert IDs are wrong, many cross quad faces
            # Solved, I setting the blender orientation twice

            # I though LOD design may be odd, but its ok.
            # When designing lods one can duplicate original, delete faces, make lods.
            # When happy with the design, the user can add the same faces to the original model.
            # Then the user can select the faces for the model, hide, uv, etc.
            # So its odd but it should be ok.

            # k
            # time to import shared uvs
            # check UV A code

            # --------------------------------
            # Load UVs from first valid upoly

            # ph_aa_uvs             # UV Coordinates

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            # all uv offsets are from the first modl
            # getting index out of range problems
            # problem is, have to use correct id from upoly, blend to modl face id
            # replace uvid

            lid = 0		# 2d uv vertex
            uvid = 0		# modl uv offset
            poly = 0		# polygon
            done = 0		# modl processed

            for p in context.active_object.data.polygons:

                done = 0

                if p.loop_total == 4:

                    if moda_upoly[poly] != 0:

                        uvid = (moda_upoly[poly]-1)*2

                        uvc = int.from_bytes((data[ph_aa_moda+ma_uv_list+uvid+0], data[ph_aa_moda+ma_uv_list+uvid+1]), byteorder='little', signed=False)
                        layer = context.active_object.data.uv_layers.active
                        layer.data[lid].uv[0] = (data[ph_aa_moda+uvc+4] +0.5)/128
                        layer.data[lid].uv[1] = (data[ph_aa_moda+uvc+5] +0.5)/160
                        layer.data[lid+1].uv[0] = (data[ph_aa_moda+uvc+0] +0.5)/128
                        layer.data[lid+1].uv[1] = (data[ph_aa_moda+uvc+1] +0.5)/160
                        layer.data[lid+2].uv[0] = (data[ph_aa_moda+uvc+8] +0.5)/128
                        layer.data[lid+2].uv[1] = (data[ph_aa_moda+uvc+9] +0.5)/160
                        layer.data[lid+3].uv[0] = (data[ph_aa_moda+uvc+10] +0.5)/128
                        layer.data[lid+3].uv[1] = (data[ph_aa_moda+uvc+11] +0.5)/160
                        lid+=4
                        done = 1

                    if modb_upoly[poly] != 0 and done == 0:

                        uvid = (modb_upoly[poly]-1)*2

                        uvc = int.from_bytes((data[ph_aa_moda+mb_uv_list+uvid+0], data[ph_aa_moda+mb_uv_list+uvid+1]), byteorder='little', signed=False)
                        layer = context.active_object.data.uv_layers.active
                        layer.data[lid].uv[0] = (data[ph_aa_moda+uvc+4] +0.5)/128
                        layer.data[lid].uv[1] = (data[ph_aa_moda+uvc+5] +0.5)/160
                        layer.data[lid+1].uv[0] = (data[ph_aa_moda+uvc+0] +0.5)/128
                        layer.data[lid+1].uv[1] = (data[ph_aa_moda+uvc+1] +0.5)/160
                        layer.data[lid+2].uv[0] = (data[ph_aa_moda+uvc+8] +0.5)/128
                        layer.data[lid+2].uv[1] = (data[ph_aa_moda+uvc+9] +0.5)/160
                        layer.data[lid+3].uv[0] = (data[ph_aa_moda+uvc+10] +0.5)/128
                        layer.data[lid+3].uv[1] = (data[ph_aa_moda+uvc+11] +0.5)/160
                        lid+=4
                        done = 1

                    if modc_upoly[poly] != 0 and done == 0:

                        uvid = (modc_upoly[poly]-1)*2

                        uvc = int.from_bytes((data[ph_aa_moda+mb_uv_list+uvid+0], data[ph_aa_moda+mb_uv_list+uvid+1]), byteorder='little', signed=False)
                        layer = context.active_object.data.uv_layers.active
                        layer.data[lid].uv[0] = (data[ph_aa_moda+uvc+4] +0.5)/128
                        layer.data[lid].uv[1] = (data[ph_aa_moda+uvc+5] +0.5)/160
                        layer.data[lid+1].uv[0] = (data[ph_aa_moda+uvc+0] +0.5)/128
                        layer.data[lid+1].uv[1] = (data[ph_aa_moda+uvc+1] +0.5)/160
                        layer.data[lid+2].uv[0] = (data[ph_aa_moda+uvc+8] +0.5)/128
                        layer.data[lid+2].uv[1] = (data[ph_aa_moda+uvc+9] +0.5)/160
                        layer.data[lid+3].uv[0] = (data[ph_aa_moda+uvc+10] +0.5)/128
                        layer.data[lid+3].uv[1] = (data[ph_aa_moda+uvc+11] +0.5)/160
                        lid+=4
                        done = 1

                if p.loop_total == 3:

                    if moda_upoly[poly] != 0:

                        uvid = (moda_upoly[poly]-1)*2

                        uvc = int.from_bytes((data[ph_aa_moda+ma_uv_list+uvid+0], data[ph_aa_moda+ma_uv_list+uvid+1]), byteorder='little', signed=False)
                        layer = context.active_object.data.uv_layers.active
                        layer.data[lid].uv[0] = (data[ph_aa_moda+uvc+4] +0.5)/128
                        layer.data[lid].uv[1] = (data[ph_aa_moda+uvc+5] +0.5)/160
                        layer.data[lid+1].uv[0] = (data[ph_aa_moda+uvc+0] +0.5)/128
                        layer.data[lid+1].uv[1] = (data[ph_aa_moda+uvc+1] +0.5)/160
                        layer.data[lid+2].uv[0] = (data[ph_aa_moda+uvc+8] +0.5)/128
                        layer.data[lid+2].uv[1] = (data[ph_aa_moda+uvc+9] +0.5)/160
                        lid+=3
                        done = 1

                    if modb_upoly[poly] != 0 and done == 0:

                        uvid = (modb_upoly[poly]-1)*2

                        uvc = int.from_bytes((data[ph_aa_moda+mb_uv_list+uvid+0], data[ph_aa_moda+mb_uv_list+uvid+1]), byteorder='little', signed=False)
                        layer = context.active_object.data.uv_layers.active
                        layer.data[lid].uv[0] = (data[ph_aa_moda+uvc+4] +0.5)/128
                        layer.data[lid].uv[1] = (data[ph_aa_moda+uvc+5] +0.5)/160
                        layer.data[lid+1].uv[0] = (data[ph_aa_moda+uvc+0] +0.5)/128
                        layer.data[lid+1].uv[1] = (data[ph_aa_moda+uvc+1] +0.5)/160
                        layer.data[lid+2].uv[0] = (data[ph_aa_moda+uvc+8] +0.5)/128
                        layer.data[lid+2].uv[1] = (data[ph_aa_moda+uvc+9] +0.5)/160
                        lid+=3
                        done = 1

                    if modc_upoly[poly] != 0 and done == 0:

                        uvid = (modc_upoly[poly]-1)*2

                        uvc = int.from_bytes((data[ph_aa_moda+mb_uv_list+uvid+0], data[ph_aa_moda+mb_uv_list+uvid+1]), byteorder='little', signed=False)
                        layer = context.active_object.data.uv_layers.active
                        layer.data[lid].uv[0] = (data[ph_aa_moda+uvc+4] +0.5)/128
                        layer.data[lid].uv[1] = (data[ph_aa_moda+uvc+5] +0.5)/160
                        layer.data[lid+1].uv[0] = (data[ph_aa_moda+uvc+0] +0.5)/128
                        layer.data[lid+1].uv[1] = (data[ph_aa_moda+uvc+1] +0.5)/160
                        layer.data[lid+2].uv[0] = (data[ph_aa_moda+uvc+8] +0.5)/128
                        layer.data[lid+2].uv[1] = (data[ph_aa_moda+uvc+9] +0.5)/160
                        lid+=3
                        done = 1

                poly += 1






            '''
            # --------------------------------
            # UV

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            #MAOA_uv_list = int.from_bytes((data[POA_model+20], data[POA_model+21]), byteorder='little', signed=False)

            # each model has a uv list
            # the uv list point to the shared uv coordinates

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+ma_uv_list+uvid+0], data[ph_aa_moda+ma_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[ph_aa_moda+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[ph_aa_moda+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+ma_uv_list+uvid+0], data[ph_aa_moda+ma_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(ob_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG
            '''




























































            '''

            # --------------------------------
            # Model A

            MAEN_verts = int.from_bytes((data[POA_model+4], data[POA_model+5]), byteorder='little', signed=False)
            MAEN_faces = int.from_bytes((data[POA_model+6], data[POA_model+7]), byteorder='little', signed=False)
            MAOA_faces = int.from_bytes((data[POA_model+12], data[POA_model+13], data[POA_model+14], data[POA_model+15]), byteorder='little', signed=False)
            MAOA_verts = int.from_bytes((data[POA_model+64], data[POA_model+65], data[POA_model+66], data[POA_model+67]), byteorder='little', signed=False)

            #print('faces: '+str(MAEN_faces))

            bverts = []
            bfaces = []
            bedges = []

            # Faces

            for f in range(MAEN_faces):
                if data[POA_model+MAOA_faces+(f*8)+3] == 0:
                    bfaces.append([data[POA_model+MAOA_faces+(f*8)+2]-1, data[POA_model+MAOA_faces+(f*8)+0]-1, data[POA_model+MAOA_faces+(f*8)+1]-1])
                else:
                    bfaces.append([data[POA_model+MAOA_faces+(f*8)+2]-1, data[POA_model+MAOA_faces+(f*8)+0]-1, data[POA_model+MAOA_faces+(f*8)+1]-1, data[POA_model+MAOA_faces+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(MAEN_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            # --------------------------------
            # UV A

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            MAOA_uv_list = int.from_bytes((data[POA_model+20], data[POA_model+21]), byteorder='little', signed=False)

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[POA_model+MAOA_uv_list+uvid+0], data[POA_model+MAOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[POA_model+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[POA_model+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[POA_model+MAOA_uv_list+uvid+0], data[POA_model+MAOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(MAEN_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # ----------------------------------------------------------------
            # Model B

            MAOA_modelb = POA_model + int.from_bytes((data[POA_model+28], data[POA_model+29]), byteorder='little', signed=False)

            MBEN_verts = int.from_bytes((data[MAOA_modelb+4], data[MAOA_modelb+5]), byteorder='little', signed=False)
            MBEN_faces = int.from_bytes((data[MAOA_modelb+6], data[MAOA_modelb+7]), byteorder='little', signed=False)
            MBOA_faces = int.from_bytes((data[MAOA_modelb+12], data[MAOA_modelb+13], data[MAOA_modelb+14], data[MAOA_modelb+15]), byteorder='little', signed=False)

            bverts = []
            bfaces = []
            bedges = []

            # Faces

            for f in range(MBEN_faces):
                if data[MAOA_modelb+MBOA_faces+(f*8)+3] == 0:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1])
                else:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1, data[MAOA_modelb+MBOA_faces+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(MAEN_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            context.active_object.location[2] += 40

            # ----------------------------------------------------------------
            # UV B

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            MBOA_uv_list = int.from_bytes((data[MAOA_modelb+20], data[MAOA_modelb+21]), byteorder='little', signed=False)

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[POA_model+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[POA_model+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(MBEN_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # ----------------------------------------------------------------
            # Model C
            # changed MAOA_modelb offset (should be modelc)

            # required data
            # verts, en, oa
            # faces, en, oa
            # these should be precalculated then passed to a function

            # previous working code

            MAOA_modelb = POA_model + int.from_bytes((data[POA_model+30], data[POA_model+31]), byteorder='little', signed=False)
            MBEN_verts = int.from_bytes((data[MAOA_modelb+4], data[MAOA_modelb+5]), byteorder='little', signed=False)
            MBEN_faces = int.from_bytes((data[MAOA_modelb+6], data[MAOA_modelb+7]), byteorder='little', signed=False)
            MBOA_faces = int.from_bytes((data[MAOA_modelb+12], data[MAOA_modelb+13], data[MAOA_modelb+14], data[MAOA_modelb+15]), byteorder='little', signed=False)

            bverts = []
            bfaces = []
            bedges = []

            # Faces

            for f in range(MBEN_faces):
                if data[MAOA_modelb+MBOA_faces+(f*8)+3] == 0:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1])
                else:
                    bfaces.append([data[MAOA_modelb+MBOA_faces+(f*8)+2]-1, data[MAOA_modelb+MBOA_faces+(f*8)+0]-1, data[MAOA_modelb+MBOA_faces+(f*8)+1]-1, data[MAOA_modelb+MBOA_faces+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(MAEN_verts):
                vec1 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)], data[POA_model+MAOA_verts+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+2], data[POA_model+MAOA_verts+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[POA_model+MAOA_verts+(v*8)+4], data[POA_model+MAOA_verts+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            context.active_object.location[2] += 80

            # ----------------------------------------------------------------
            # UV C

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            MBOA_uv_list = int.from_bytes((data[MAOA_modelb+20], data[MAOA_modelb+21]), byteorder='little', signed=False)

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[POA_model+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[POA_model+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[POA_model+MBOA_uv_list+uvid+0], data[POA_model+MBOA_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[POA_model+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[POA_model+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[POA_model+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[POA_model+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[POA_model+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[POA_model+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(MBEN_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            '''

        # --------------------------------
        # Finished

        return {'FINISHED'}

# ----------------------------------------------------------------






# ----------------------------------------------------------------
# Export PINP

class OT_EX_PINP(bpy.types.Operator):
    bl_idname = "scene.ex_pinp"
    bl_label = "Export PINP"
    bl_description = "Export PINP"
    bl_options = {'REGISTER', 'UNDO'}

    # Texture
    # Model 0 object
    # Model 1 object
    # Model 2 object
    # Empty objects
    # Custom Properties

    i_text      = bpy.props.StringProperty(name="Texture", description="Blender Texture to export (Max 256 colors).", default='')
    i_moda      = bpy.props.StringProperty(name="Model", description="Mesh Object to export (Requires UV Mapping).", default='')
    i_modb      = bpy.props.StringProperty(name="MIP 1", description="Mip 1 Mesh Object to export (Must use duplicated verts from Model).", default='')
    i_modc      = bpy.props.StringProperty(name="MIP 2", description="Mip 2 Mesh Object to export (Must use duplicated verts from Model).", default='')
    i_pnt1	= bpy.props.StringProperty(name="Test Point", description="Lights, Flames.", default='')
    o_file      = bpy.props.StringProperty(name="Save As", description="Name for the pinp file.", default='filename')

    def execute(self, context):

        # --------------------------------
        # Error Check

        error = 0

        if self.i_text == "":
            error = 1
            print('error: No Image Texture selected')
            self.report({"ERROR"},"No Image Texture selected, select a blender image to export.")

        if error == 0:

            # --------------------------------
            # Pinp Data

            ph_id           = [0x50,0x49,0x4E,0x50]         # fixed
            ph_zero         = 0                             # fixed
            ph_tex_height   = 128+32                        # fixed
            ph_aa_pallet    = 0x10                          # fixed
            ph_aa_moda      = 0x5310                        # fixed
            ph_aa_uvs       = 0                             # can calc after shared verts
            ph_en_uvs       = 0                             # can calc whenever
            ph_ob_size      = 0                             # can calc after all

            # We can now export header, colors, points, moda header.
            # But before that, data for the header is required (uv address, ob size etc).
            # To calculate this, its best to check the mesh.

            # --------------------------------
            # Mesh Check

            ma_me           = context.scene.objects[self.i_moda].data
            mb_me           = context.scene.objects[self.i_modb].data
            mc_me           = context.scene.objects[self.i_modc].data

            # This is a list of data we need to know for the moda header
            # data		size	address		info
            # faces		calc	calc		header -> 0x50
            # xxx		fixed	calc		header -> xxx
            # mips		calc	calc		header -> mips
            # verts		calc	calc		header -> verts
            # points		check	calc		header -> points
            # uv		calc	calc		header -> uvs

            # --------------------------------
            # Header

            #ma_aa_header    = 0        # ph_aa_moda

            ma_faces        = 0
            ma_verts        = 0
            ma_points       = 0
            ma_uvs          = 0
            ma_texts        = 0

            ma_face_list    = 0
            ma_vert_list    = 0
            ma_point_list   = 0
            ma_uv_list      = 0

            ma_modb         = 0
            ma_modc         = 0
            ma_xx1          = 0
            ma_xx2          = 0
            ma_xx3          = 0


            mb_faces        = 0
            mb_verts        = 0
            mb_points       = 0
            mb_uvs          = 0
            mb_texts        = 0

            mb_face_list    = 0
            mb_vert_list    = 0
            mb_point_list   = 0
            mb_uv_list      = 0

            mb_modb         = 0
            mb_modc         = 0
            mb_xx1          = 0
            mb_xx2          = 0
            mb_xx3          = 0


            mc_faces        = 0
            mc_verts        = 0
            mc_points       = 0
            mc_uvs          = 0
            mc_texts        = 0

            mc_face_list    = 0
            mc_vert_list    = 0
            mc_point_list   = 0
            mc_uv_list      = 0

            mc_modb         = 0
            mc_modc         = 0
            mc_xx1          = 0
            mc_xx2          = 0
            mc_xx3          = 0


            # --------------------------------
            # Sizes

            for v in ma_me.vertices:
                ma_verts += 1

            for p in ma_me.polygons:
                ma_faces += 1
                ma_texts += p.loop_total

            for v in mb_me.vertices:
                mb_verts += 1

            for p in mb_me.polygons:
                mb_faces += 1
                mb_texts += p.loop_total

            for v in mc_me.vertices:
                mc_verts += 1

            for p in mc_me.polygons:
                mc_faces += 1
                mc_texts += p.loop_total


            # --------------------------------
            # Addresses

            ma_face_list    = 0x50
            ma_xx1          = ma_face_list + (ma_faces*8)
            ma_xx2          = ma_xx1 + 24
            ma_xx3          = ma_xx2 + 512

            ma_modb         = ma_xx3 + 512

            mb_face_list    = 0x50
            mb_xx1          = mb_face_list + (mb_faces*8)
            mb_xx2          = mb_xx1 + 36
            mb_xx3          = mb_xx2 + 512

            ma_modc         = ma_modb + mb_xx3 + 512

            mc_face_list    = 0x50
            mc_xx1          = mc_face_list + (mc_faces*8)
            mc_xx2          = mc_xx1 + 4
            mc_xx3          = mc_xx2 + 4

            ma_vert_list    = ma_modc + mc_xx3 + 240
            ma_point_list   = ma_modc + mc_xx3 + 240			# add verts

            # could add points and uv here but first
            # may be best to try make pinp file then compare data

            # --------------------------------
            # Texture Data

            im = bpy.data.images[self.i_text]
            px = im.pixels[:]
            check = [0] * 61440
            color = [0] * 61440
            point = [0] * 20480

            colors = 0
            t=0

            t = 0
            while(t < (128*160)):

                check[(t*3)]   = int(px[(t*4)+2]*255)
                check[(t*3)+1] = int(px[(t*4)+1]*255)
                check[(t*3)+2] = int(px[(t*4)]*255)

                t += 1

            t = 0
            while(t < ((128*160)*3)):

                a = check[(t)]
                a &= 128
                b = check[(t)]
                b &= 4

                if a == 0 and b == 4:
                    check[(t)] -= 4

                if a == 128 and b == 0:
                    check[(t)] += 4

                t += 1

            # loop colors
            # if previous color, skip
            # if new color, add color

            t = 0
            while(t < (128*160)):

                r = check[(t*3)]
                g = check[(t*3)+1]
                b = check[(t*3)+2]

                new_color = 1
                scan = 0

                while(scan < 256 and scan < colors):

                    if color[(scan*3)] == r and color[(scan*3)+1] == g and color[(scan*3)+2] == b:
                        new_color = 0
                        scan = 256

                    scan += 1

                if colors > 256:
                    error = 2
                    t = 20480
                    print('check error: too many colors')
                    self.report({"ERROR"},"Too many colors, 256 max, try reducing colors.")

                if new_color == 1 and error == 0:
                    color[((colors)*3)] = r
                    color[((colors)*3)+1] = g
                    color[((colors)*3)+2] = b
                    colors += 1

                t += 1

            if error == 2:
                print('Too many colors.')
                self.report({"ERROR"},"Too many colors, 256 max, try reducing colors.")

            if error == 0:

                t = 0
                while(t < (128*160)):

                    r = check[(t*3)]
                    g = check[(t*3)+1]
                    b = check[(t*3)+2]

                    for i in range(colors):
                        if color[(i*3)] == r and color[(i*3)+1] == g and color[(i*3)+2] == b:
                            point[t] = i

                    t += 1

            # Added below

            '''
            if error == 0:

                f = open(self.out_file, 'wb')

                for i in range(256*3):
                    f.write(struct.pack("B", color[i]))

                for i in range(128*160):
                    f.write(struct.pack("B", point[i]))

                f.close()
            '''

            # --------------------------------
            # Write File

            #f = open(self.o_file+'.pinp', 'wb')
            #f.write(bytearray(xxxx))
            #f.close()

            '''
            ph_id           = [0x50,0x49,0x4E,0x50]         # fixed
            ph_zero         = 0                             # fixed
            ph_tex_height   = 128+32                        # fixed
            ph_aa_pallet    = 0x10                          # fixed
            ph_aa_moda      = 0x5310                        # fixed
            ph_aa_uvs       = 0                             # can calc after shared verts
            ph_en_uvs       = 0                             # can calc whenever
            ph_ob_size      = 0                             # can calc after all
            '''

            '''
            Silly Bytes
            >>> bb = xx.to_bytes(length=2,byteorder='little',signed=True)
            b'\xfd\x02'
            >>> bb[
                   0]
                   1]
            '''

            f = open(self.o_file+'.pinp', 'wb')

            # Header

            f.write(bytearray(ph_id))
            f.write(bytearray(ph_zero.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(ph_tex_height.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(ph_aa_pallet.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_aa_moda.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_aa_uvs.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_en_uvs.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_ob_size.to_bytes(length=2,byteorder='little',signed=False)))

            # I will have to start writing data now
            # Color and point tables need to be calculated above
            # Then the preallocated arrays can be written to the file

            # colors and points

            for i in range(256*3):
                f.write(struct.pack("B", color[i]))

            for i in range(128*160):
                f.write(struct.pack("B", point[i]))

            # K
            # Time to write the moda header
            # Most data should be correct from the code above

            # --------------------------------
            # MODA Header

            data_char = [0x4D,0x4F,0x44,0x4C]
            data_int = 1

            f.write(bytearray(data_char))
            f.write(bytearray(ma_verts.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ma_faces.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_face_list.to_bytes(length=4,byteorder='little',signed=False)))

            f.write(bytearray(ma_xx1.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_uv_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_xx2.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_modb.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ma_modc.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 0
            for i in range(32):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            f.write(bytearray(ma_vert_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_xx3.to_bytes(length=4,byteorder='little',signed=False)))
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(ma_point_list.to_bytes(length=4,byteorder='little',signed=False)))

            # Ive done the moda header for now
            # Some data is odd, like the address to shared points
            # But its ok for now
            # Next is the faces, then the xx formats

            # Faces are 8 bytes V1 V2 V3 V4, then 4 bytes of unknown data
            # The unknown data can be zero for now

            # --------------------------------
            # MODA Data

            for p in ma_me.polygons:
                for i in range(p.loop_total):
                    data_int = p.vertices[i]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                if p.loop_total == 3:
                    data_int = 0
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                # extra 4 bytes per face
                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 1
            for i in range(24):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 2
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 3
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # MODB Header

            # need to change all below
            # I will have to check whats an absolute and offset address, shared verts address for example
            # but for now just change all ma_ to mb_, and change xx sizes
            # shared data, verts and uva are added after the models

            data_char = [0x4D,0x4F,0x44,0x4C]
            data_int = 1

            f.write(bytearray(data_char))
            f.write(bytearray(mb_verts.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mb_faces.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_face_list.to_bytes(length=4,byteorder='little',signed=False)))

            f.write(bytearray(mb_xx1.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_uv_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_xx2.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_modb.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mb_modc.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 0
            for i in range(32):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            f.write(bytearray(mb_vert_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_xx3.to_bytes(length=4,byteorder='little',signed=False)))
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(mb_point_list.to_bytes(length=4,byteorder='little',signed=False)))

            # --------------------------------
            # MODB Data

            for p in mb_me.polygons:
                for i in range(p.loop_total):
                    data_int = p.vertices[i]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                if p.loop_total == 3:
                    data_int = 0
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                # extra 4 bytes per face
                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 1
            for i in range(36):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 2
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 3
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # MODC Header

            # need to change all below
            # I will have to check whats an absolute and offset address, shared verts address for example
            # but for now just change all ma_ to mb_, and change xx sizes
            # shared data, verts and uva are added after the models

            data_char = [0x4D,0x4F,0x44,0x4C]
            data_int = 1

            f.write(bytearray(data_char))
            f.write(bytearray(mc_verts.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mc_faces.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_face_list.to_bytes(length=4,byteorder='little',signed=False)))

            f.write(bytearray(mc_xx1.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_uv_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_xx2.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_modb.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mc_modc.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 0
            for i in range(32):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            f.write(bytearray(mc_vert_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_xx3.to_bytes(length=4,byteorder='little',signed=False)))
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(mc_point_list.to_bytes(length=4,byteorder='little',signed=False)))

            # --------------------------------
            # MODC Data

            for p in mc_me.polygons:
                for i in range(p.loop_total):
                    data_int = p.vertices[i]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                if p.loop_total == 3:
                    data_int = 0
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                # extra 4 bytes per face
                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 1
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 2
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 3
            for i in range(240):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # Shared Data

            # Verts, UV Map, check bmo
            # int(float * 10)
            # Verts are signed shorts xxyyzz--

            # --------------------------------
            # Verts

            for v in ma_me.vertices:				# verts

                '''
                f.write(struct.pack("f", v.co[0]))		# X
                f.write(struct.pack("f", v.co[1]))		# Y
                f.write(struct.pack("f", v.co[2]))		# Z
                '''

                x = int(v.co[0])
                y = int(v.co[1])
                z = int(v.co[2])
                p = int(0)

                '''
                #  can divide above inside the local shit ()
                x /= 10.0
                y /= 10.0
                z /= 10.0
                p /= 10.0
                '''

                f.write(bytearray(x.to_bytes(length=2,byteorder='little',signed=True)))
                f.write(bytearray(y.to_bytes(length=2,byteorder='little',signed=True)))
                f.write(bytearray(z.to_bytes(length=2,byteorder='little',signed=True)))
                f.write(bytearray(p.to_bytes(length=2,byteorder='little',signed=True)))

            # --------------------------------
            # UV Map



            f.close()

        # --------------------------------
        # Finished

        return {'FINISHED'}

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Draw Panel

class PT_EX_PINP(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "Export PINP"

    def draw(self, context):

        layout = self.layout

        col = layout.column(align=True)
        col.prop_search(context.scene.PP, "i_moda", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_modb", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_modc", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_pnt1", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_text", bpy.data, "images", text="")
        col.prop(context.scene.PP,"o_file",text="")

        col = layout.column(align=True)
        p = col.operator("scene.ex_pinp", text="Export PINP")
        p.i_text = context.scene.PP.i_text
        p.i_moda = context.scene.PP.i_moda
        p.i_modb = context.scene.PP.i_modb
        p.i_modc = context.scene.PP.i_modc
        p.i_pnt1 = context.scene.PP.i_pnt1
        p.o_file = context.scene.PP.o_file

        col = layout.column(align=True)
        p = col.operator("scene.im_pinp", text="Import PINP")
        p.in_file = context.scene.PP.o_file

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Register

def register():
    #bpy.types.Scene.pinp_in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="Read_File_Name")
    #bpy.types.Scene.pinp_out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="Write_File_Name")
    #bpy.types.Scene.pinp_out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")

    bpy.utils.register_class(PP_props)
    bpy.types.Scene.PP = bpy.props.PointerProperty(type=PP_props)

    bpy.utils.register_module(__name__)

def unregister():

    del bpy.types.Scene.PP
    bpy.utils.unregister_class(PP_props)

    bpy.utils.unregister_module(__name__)

    #del bpy.types.Scene.pinp_in_file
    #del bpy.types.Scene.pinp_out_file
    #del bpy.types.Scene.pinp_out_text

if __name__ == "__main__":
    register()

# ----------------------------------------------------------------




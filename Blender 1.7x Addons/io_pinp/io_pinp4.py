# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# ----------------------------------------------------------------
# Header

bl_info = {
    "name": "Export PINP 4",
    "author": "orgyia",
    "version": (1, 0, 4),
    "blender": (2, 72, 0),
    "location": "3D View -> Properties -> PINP Tool",
    "description": "Misc Tools for pinp files",
    "wiki_url": "",
    "category": "Import-Export"
}

import bpy
import struct

from bpy_extras.object_utils import AddObjectHelper, object_data_add
from bpy.props import FloatVectorProperty
from mathutils import Vector

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# UI Properties

class PP_props(bpy.types.PropertyGroup):
    i_text      = bpy.props.StringProperty(name="Texture", description="Blender Texture to export (Max 256 colors).", default='')
    i_moda      = bpy.props.StringProperty(name="Model", description="Mesh Object to export (Requires UV Mapping).", default='')
    i_modb      = bpy.props.StringProperty(name="MIP 1", description="Mip 1 Mesh Object to export (Must use duplicated verts from Model).", default='')
    i_modc      = bpy.props.StringProperty(name="MIP 2", description="Mip 2 Mesh Object to export (Must use duplicated verts from Model).", default='')
    o_file      = bpy.props.StringProperty(name="Save As", description="Name for the pinp file.", default='filename')

    i_pnt1	= bpy.props.StringProperty(name="Point 1", description="Lights, Flames.", default='')
    i_pnt2	= bpy.props.StringProperty(name="Point 2", description="Lights, Flames.", default='')
    i_pnt3	= bpy.props.StringProperty(name="Point 3", description="Lights, Flames.", default='')
    i_pnt4	= bpy.props.StringProperty(name="Point 4", description="Lights, Flames.", default='')

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Import PINP

class OT_IM_PINP(bpy.types.Operator):
    bl_idname = "scene.im_pinp"
    bl_label = "Import PINP"
    bl_description = "Import PINP"
    bl_options = {'REGISTER', 'UNDO'}

    #im_name = bpy.props.StringProperty(default="FILENAME")
    #pinp = bpy.props.BoolProperty(default=False)
    in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="in_file")

    #out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="out_file")
    #out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")

    def execute(self, context):

        # --------------------------------
        # Read File

        f = open(self.in_file, 'rb')
        data = f.read()
        f.close()

        error = 0
        check = 0

        if error == 0:

            '''
            # --------------------------------
            # Header

            PEN_unknown = int.from_bytes((data[4], data[5]), byteorder='little', signed=False)
            POA_pallet = int.from_bytes((data[6], data[7]), byteorder='little', signed=False)
            POA_model = int.from_bytes((data[8], data[9]), byteorder='little', signed=False)
            POA_uvs = int.from_bytes((data[10], data[11]), byteorder='little', signed=False)
            PEN_uvs = int.from_bytes((data[12], data[13]), byteorder='little', signed=False)
            PSZ_model = int.from_bytes((data[14], data[15]), byteorder='little', signed=False)
            POA_texture = POA_pallet+768
            '''

            # --------------------------------
            # Pinp Data

            #ph_id           = [0x50,0x49,0x4E,0x50]         # PINP
            #ph_zero         = 0                             # 0
            #ph_tex_height   = int.from_bytes((data[5]), byteorder='little', signed=False)
            ph_tex_height   = data[5]
            ph_aa_pallet    = int.from_bytes((data[6], data[7]), byteorder='little', signed=False)
            ph_aa_moda      = int.from_bytes((data[8], data[9]), byteorder='little', signed=False)
            ph_aa_uvs       = int.from_bytes((data[10], data[11]), byteorder='little', signed=False)
            ph_en_uvs       = int.from_bytes((data[12], data[13]), byteorder='little', signed=False)
            #ph_ob_size      = 0                             # not required

            #print('ph_en_uvs: '+str(ph_en_uvs))

            # --------------------------------
            # Load Texture

            aa_texture   = ph_aa_pallet+768

            PNG = bpy.data.images.new(self.in_file+'.tex',128,160,True,False)
            PNG_data = []

            t=0
            c=0

            while(t < (128*160)):
                c = data[aa_texture+t]
                r = data[ph_aa_pallet+(c*3)]
                g = data[ph_aa_pallet+(c*3)+1]
                b = data[ph_aa_pallet+(c*3)+2]
                PNG_data.append(b/255)
                PNG_data.append(g/255)
                PNG_data.append(r/255)
                PNG_data.append(1.0)
                t+=1

            PNG.pixels=PNG_data
            PNG.pack(as_png=True)

            # --------------------------------
            # MODLA Header

            ma_faces        = int.from_bytes((data[ph_aa_moda+6], data[ph_aa_moda+7]), byteorder='little', signed=False)
            ma_verts        = int.from_bytes((data[ph_aa_moda+4], data[ph_aa_moda+5]), byteorder='little', signed=False)
            ma_points       = 0
            ma_uvs          = 0
            ma_texts        = 0

            ma_face_list    = int.from_bytes((data[ph_aa_moda+12], data[ph_aa_moda+13], data[ph_aa_moda+14], data[ph_aa_moda+15]), byteorder='little', signed=False)
            ma_vert_list    = int.from_bytes((data[ph_aa_moda+64], data[ph_aa_moda+65], data[ph_aa_moda+66], data[ph_aa_moda+67]), byteorder='little', signed=False)
            ma_point_list   = 0
            ma_uv_list      = int.from_bytes((data[ph_aa_moda+20], data[ph_aa_moda+21]), byteorder='little', signed=False)

            ma_modb         = int.from_bytes((data[ph_aa_moda+28], data[ph_aa_moda+29]), byteorder='little', signed=False)
            ma_modc         = int.from_bytes((data[ph_aa_moda+30], data[ph_aa_moda+31]), byteorder='little', signed=False)
            ma_xx1          = 0
            ma_xx2          = 0
            ma_xx3          = 0

            # --------------------------------
            # MODLB Header

            mb_faces        = int.from_bytes((data[ph_aa_moda+ma_modb+6], data[ph_aa_moda+ma_modb+7]), byteorder='little', signed=False)
            mb_verts        = int.from_bytes((data[ph_aa_moda+ma_modb+4], data[ph_aa_moda+ma_modb+5]), byteorder='little', signed=False)
            mb_points       = 0
            mb_uvs          = 0
            mb_texts        = 0

            mb_face_list    = int.from_bytes((data[ph_aa_moda+ma_modb+12], data[ph_aa_moda+ma_modb+13], data[ph_aa_moda+ma_modb+14], data[ph_aa_moda+ma_modb+15]), byteorder='little', signed=False)
            mb_vert_list    = int.from_bytes((data[ph_aa_moda+ma_modb+64], data[ph_aa_moda+ma_modb+65], data[ph_aa_moda+ma_modb+66], data[ph_aa_moda+ma_modb+67]), byteorder='little', signed=False)
            mb_point_list   = 0
            mb_uv_list      = int.from_bytes((data[ph_aa_moda+ma_modb+20], data[ph_aa_moda+ma_modb+21]), byteorder='little', signed=False)

            mb_modb         = 0
            mb_modc         = 0
            mb_xx1          = 0
            mb_xx2          = 0
            mb_xx3          = 0

            # --------------------------------
            # MODLC Header

            mc_faces        = int.from_bytes((data[ph_aa_moda+ma_modc+6], data[ph_aa_moda+ma_modc+7]), byteorder='little', signed=False)
            mc_verts        = int.from_bytes((data[ph_aa_moda+ma_modc+4], data[ph_aa_moda+ma_modc+5]), byteorder='little', signed=False)
            mc_points       = 0
            mc_uvs          = 0
            mc_texts        = 0

            mc_face_list    = int.from_bytes((data[ph_aa_moda+ma_modc+12], data[ph_aa_moda+ma_modc+13], data[ph_aa_moda+ma_modc+14], data[ph_aa_moda+ma_modc+15]), byteorder='little', signed=False)
            mc_vert_list    = int.from_bytes((data[ph_aa_moda+ma_modc+64], data[ph_aa_moda+ma_modc+65], data[ph_aa_moda+ma_modc+66], data[ph_aa_moda+ma_modc+67]), byteorder='little', signed=False)
            mc_point_list   = 0
            mc_uv_list      = int.from_bytes((data[ph_aa_moda+ma_modc+20], data[ph_aa_moda+ma_modc+21]), byteorder='little', signed=False)

            mc_modb         = 0
            mc_modc         = 0
            mc_xx1          = 0
            mc_xx2          = 0
            mc_xx3          = 0

            # --------------------------------
            # Print Check

            if check == 0:

                print('')
                print('MODLA')
                print('Verts: '+str(ma_verts))
                print('Faces: '+str(ma_faces))
                print('MODLA -> Faces: '+hex(ma_face_list))
                print('MODLA -> Verts: '+hex(ma_vert_list))
                print('MODLA -> UVMap: '+hex(ma_uv_list))
                print('MODLA -> MODLB: '+hex(ma_modb))
                print('MODLA -> MODLC: '+hex(ma_modc))

                print('')
                print('MODLB')
                print('Verts: '+str(mb_verts))
                print('Faces: '+str(mb_faces))
                print('MODLB -> Faces: '+hex(mb_face_list))
                print('MODLA -> UVMap: '+hex(mb_uv_list))

                print('')
                print('MODLC')
                print('Verts: '+str(mc_verts))
                print('Faces: '+str(mc_faces))
                print('MODLC -> Faces: '+hex(mc_face_list))
                print('MODLA -> UVMap: '+hex(mc_uv_list))

            # --------------------------------
            # Model A

            bverts = []
            bfaces = []
            bedges = []

            Face_List = ph_aa_moda+ma_face_list
            Vert_List = ph_aa_moda+ma_vert_list

            # Faces

            for f in range(ma_faces):
                if data[Face_List+(f*8)+3] == 0:
                    bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1])
                else:
                    bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1, data[Face_List+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(ma_verts):
                vec1 = int.from_bytes((data[Vert_List+(v*8)], data[Vert_List+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[Vert_List+(v*8)+2], data[Vert_List+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[Vert_List+(v*8)+4], data[Vert_List+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            # --------------------------------
            # UV A

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+ma_uv_list+uvid+0], data[ph_aa_moda+ma_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[ph_aa_moda+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[ph_aa_moda+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+ma_uv_list+uvid+0], data[ph_aa_moda+ma_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(ma_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # --------------------------------
            # Model B

            bverts = []
            bfaces = []
            bedges = []

            Face_List = ph_aa_moda+ma_modb+mb_face_list
            Vert_List = ph_aa_moda+ma_vert_list

            # Faces

            for f in range(mb_faces):
                if data[Face_List+(f*8)+3] == 0:
                    bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1])
                else:
                    bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1, data[Face_List+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(ma_verts):
                vec1 = int.from_bytes((data[Vert_List+(v*8)], data[Vert_List+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[Vert_List+(v*8)+2], data[Vert_List+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[Vert_List+(v*8)+4], data[Vert_List+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            context.active_object.location[2] += 40

            # --------------------------------
            # UV B

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+mb_uv_list+uvid+0], data[ph_aa_moda+mb_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[ph_aa_moda+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[ph_aa_moda+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+mb_uv_list+uvid+0], data[ph_aa_moda+mb_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(mb_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # --------------------------------
            # Model C

            bverts = []
            bfaces = []
            bedges = []

            Face_List = ph_aa_moda+ma_modc+mc_face_list
            Vert_List = ph_aa_moda+ma_vert_list

            # Faces

            for f in range(mc_faces):
                if data[Face_List+(f*8)+3] == 0:
                    bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1])
                else:
                    bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1, data[Face_List+(f*8)+3]-1])
            #print(bfaces)

            # Verts

            for v in range(ma_verts):
                vec1 = int.from_bytes((data[Vert_List+(v*8)], data[Vert_List+(v*8)+1]), byteorder='little', signed=True)
                vec2 = int.from_bytes((data[Vert_List+(v*8)+2], data[Vert_List+(v*8)+3]), byteorder='little', signed=True)
                vec3 = int.from_bytes((data[Vert_List+(v*8)+4], data[Vert_List+(v*8)+5]), byteorder='little', signed=True)
                # Rollcage Orientation
                #bverts.append(Vector((vec1,vec2,vec3)))
                # Blender Orientation
                bverts.append(Vector((-vec1,-vec3,-vec2)))
            #print(bverts)

            # Add Mesh

            mesh = bpy.data.meshes.new(name=self.in_file)
            mesh.from_pydata(bverts, bedges, bfaces)
            object_data_add(context, mesh, operator=None)

            context.active_object.location[2] += 80

            # --------------------------------
            # UV C

            bpy.ops.mesh.uv_texture_add()
            uvt = context.active_object.data.uv_textures.active
            mat = bpy.data.materials.new('MT')
            context.active_object.data.materials.append(mat)
            tex = bpy.data.textures.new('TT', type='IMAGE')
            tex_slot = mat.texture_slots.add()
            tex_slot.texture = tex
            tex_slot.uv_layer = uvt.name
            tex_slot.texture.image = PNG

            lid = 0
            uvid = 0

            for p in context.active_object.data.polygons:

                if p.loop_total == 4:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+mc_uv_list+uvid+0], data[ph_aa_moda+mc_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    layer.data[lid+3].uv[0] = (data[ph_aa_moda+OA_uvo+10] +0.5)/128
                    layer.data[lid+3].uv[1] = (data[ph_aa_moda+OA_uvo+11] +0.5)/160
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:
                    OA_uvo = int.from_bytes((data[ph_aa_moda+mc_uv_list+uvid+0], data[ph_aa_moda+mc_uv_list+uvid+1]), byteorder='little', signed=False)
                    layer = context.active_object.data.uv_layers.active
                    layer.data[lid].uv[0] = (data[ph_aa_moda+OA_uvo+4] +0.5)/128
                    layer.data[lid].uv[1] = (data[ph_aa_moda+OA_uvo+5] +0.5)/160
                    layer.data[lid+1].uv[0] = (data[ph_aa_moda+OA_uvo+0] +0.5)/128
                    layer.data[lid+1].uv[1] = (data[ph_aa_moda+OA_uvo+1] +0.5)/160
                    layer.data[lid+2].uv[0] = (data[ph_aa_moda+OA_uvo+8] +0.5)/128
                    layer.data[lid+2].uv[1] = (data[ph_aa_moda+OA_uvo+9] +0.5)/160
                    lid+=3
                    uvid+=2

            for f in range(mc_faces):
                context.active_object.data.uv_textures.active.data[f].image = PNG

            # Funny Fact
            # The shared verts are added in reverse order, MODLC, B, A
            # Thus to add only used verts, add the number of MODL verts
            # To add all verts for each model, add the number of MODLA verts

        # --------------------------------
        # Finished

        return {'FINISHED'}

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Export PINP

class OT_EX_PINP(bpy.types.Operator):
    bl_idname = "scene.ex_pinp"
    bl_label = "Export PINP"
    bl_description = "Export PINP"
    bl_options = {'REGISTER', 'UNDO'}

    # Texture
    # Model 0 object
    # Model 1 object
    # Model 2 object
    # Empty objects
    # Custom Properties

    i_text      = bpy.props.StringProperty(name="Texture", description="Blender Texture to export (Max 256 colors).", default='')
    i_moda      = bpy.props.StringProperty(name="Model", description="Mesh Object to export (Requires UV Mapping).", default='')
    i_modb      = bpy.props.StringProperty(name="MIP 1", description="Mip 1 Mesh Object to export (Must use duplicated verts from Model).", default='')
    i_modc      = bpy.props.StringProperty(name="MIP 2", description="Mip 2 Mesh Object to export (Must use duplicated verts from Model).", default='')
    o_file      = bpy.props.StringProperty(name="Save As", description="Name for the pinp file.", default='filename')

    i_pnt1	= bpy.props.StringProperty(name="Point 1", description="Lights, Flames.", default='')
    i_pnt2	= bpy.props.StringProperty(name="Point 2", description="Lights, Flames.", default='')
    i_pnt3	= bpy.props.StringProperty(name="Point 3", description="Lights, Flames.", default='')
    i_pnt4	= bpy.props.StringProperty(name="Point 4", description="Lights, Flames.", default='')

    def execute(self, context):

        # --------------------------------
        # XX1 Data

        XX1A = [0] * 12
        XX1B = [0] * 18
        XX1C = [0] * 2

        # These are the XX1 data streams.
        # The code will export these to each model.
        # 
        # Im not sure whats its for, but I think its something to do with face allocation.
        # 
        # Each model has a number shorts.
        # Each short adds a number of faces to the model to be rendered.
        # 
        # To little and the model will have missing faces.
        # To many and the car will render extra tris and quads with random coordinates.
        # 
        # Also, the format can only use a specific set of hex digits.
        # I think each hex digit has a set of 9 (could be more) unique numbers.
        #
        # For example, for XX1A, below is the known hex digits for the UGENESIS 057.
        # nibble 1: 1- 4- 8-
        # nibble 2: -2 -3 -A -B -E
        #
        # After looking at some files, these are known digits.
        # nibble 1: 1- 2- -- 4- 5- 6- -- 8- 9- -- -- -- D- E- --
        # nibble 2: -1 -2 -3 -4 -- -- -- -- -9 -A -B -- -- -E -F

        #XX1A

        XX1A[0] = 0x12
        XX1A[1] = 0x4B
        XX1A[2] = 0x5A
        XX1A[3] = 0x13       #5 to 1
        XX1A[4] = 0x13
        XX1A[5] = 0x43
        XX1A[6] = 0x83
        XX1A[7] = 0x12
        XX1A[8] = 0x4B
        XX1A[9] = 0x8B
        XX1A[10] = 0x12
        XX1A[11] = 0x1E

        '''
        XX1A[0] = 0x12
        XX1A[1] = 0x4B
        XX1A[2] = 0x1A
        XX1A[3] = 0x13
        XX1A[4] = 0x13
        XX1A[5] = 0x43
        XX1A[6] = 0x83
        XX1A[7] = 0x12
        XX1A[8] = 0x4B
        XX1A[9] = 0x8B
        XX1A[10] = 0x12
        XX1A[11] = 0x1E
        '''

        #XX1B

        XX1B[0] = 0x12
        XX1B[1] = 0x9
        XX1B[2] = 0x9
        XX1B[3] = 0xA
        XX1B[4] = 0x1A
        XX1B[5] = 0x13
        XX1B[6] = 0x13
        XX1B[7] = 0x1
        XX1B[8] = 0x1
        XX1B[9] = 0x1A
        XX1B[10] = 0x2A
        XX1B[11] = 0x12
        XX1B[12] = 0x9
        XX1B[13] = 0x1A
        XX1B[14] = 0x2A
        XX1B[15] = 0x12
        XX1B[16] = 0x1E
        XX1B[17] = 0x0

        #XX1C

        XX1C[0] = 0x2E
        XX1C[1] = 0x0

        # --------------------------------
        # Error Check

        error = 0

        if self.i_text == "":
            error = 1
            print('error: No Image Texture selected')
            self.report({"ERROR"},"No Image Texture selected, select a blender image to export.")

        if error == 0:

            # --------------------------------
            # Pinp Data

            ph_id           = [0x50,0x49,0x4E,0x50]         # fixed
            ph_zero         = 0                             # fixed
            ph_tex_height   = 128+32                        # fixed
            ph_aa_pallet    = 0x10                          # fixed
            ph_aa_moda      = 0x5310                        # fixed
            ph_aa_uvs       = 0                             # can calc after shared verts
            ph_en_uvs       = 0                             # can calc whenever
            ph_ob_size      = 0                             # can calc after all

            # We can now export header, colors, points, moda header.
            # But before that, data for the header is required (uv address, ob size etc).
            # To calculate this, its best to check the mesh.

            # --------------------------------
            # Mesh Check

            ma_me           = context.scene.objects[self.i_moda].data
            mb_me           = context.scene.objects[self.i_modb].data
            mc_me           = context.scene.objects[self.i_modc].data

            # This is a list of data we need to know for the moda header
            # data		size	address		info
            # faces		calc	calc		header -> 0x50
            # xxx		fixed	calc		header -> xxx
            # mips		calc	calc		header -> mips
            # verts		calc	calc		header -> verts
            # points		check	calc		header -> points
            # uv		calc	calc		header -> uvs

            # --------------------------------
            # Header

            #ma_aa_header    = 0        # ph_aa_moda

            ma_faces        = 0
            ma_verts        = 0
            ma_points       = 0
            ma_uvs          = 0
            ma_texts        = 0

            ma_face_list    = 0
            ma_vert_list    = 0
            ma_point_list   = 0
            ma_uv_list      = 0

            ma_modb         = 0
            ma_modc         = 0
            ma_xx1          = 0
            ma_xx2          = 0
            ma_xx3          = 0


            mb_faces        = 0
            mb_verts        = 0
            mb_points       = 0
            mb_uvs          = 0
            mb_texts        = 0

            mb_face_list    = 0
            mb_vert_list    = 0
            mb_point_list   = 0
            mb_uv_list      = 0

            mb_modb         = 0
            mb_modc         = 0
            mb_xx1          = 0
            mb_xx2          = 0
            mb_xx3          = 0


            mc_faces        = 0
            mc_verts        = 0
            mc_points       = 0
            mc_uvs          = 0
            mc_texts        = 0

            mc_face_list    = 0
            mc_vert_list    = 0
            mc_point_list   = 0
            mc_uv_list      = 0

            mc_modb         = 0
            mc_modc         = 0
            mc_xx1          = 0
            mc_xx2          = 0
            mc_xx3          = 0


            # --------------------------------
            # Sizes

            for v in ma_me.vertices:
                ma_verts += 1

            for p in ma_me.polygons:
                ma_faces += 1
                ma_texts += p.loop_total

            for v in mb_me.vertices:
                mb_verts += 1

            for p in mb_me.polygons:
                mb_faces += 1
                mb_texts += p.loop_total

            for v in mc_me.vertices:
                mc_verts += 1

            for p in mc_me.polygons:
                mc_faces += 1
                mc_texts += p.loop_total

            print('ma_faces: '+str(ma_faces))
            print('mb_faces: '+str(mb_faces))
            print('mc_faces: '+str(mc_faces))

            # --------------------------------
            # Addresses

            ma_face_list    = 0x50
            ma_xx1          = ma_face_list + (ma_faces*8)
            ma_xx2          = ma_xx1 + 24
            ma_xx3          = ma_xx2 + 512

            ma_modb         = ma_xx3 + 512

            mb_face_list    = 0x50
            mb_xx1          = mb_face_list + (mb_faces*8)
            mb_xx2          = mb_xx1 + 36
            mb_xx3          = mb_xx2 + 512

            ma_modc         = ma_modb + mb_xx3 + 512

            mc_face_list    = 0x50
            mc_xx1          = mc_face_list + (mc_faces*8)
            mc_xx2          = mc_xx1 + 4
            mc_xx3          = mc_xx2 + 4

            ma_vert_list    = ma_modc + mc_xx3 + 240
            ma_point_list   = ma_modc + mc_xx3 + 240 + (ma_verts*8)	# add verts

            ma_uv_list      = ma_point_list + (8*40)                    # 40 points, includes first 4
            mb_uv_list      = ma_uv_list + (ma_faces*2)
            mc_uv_list      = mb_uv_list + (mb_faces*2)

            ph_aa_uvs       = ph_aa_moda + mc_uv_list + (mc_faces*2)
            ph_en_uvs       = ma_faces + mb_faces + mc_faces

            ph_ob_size      = ( ph_aa_uvs + ((ma_faces + mb_faces + mc_faces)*12) ) - ph_aa_moda

            # could add points and uv here but first
            # may be best to try make pinp file then compare data

            # --------------------------------
            # Texture Data

            im = bpy.data.images[self.i_text]
            px = im.pixels[:]
            check = [0] * 61440
            color = [0] * 61440
            point = [0] * 20480

            colors = 0
            t=0

            t = 0
            while(t < (128*160)):

                check[(t*3)]   = int(px[(t*4)+2]*255)
                check[(t*3)+1] = int(px[(t*4)+1]*255)
                check[(t*3)+2] = int(px[(t*4)]*255)

                t += 1

            t = 0
            while(t < ((128*160)*3)):

                a = check[(t)]
                a &= 128
                b = check[(t)]
                b &= 4

                if a == 0 and b == 4:
                    check[(t)] -= 4

                if a == 128 and b == 0:
                    check[(t)] += 4

                t += 1

            # loop colors
            # if previous color, skip
            # if new color, add color

            t = 0
            while(t < (128*160)):

                r = check[(t*3)]
                g = check[(t*3)+1]
                b = check[(t*3)+2]

                new_color = 1
                scan = 0

                while(scan < 256 and scan < colors):

                    if color[(scan*3)] == r and color[(scan*3)+1] == g and color[(scan*3)+2] == b:
                        new_color = 0
                        scan = 256

                    scan += 1

                if colors > 256:
                    error = 2
                    t = 20480
                    print('check error: too many colors')
                    self.report({"ERROR"},"Too many colors, 256 max, try reducing colors.")

                if new_color == 1 and error == 0:
                    color[((colors)*3)] = r
                    color[((colors)*3)+1] = g
                    color[((colors)*3)+2] = b
                    colors += 1

                t += 1

            if error == 2:
                print('Too many colors.')
                self.report({"ERROR"},"Too many colors, 256 max, try reducing colors.")

            if error == 0:

                t = 0
                while(t < (128*160)):

                    r = check[(t*3)]
                    g = check[(t*3)+1]
                    b = check[(t*3)+2]

                    for i in range(colors):
                        if color[(i*3)] == r and color[(i*3)+1] == g and color[(i*3)+2] == b:
                            point[t] = i

                    t += 1

            # Added below

            '''
            if error == 0:

                f = open(self.out_file, 'wb')

                for i in range(256*3):
                    f.write(struct.pack("B", color[i]))

                for i in range(128*160):
                    f.write(struct.pack("B", point[i]))

                f.close()
            '''

            # --------------------------------
            # Write File

            #f = open(self.o_file+'.pinp', 'wb')
            #f.write(bytearray(xxxx))
            #f.close()

            '''
            ph_id           = [0x50,0x49,0x4E,0x50]         # fixed
            ph_zero         = 0                             # fixed
            ph_tex_height   = 128+32                        # fixed
            ph_aa_pallet    = 0x10                          # fixed
            ph_aa_moda      = 0x5310                        # fixed
            ph_aa_uvs       = 0                             # can calc after shared verts
            ph_en_uvs       = 0                             # can calc whenever
            ph_ob_size      = 0                             # can calc after all
            '''

            '''
            Silly Bytes
            >>> bb = xx.to_bytes(length=2,byteorder='little',signed=True)
            b'\xfd\x02'
            >>> bb[
                   0]
                   1]
            '''

            f = open(self.o_file+'.pinp', 'wb')

            # Header

            f.write(bytearray(ph_id))
            f.write(bytearray(ph_zero.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(ph_tex_height.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(ph_aa_pallet.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_aa_moda.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_aa_uvs.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_en_uvs.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ph_ob_size.to_bytes(length=2,byteorder='little',signed=False)))

            # I will have to start writing data now
            # Color and point tables need to be calculated above
            # Then the preallocated arrays can be written to the file

            # colors and points

            for i in range(256*3):
                f.write(struct.pack("B", color[i]))

            for i in range(128*160):
                f.write(struct.pack("B", point[i]))

            # K
            # Time to write the moda header
            # Most data should be correct from the code above

            # --------------------------------
            # MODA Header

            data_char = [0x4D,0x4F,0x44,0x4C]
            data_int = 1

            f.write(bytearray(data_char))
            f.write(bytearray(ma_verts.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ma_faces.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_face_list.to_bytes(length=4,byteorder='little',signed=False)))

            f.write(bytearray(ma_xx1.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_uv_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_xx2.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_modb.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(ma_modc.to_bytes(length=2,byteorder='little',signed=False)))

            # zeros

            data_int = 0
            for i in range(16):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            for i in range(8):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            data_int = 0
            f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            # Texture RAM Address
            #data_int = 0x9234       # 057
            #data_int = 0x9224       # Works but textures upside down
                                     # Found this probably the face normals
                                     # 0x91F4 worked after deleting 6 faces, which is -0x30
            data_int = 0x9224
            f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 0
            f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            # verts, xx3, zero, points

            data_int = 0
            f.write(bytearray(ma_vert_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(ma_xx3.to_bytes(length=4,byteorder='little',signed=False)))
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(ma_point_list.to_bytes(length=4,byteorder='little',signed=False)))

            # Ive done the moda header for now
            # Some data is odd, like the address to shared points
            # But its ok for now
            # Next is the faces, then the xx formats

            # Faces are 8 bytes V1 V2 V3 V4, then 4 bytes of unknown data
            # The unknown data can be zero for now

            # MODA Data can be exported like below
            # But I need to stack faces, to check for old and new for MODB Data later

            '''
            moda_upoly = [0] * (ph_en_uvs+100)          # A modu face and uv ids
            modb_upoly = [0] * (ph_en_uvs+100)          # B modu face and uv ids
            modc_upoly = [0] * (ph_en_uvs+100)          # C modu face and uv ids
            '''

            # in blender, each model has a set of faces, but the ids are wrong
            # i wonder faces with same ids exist, but different uv coords
            #   yes these do exist, botton right of fron air intake
            #   model polygon 41	18 02 17 0F 00 00 04 00
            #   mip 1 polygon 30	18 02 17 0F 00 00 04 00
            #   yep, just different uv choords

            # so when comparing faces, also need to compare uv coords
            # uv choords checked via uv layer polygon loop

            # so for each face, need to make face list + uv coord list
            # hmm
            # may be able to get away with this
            # even though uvs are different

            # each model has the same ammount of faces and uvs offsets
            # the faces point to the same faces
            # the uvs point to different uv choords

            # just try export faces for now
            # shared uvs should be able to be solved later

            # k
            # export faces, thats all i need to do, faces and uv offsets are unique
            # shared uvs are different, these can stacked and checked later

            # --------------------------------
            # MODA Data

            for p in ma_me.polygons:
                if p.loop_total == 3:
                    data_int = p.vertices[1]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[2]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[0]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = 0
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                if p.loop_total == 4:
                    data_int = p.vertices[1]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[2]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[0]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[3]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0xCB
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0x0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0xCB
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0x0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 1
            for i in range(12):
                data_int = XX1A[i]
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 2
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 3
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # MODB Header

            # need to change all below
            # I will have to check whats an absolute and offset address, shared verts address for example
            # but for now just change all ma_ to mb_, and change xx sizes
            # shared data, verts and uva are added after the models

            data_char = [0x4D,0x4F,0x44,0x4C]
            data_int = 1

            f.write(bytearray(data_char))
            f.write(bytearray(mb_verts.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mb_faces.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_face_list.to_bytes(length=4,byteorder='little',signed=False)))

            f.write(bytearray(mb_xx1.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_uv_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_xx2.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_modb.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mb_modc.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 0
            for i in range(32):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            f.write(bytearray(mb_vert_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mb_xx3.to_bytes(length=4,byteorder='little',signed=False)))
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(mb_point_list.to_bytes(length=4,byteorder='little',signed=False)))

            # --------------------------------
            # MODB Data

            for p in mb_me.polygons:
                if p.loop_total == 3:
                    data_int = p.vertices[1]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[2]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[0]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = 0
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                if p.loop_total == 4:
                    data_int = p.vertices[1]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[2]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[0]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[3]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 1
            for i in range(18):
                data_int = XX1B[i]
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 2
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 3
            for i in range(512):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # MODC Header

            # need to change all below
            # I will have to check whats an absolute and offset address, shared verts address for example
            # but for now just change all ma_ to mb_, and change xx sizes
            # shared data, verts and uva are added after the models

            data_char = [0x4D,0x4F,0x44,0x4C]
            data_int = 1

            f.write(bytearray(data_char))
            f.write(bytearray(mc_verts.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mc_faces.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(data_int.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_face_list.to_bytes(length=4,byteorder='little',signed=False)))

            f.write(bytearray(mc_xx1.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_uv_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_xx2.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_modb.to_bytes(length=2,byteorder='little',signed=False)))
            f.write(bytearray(mc_modc.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 0
            for i in range(32):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            f.write(bytearray(mc_vert_list.to_bytes(length=4,byteorder='little',signed=False)))
            f.write(bytearray(mc_xx3.to_bytes(length=4,byteorder='little',signed=False)))
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            f.write(bytearray(mc_point_list.to_bytes(length=4,byteorder='little',signed=False)))

            # --------------------------------
            # MODC Data

            for p in mc_me.polygons:
                if p.loop_total == 3:
                    data_int = p.vertices[1]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[2]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[0]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = 0
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                if p.loop_total == 4:
                    data_int = p.vertices[1]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[2]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[0]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                    data_int = p.vertices[3]+1
                    f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 1
            for i in range(2):
                data_int = XX1C[i]
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            data_int = 2
            for i in range(4):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            data_int = 3
            for i in range(240):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # Shared Data

            # Verts, UV Map, check bmo
            # int(float * 10)
            # Verts are signed shorts xxyyzz--

            # --------------------------------
            # Verts

            for v in ma_me.vertices:				# verts

                '''
                f.write(struct.pack("f", v.co[0]))		# X
                f.write(struct.pack("f", v.co[1]))		# Y
                f.write(struct.pack("f", v.co[2]))		# Z
                '''

                x = int(-v.co[0])
                y = int(-v.co[2])
                z = int(-v.co[1])
                p = int(0)

                '''
                #  can divide above inside the local shit ()
                x /= 10.0
                y /= 10.0
                z /= 10.0
                p /= 10.0
                '''

                f.write(bytearray(x.to_bytes(length=2,byteorder='little',signed=True)))
                f.write(bytearray(y.to_bytes(length=2,byteorder='little',signed=True)))
                f.write(bytearray(z.to_bytes(length=2,byteorder='little',signed=True)))
                f.write(bytearray(p.to_bytes(length=2,byteorder='little',signed=True)))

            # --------------------------------
            # UV Map

            # Best to verify data for a bit
            # Need to stack shared uv coords after

            # Data looks good so far, can import first modl

            # k
            # need to stack a list of uv coords and retain face ids
            # loop polies
            #   for each face, save uv coords
            # moda_uv_coords = [[xyxyxyxy],[xyxyxyxy]]

            # save all shared uv coords
            # all modls have to be checked first, or stack as processed

            # loop polies, stack new uvs, for each poly keep uvid, id to shared uv
            # shared_uv = [xyxyxyxy,xyxyxyxy,xyxyxyxy,xyxyxyxy,xyxyxyxy,xyxyxyxy,..]
            # moda_uvid = [1,2,3,4,5,2,3,4,5,6,7,8,9,7,8,9] 
            # modb_uvid = [1,2,3,4,5,2,3,4,5,a,b,c,d]
            # modc_uvid = [a,b,c,d,e,f]

            # we dont want loop total problems, so new uvs will be zero padded, ready to export when added to list

            moda_uvid = [0] * (ma_faces)
            modb_uvid = [0] * (mb_faces)
            modc_uvid = [0] * (mc_faces)

            uv_coords = []

            #bfaces.append([data[Face_List+(f*8)+2]-1, data[Face_List+(f*8)+0]-1, data[Face_List+(f*8)+1]-1, data[Face_List+(f*8)+3]-1])

            '''
            for p in ma_me.polygons:
                ma_faces += 1
                ma_texts += p.loop_total
            '''

            pid = 0         # polygon id

            # MODA

            uvs = 0         # uv
            lid = 0         # layer id
            uvid = 0        # uv id

            data_int = 0
            for p in ma_me.polygons:

                if p.loop_total == 4:

                    layer = ma_me.uv_layers.active
                    uv_coords.append([layer.data[lid+1].uv[0],layer.data[lid+1].uv[1],layer.data[lid].uv[0],layer.data[lid].uv[1],layer.data[lid+2].uv[0],layer.data[lid+2].uv[1],layer.data[lid+3].uv[0],layer.data[lid+3].uv[1]])
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:

                    layer = ma_me.uv_layers.active
                    uv_coords.append([layer.data[lid+1].uv[0],layer.data[lid+1].uv[1],layer.data[lid].uv[0],layer.data[lid].uv[1],layer.data[lid+2].uv[0],layer.data[lid+2].uv[1],data_int,data_int])
                    lid+=3
                    uvid+=2

                moda_uvid[uvs] = pid
                pid += 1
                uvs += 1

            # MODB

            uvs = 0         # uv
            lid = 0         # layer id
            uvid = 0        # uv id

            data_int = 0
            for p in mb_me.polygons:

                if p.loop_total == 4:

                    layer = mb_me.uv_layers.active
                    uv_coords.append([layer.data[lid+1].uv[0],layer.data[lid+1].uv[1],layer.data[lid].uv[0],layer.data[lid].uv[1],layer.data[lid+2].uv[0],layer.data[lid+2].uv[1],layer.data[lid+3].uv[0],layer.data[lid+3].uv[1]])
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:

                    layer = mb_me.uv_layers.active
                    uv_coords.append([layer.data[lid+1].uv[0],layer.data[lid+1].uv[1],layer.data[lid].uv[0],layer.data[lid].uv[1],layer.data[lid+2].uv[0],layer.data[lid+2].uv[1],data_int,data_int])
                    lid+=3
                    uvid+=2

                modb_uvid[uvs] = pid
                pid += 1
                uvs += 1

            # MODC

            uvs = 0         # uv
            lid = 0         # layer id
            uvid = 0        # uv id

            data_int = 0
            for p in mc_me.polygons:

                if p.loop_total == 4:

                    layer = mc_me.uv_layers.active
                    uv_coords.append([layer.data[lid+1].uv[0],layer.data[lid+1].uv[1],layer.data[lid].uv[0],layer.data[lid].uv[1],layer.data[lid+2].uv[0],layer.data[lid+2].uv[1],layer.data[lid+3].uv[0],layer.data[lid+3].uv[1]])
                    lid+=4
                    uvid+=2

                if p.loop_total == 3:

                    layer = mc_me.uv_layers.active
                    uv_coords.append([layer.data[lid+1].uv[0],layer.data[lid+1].uv[1],layer.data[lid].uv[0],layer.data[lid].uv[1],layer.data[lid+2].uv[0],layer.data[lid+2].uv[1],data_int,data_int])
                    lid+=3
                    uvid+=2

                modc_uvid[uvs] = pid
                pid += 1
                uvs += 1

            print('')
            print('pid')
            print(pid)

            #for i in range(ma_faces):
            #    print('uvs:'+str(uv_coords[i][0])+' '+str(uv_coords[i][1])+' '+str(uv_coords[i][2])+' '+str(uv_coords[i][3])+' '+str(uv_coords[i][4])+' '+str(uv_coords[i][5])+' '+str(uv_coords[i][6])+' '+str(uv_coords[i][7]))

            #print('')
            #print('moda_uvid')
            #print(moda_uvid)

            #print('')
            #print('uv_coords')
            #print(uv_coords)

            # k this is a straight export
            # this could be done for other models also
            # can save space later by sharing uvs with same coords, but dont have to
            # moda_uvid are the offsets
            # uv_coords are the uv coords

            # uv coords are added after the verts, require address (ma_uv_list)
            # first, write 4 8 bit zero points

            # changed 4 to 40, 40 points

            # --------------------------------
            # 40 Points

            data_int = 0
            for i in range(8*40):
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            # --------------------------------
            # UV Map

            '''
            for i in range(ma_faces):
                f.write(bytearray(moda_uvid[i].to_bytes(length=2,byteorder='little',signed=False)))

            for i in range(mb_faces):
                f.write(bytearray(modb_uvid[i].to_bytes(length=2,byteorder='little',signed=False)))

            for i in range(mc_faces):
                f.write(bytearray(modc_uvid[i].to_bytes(length=2,byteorder='little',signed=False)))
            '''

            # The above code writes id numbers, need to write offsets from MODA
            # The UV Offsets are unsigned shorts so UV offset addres +2*id
            # ma_uv_list
            # be nice to have an address to each modl header

            '''
            # addresses need to be moda to uv coords
            for i in range(ma_faces):
                data_int = ma_uv_list + (moda_uvid[i]*2)
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            for i in range(mb_faces):
                data_int = ma_uv_list + (modb_uvid[i]*2)
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            for i in range(mc_faces):
                data_int = ma_uv_list + (modc_uvid[i]*2)
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))
            '''

            for i in range(ma_faces):
                data_int = (ph_aa_uvs - ph_aa_moda) + (moda_uvid[i]*12)
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            for i in range(mb_faces):
                data_int = (ph_aa_uvs - ph_aa_moda) + (modb_uvid[i]*12)
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            for i in range(mc_faces):
                data_int = (ph_aa_uvs - ph_aa_moda) + (modc_uvid[i]*12)
                f.write(bytearray(data_int.to_bytes(length=2,byteorder='little',signed=False)))

            # --------------------------------
            # UV Coords

            # need to convert floats (0.000 - 1.000), to rollcage ints
            # fill with 'C's, for now

            # X (-0.5)*128
            # Y (-0.5)*160

            for i in range(pid):
                data_int = int((uv_coords[i][0]*128))
                print('data_int0: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][1]*160))
                print('data_int1: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int((uv_coords[i][2]*128))
                print('data_int2: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][3]*160))
                print('data_int3: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int((uv_coords[i][4]*128))
                print('data_int4: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][5]*160))
                print('data_int5: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int((uv_coords[i][6]*128))
                print('data_int6: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][7]*160))
                print('data_int7: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

            '''
            # precision is off, float to int conversion may be the problem
            # try remove -0.5
            for i in range(pid):
                data_int = int((uv_coords[i][0]*128)-0.5)
                print('data_int0: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][1]*160)-0.5)
                print('data_int1: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int((uv_coords[i][2]*128)-0.5)
                print('data_int2: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][3]*160)-0.5)
                print('data_int3: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int((uv_coords[i][4]*128)-0.5)
                print('data_int4: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][5]*160)-0.5)
                print('data_int5: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int((uv_coords[i][6]*128)-0.5)
                print('data_int6: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int((uv_coords[i][7]*160)-0.5)
                print('data_int7: '+str(data_int))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            '''

            '''
            # Floats
            for i in range(pid):
                data_int = int(uv_coords[i][0])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int(uv_coords[i][1])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int(uv_coords[i][2])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int(uv_coords[i][3])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int(uv_coords[i][4])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int(uv_coords[i][5])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = int(uv_coords[i][6])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = int(uv_coords[i][7])
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            '''

            '''
            # C's
            for i in range(pid):
                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))

                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
                data_int = 0xCC
                f.write(bytearray(data_int.to_bytes(length=1,byteorder='little',signed=False)))
            '''

            f.close()

        # --------------------------------
        # Finished

        return {'FINISHED'}

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Decompile BLP

# This is an attempt to dcompil the BLP to seperate files

class OT_DEC_BLP(bpy.types.Operator):
    bl_idname = "scene.dec_blp"
    bl_label = "Decompile BLP"
    bl_description = "Decompile BLP"
    bl_options = {'REGISTER', 'UNDO'}

    in_file = bpy.props.StringProperty(name="In File", description="File to read", default="in_file")

    def execute(self, context):

        # --------------------------------
        # Read File

        # --------------------------------
        # Read File

        f = open(self.in_file, 'rb')
        data = f.read()
        f.close()

        error = 0
        check = 0

        # The BLP is a set of data formats, each one requires an address and size
        # This codes purpose is to decompile the BLP to seperate files

        # The bools below are used to debug, save_all should be replaced later
        # But can be used for new code for now

        save_all = 1

        save_gfxm_header = 1
        save_gfxm_segm_dir = 1
        save_gfxm_modl_dir = 1

        save_segm_faces = 1
        save_segm_verts = 1

        if error == 0:

            # --------------------------------
            # Header Check

            print('--------------------------------')
            if data[0] == 0x47 and data[1] == 0x46 and data[2] == 0x58 and data[3] == 0x4D:

                # --------------------------------
                # Save GFXM Header

                if save_gfxm_header == 1:

                    bb = [0] * (140)

                    for i in range(140):

                        bb[i] = data[i]

                    f = open(self.in_file+'_GFXM_Header', 'wb')
                    f.write(bytearray(bb))
                    f.close()

                # --------------------------------
                # Data

                SEGM_Entries = int.from_bytes((data[0x10], data[0x11], data[0x12], data[0x13]), byteorder='little', signed=False)
                SEGM_Directory = int.from_bytes((data[0x14], data[0x15], data[0x16], data[0x17]), byteorder='little', signed=False)
                MODL_Directory = int.from_bytes((data[0x18], data[0x19], data[0x1A], data[0x1B]), byteorder='little', signed=False)

                print(' ')
                print('GFXM')

                print(' SEGM Entries: '+str(SEGM_Entries))
                print(' SEGM Directory: '+hex(SEGM_Directory))
                print(' MODL Directory: '+hex(MODL_Directory))

                print(' ')
                print(' Segments')

                # --------------------------------
                # Save SEGM DIR

                if save_gfxm_segm_dir == 1:

                    bb = [0] * (SEGM_Entries*4)

                    for i in range(SEGM_Entries*4):

                        bb[i] = data[SEGM_Directory+i]

                    f = open(self.in_file+'_GFXM_SEGM_DIR', 'wb')
                    f.write(bytearray(bb))
                    f.close()

                # --------------------------------
                # Segments

                for i in range(SEGM_Entries):

                    SEGM_Header = int.from_bytes((data[SEGM_Directory+(i*4)], data[SEGM_Directory+(i*4)+1], data[SEGM_Directory+(i*4)+2], data[SEGM_Directory+(i*4)+3]), byteorder='little', signed=False)

                    print('  SEGM Header: '+hex(SEGM_Header))

                    # --------------------------------
                    # Save SEGM Header

                    if save_all == 1:

                        bb = [0] * (96)

                        for ii in range(96):

                            bb[ii] = data[SEGM_Header+ii]

                        f = open(self.in_file+'_SEGM_'+str(i)+'_Header', 'wb')
                        f.write(bytearray(bb))
                        f.close()

                    # --------------------------------
                    # Data

                    SEGM_Width = int.from_bytes((data[SEGM_Header+8], data[SEGM_Header+9], data[SEGM_Header+10], data[SEGM_Header+11]), byteorder='little', signed=False)
                    SEGM_Length = int.from_bytes((data[SEGM_Header+12], data[SEGM_Header+13], data[SEGM_Header+14], data[SEGM_Header+15]), byteorder='little', signed=False)
                    SEGM_EN_Faces = SEGM_Width*SEGM_Length
                    SEGM_EN_Verts = (SEGM_Width+1)*(SEGM_Length+1)
                    SEGM_AA_Faces = int.from_bytes((data[SEGM_Header+16], data[SEGM_Header+17], data[SEGM_Header+18], data[SEGM_Header+19]), byteorder='little', signed=False)
                    SEGM_AA_Verts = int.from_bytes((data[SEGM_Header+32], data[SEGM_Header+33], data[SEGM_Header+34], data[SEGM_Header+35]), byteorder='little', signed=False)

                    print('  SEGM_Width: '+str(SEGM_Width))
                    print('  SEGM_Length: '+str(SEGM_Length))
                    print('  SEGM_EN_Faces: '+str(SEGM_EN_Faces))
                    print('  SEGM_EN_Verts: '+str(SEGM_EN_Verts))
                    print('  SEGM_AA_Faces: '+hex(SEGM_AA_Faces))
                    print('  SEGM_AA_Verts: '+hex(SEGM_AA_Verts))

                    # --------------------------------
                    # Save SEGM Faces

                    if save_segm_faces == 1:

                        bb = [0] * (SEGM_EN_Faces*8)

                        for ii in range(SEGM_EN_Faces*8):

                            bb[ii] = data[SEGM_AA_Faces+ii]

                        f = open(self.in_file+'_SEGM_'+str(i)+'_Face_List', 'wb')
                        f.write(bytearray(bb))
                        f.close()

                        # bytes dont match for some reason
                        # byte are the same, n++ hex view doesnt work
                        # could be char encoder, but whatever
                        # use hex to verify

                    # --------------------------------
                    # Save SEGM Verts

                    if save_segm_verts == 1:

                        bb = [0] * (SEGM_EN_Verts*8)

                        for ii in range(SEGM_EN_Verts*8):

                            bb[ii] = data[SEGM_AA_Verts+ii]

                        f = open(self.in_file+'_SEGM_'+str(i)+'_Vert_List', 'wb')
                        f.write(bytearray(bb))
                        f.close()

                    # --------------------------------

                # --------------------------------

            # --------------------------------
            # No Header

            else:
                print('Error: This is probably not a BLP file.')

            # --------------------------------

        return {'FINISHED'}

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Draw Panel

class PT_EX_PINP(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "Export PINP"

    def draw(self, context):

        layout = self.layout

        col = layout.column(align=True)
        col.prop_search(context.scene.PP, "i_moda", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_modb", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_modc", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_text", bpy.data, "images", text="")
        col.prop(context.scene.PP,"o_file",text="")

        col = layout.column(align=True)
        col.prop_search(context.scene.PP, "i_pnt1", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_pnt2", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_pnt3", bpy.data, "objects", text="")
        col.prop_search(context.scene.PP, "i_pnt4", bpy.data, "objects", text="")

        # Export PINP

        col = layout.column(align=True)
        p = col.operator("scene.ex_pinp", text="Export PINP")
        p.i_text = context.scene.PP.i_text
        p.i_moda = context.scene.PP.i_moda
        p.i_modb = context.scene.PP.i_modb
        p.i_modc = context.scene.PP.i_modc
        p.o_file = context.scene.PP.o_file
        p.i_pnt1 = context.scene.PP.i_pnt1
        p.i_pnt2 = context.scene.PP.i_pnt2
        p.i_pnt3 = context.scene.PP.i_pnt3
        p.i_pnt4 = context.scene.PP.i_pnt4

        # Import PINP

        col = layout.column(align=True)
        p = col.operator("scene.im_pinp", text="Import PINP")
        p.in_file = context.scene.PP.o_file

        # Decompile BLP

        col = layout.column(align=True)
        p = col.operator("scene.dec_blp", text="Decompile BLP")
        p.in_file = context.scene.PP.o_file

        # Import BLP

        # check decode_level.py

# ----------------------------------------------------------------


# ----------------------------------------------------------------
# Register

def register():
    #bpy.types.Scene.pinp_in_file = bpy.props.StringProperty(name="In File", description="File to read, pinp or texture", default="Read_File_Name")
    #bpy.types.Scene.pinp_out_file = bpy.props.StringProperty(name="Out File", description="File to write, pinp or texture", default="Write_File_Name")
    #bpy.types.Scene.pinp_out_text = bpy.props.StringProperty(name="Out Texture", description="Image to write, to pinp or texture", default="")

    bpy.utils.register_class(PP_props)
    bpy.types.Scene.PP = bpy.props.PointerProperty(type=PP_props)

    bpy.utils.register_module(__name__)

def unregister():

    del bpy.types.Scene.PP
    bpy.utils.unregister_class(PP_props)

    bpy.utils.unregister_module(__name__)

    #del bpy.types.Scene.pinp_in_file
    #del bpy.types.Scene.pinp_out_file
    #del bpy.types.Scene.pinp_out_text

if __name__ == "__main__":
    register()

# ----------------------------------------------------------------




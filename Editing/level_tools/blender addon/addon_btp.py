# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# ----------------------------------------------------------------
# Header

bl_info = {
    "name": "Import Export BTP Textures",
    "author": "orgyia",
    "version": (1, 0, 0),
    "blender": (2, 72, 0),
    "location": "3D View -> Properties -> BTP Textures",
    "description": "Import Export BTP Textures.",
    "wiki_url": "",
    "category": "Import-Export"
}

import bpy

# ----------------------------------------------------------------
# Function

# read short from list
def to_short(data, da):
    return int.from_bytes((data[da], data[da+1]), byteorder='little', signed=False)

# read int from list
def to_int(data, da):
    return int.from_bytes((data[da], data[da+1], data[da+2], data[da+3]), byteorder='little', signed=False)

# write int to list
def add_int(vari,size,sign,addr,data):
    
    if size == 1:
        data[addr+0] = vari.to_bytes(length=1,byteorder='little',signed=sign)[0]
    
    if size == 2:
        data[addr+0] = vari.to_bytes(length=2,byteorder='little',signed=sign)[0]
        data[addr+1] = vari.to_bytes(length=2,byteorder='little',signed=sign)[1]
    
    if size == 4:
        data[addr+0] = vari.to_bytes(length=4,byteorder='little',signed=sign)[0]
        data[addr+1] = vari.to_bytes(length=4,byteorder='little',signed=sign)[1]
        data[addr+2] = vari.to_bytes(length=4,byteorder='little',signed=sign)[2]
        data[addr+3] = vari.to_bytes(length=4,byteorder='little',signed=sign)[3]

# save list
def add_bin(oname, fo):
    f = open(oname, 'rb')
    data = f.read()
    f.close()
    fo.write(bytearray(data))
    del data

# ----------------------------------------------------------------
# Import BTP

class OT_STEX_IMPORT_BTP(bpy.types.Operator):
    bl_idname = "scene.stex_import_btp"
    bl_label = "Import BTP"
    bl_description = "Import BTP"
    bl_options = {'REGISTER', 'UNDO'}

    stex_filename = bpy.props.StringProperty(name="Filename", description="File to read or write.", default="filename")

    def execute(self, context):

        f = open(self.stex_filename, 'rb')
        data = f.read()
        f.close()

        error = 0

        if error == 0:

            # --------------------------------
            # Textures

            EN_TPAGES = to_short(data,52)
            AA_TPAGES = to_int(data,56)
            EN_PALLETS = to_short(data,54)
            AA_PALLETS = to_int(data,60)
            AA_TEXDATA = to_int(data,36)

            print('EN_TPAGES: '+str(EN_TPAGES))
            print('AA_TPAGES: '+hex(AA_TPAGES))
            print('EN_PALLETS: '+str(EN_PALLETS))
            print('AA_PALLETS: '+hex(AA_PALLETS))
            print('AA_TEXDATA: '+hex(AA_TEXDATA))

            # Texture Pages are added together based on the pallet id
            # First the Total PNG_height is required for the PNG file

            for p in range(EN_PALLETS):

                # Return the Total PNG_height

                TP_width = 256
                TP_height = 0

                for t in range(EN_TPAGES):
                    TP_pallet = to_int(data,AA_TPAGES+(12*t)+4)
                    if TP_pallet == p:
                        TP_height += to_short(data,AA_TPAGES+(12*t)+2)

                PNG = bpy.data.images.new(self.stex_filename+'_Texture',TP_width,TP_height,True,False)
                PNG_data = [0.0] * ((TP_width*TP_height)*4)
                PNG_height = TP_height

                # Blenders Y is reversed, start at the end, the decrement each Y

                dy = ((PNG_height-1)*TP_width)*4

                for t in range(EN_TPAGES):
                    TP_pallet = to_int(data,AA_TPAGES+(12*t)+4)
                    if TP_pallet == p:

                        TP_height = to_short(data,AA_TPAGES+(12*t)+2)
                        TP_data = to_int(data,AA_TPAGES+(12*t)+8)
                        print(hex(TP_data))

                        for y in range(TP_height):
                            for x in range(TP_width):

                                c = data[AA_TEXDATA+TP_data+((y*TP_width)+x)]

                                PNG_data[dy+(x*4)+0] = data[AA_PALLETS+(TP_pallet*1024)+(c*4)+2] / 255
                                PNG_data[dy+(x*4)+1] = data[AA_PALLETS+(TP_pallet*1024)+(c*4)+1] / 255
                                PNG_data[dy+(x*4)+2] = data[AA_PALLETS+(TP_pallet*1024)+(c*4)+0] / 255
                                PNG_data[dy+(x*4)+3] = data[AA_PALLETS+(TP_pallet*1024)+(c*4)+3] / 255

                            dy -= TP_width*4

                PNG.pixels=PNG_data
                PNG.pack(as_png=True)
                del PNG_data

            # --------------------------------
            # Cobjects

            en_cob = to_int(data,0x14)
            aa_cob = to_int(data,0x28)

            f = open(self.stex_filename+'_cobjects', 'wb')
            f.write(en_cob.to_bytes(length=4,byteorder='little',signed=False))
            for i in range(en_cob*66):
                bdata = data[aa_cob+i]
                f.write(bdata.to_bytes(length=1,byteorder='little',signed=False))
            f.close()

        return {'FINISHED'}

# ----------------------------------------------------------------
# Import BTP

class OT_STEX_EXPORT_BTP(bpy.types.Operator):
    bl_idname = "scene.stex_export_btp"
    bl_label = "Export BTP"
    bl_description = "Export BTP"
    bl_options = {'REGISTER', 'UNDO'}

    stex_filename = bpy.props.StringProperty(name="Filename", description="File to read or write.", default="filename")

    def execute(self, context):

        # so, how do we export
        # we require a list of images to export

        # --------------------------------
        # Texture Image List

        image_entries = 11
        image_name = []
        image_name.append('123_rc2_Texture')
        image_name.append('123_rc2_Texture.001')
        image_name.append('123_rc2_Texture.002')
        image_name.append('123_rc2_Texture.003')
        image_name.append('123_rc2_Texture.004')
        image_name.append('123_rc2_Texture.005')
        image_name.append('123_rc2_Texture.006')
        image_name.append('123_rc2_Texture.007')
        image_name.append('123_rc2_Texture.008')
        image_name.append('123_rc2_Texture.009')
        image_name.append('123_rc2_Texture.010')
        print(image_name)

        # --------------------------------
        # Calculate Pages

        # For each image, all texture pages are 256 x 256, except the last one, which has a variable last_height.
        # This counts the total texture pages, image texture pages, and the last height for each image.

        image_pages = [0] * image_entries
        image_last_height = [0] * image_entries
        page_entries = 0

        for i in range(image_entries):

            im = bpy.data.images[image_name[i]]
            image_last_height[i] = im.size[1]
            image_pages[i] = 1
            page_entries += 1

            while(image_last_height[i]>256):
                image_last_height[i] -= 256
                image_pages[i] += 1
                page_entries += 1

        # --------------------------------
        # Read Cobjects

        f = open(self.stex_filename+'_cobjects', 'rb')
        cdata = f.read()
        f.close()

        cobs_entries = to_int(cdata, 0)
        cobs_data = [0] * (cobs_entries*66)

        for i in range(cobs_entries*66):
            cobs_data[i] = cdata[i+4]

        del cdata

        # --------------------------------
        # Allocate Data

        header_data = [0] * 0x40
        short_data = [0] * 0x880
        vram_label = [0x2D,0x20,0x56,0x52,0x41,0x4D,0x20,0x42,0x49,0x54,0x4D,0x41,0x50,0x20,0x2D,0x2D]
        vram_data = [0] * page_entries
        cobs_label = [0x2D,0x2D,0x2D,0x20,0x43,0x4F,0x42,0x4A,0x45,0x43,0x54,0x53,0x20,0x2D,0x2D,0x2D]
        # cobs_data, allocated above
        cloud_label = [0x2D,0x2D,0x20,0x43,0x4C,0x4F,0x55,0x44,0x20,0x44,0x41,0x54,0x41,0x20,0x2D,0x2D]
        # cloud_data, unknown format
        page_label = [0x2D,0x2D,0x20,0x54,0x50,0x41,0x47,0x45,0x20,0x44,0x41,0x54,0x41,0x20,0x2D,0x2D]
        page_data = [0] * (page_entries*12)
        pallet_label = [0x2D,0x2D,0x20,0x50,0x41,0x4C,0x45,0x54,0x20,0x44,0x41,0x54,0x41,0x20,0x2D,0x2D]
        pallet_data = [0] * image_entries

        # --------------------------------
        # Page Helpers

        page_width = [0] * page_entries
        page_height = [0] * page_entries
        page_pallet = [0] * page_entries
        page_offset = [0] * page_entries

        # --------------------------------
        # Pallet Helpers

        pallet_colors = [0] * image_entries

        # --------------------------------
        # Add Page Data

        page = 0
        addr = 0
        for i in range(image_entries):
            for p in range(image_pages[i]):

                # use add int / short
                #page_data[(page*12)+0] = 

                page_pallet[page] = i
                page_width[page] = 256
                page_height[page] = 256

                if p == image_pages[i]-1:
                    page_height[page] = image_last_height[i]

                add_int(page_width[page],2,False,(page*12)+0,page_data)
                add_int(page_height[page],2,False,(page*12)+2,page_data)
                add_int(page_pallet[page],4,False,(page*12)+4,page_data)

                page_offset[page] = addr
                addr += page_width[page]*page_height[page]

                add_int(page_offset[page],4,False,(page*12)+8,page_data)

                page += 1

        # --------------------------------
        # Print Page Data

        #for i in range(page_entries):
        #    print(hex(page_width[i])+'	'+hex(page_height[i])+'	'+hex(page_pallet[i])+'	'+hex(page_offset[i]))

        # --------------------------------
        # Allocate Pallet Data

        for i in range(image_entries):
            pallet_data[i] = [0] * 1024

        # --------------------------------
        # Allocate VRAM Data

        size_vram_data = 0

        for i in range(page_entries):
            vram_data[i] = [0] * (page_width[i]*page_height[i])
            size_vram_data += (page_width[i]*page_height[i])

        # --------------------------------
        # Status

        # Here all data lists have been allocated
        # Now the data is added to the lists
        # The labels and page_data have already been updated

        # Header                               0x40
        # 1088 '0100's                         0x880
        # - VRAM BITMAP --      updated        0x10
        # Texture Data                         Count based on image_pages + image_last_height
        # --- COBJECTS ---      updated        0x10
        # Cobject Data          updated        entries * 66
        # -- CLOUD DATA --      updated        0x10
        # Cloud Data            no data        0x0
        # -- TPAGE DATA --      updated        0x10
        # Texture Pages         updated        entries * 12
        # -- PALET DATA --      updated        0x10
        # Pallet Data                          entries * 1024

        # --------------------------------
        # Helper Addresses

        addr_header = 0x0
        addr_shorts = addr_header + 0x40
        addr_vram_label = addr_shorts + 0x880
        addr_vram_data = addr_vram_label + 0x10
        addr_cobs_label = addr_vram_data + size_vram_data
        addr_cobs_data = addr_cobs_label + 0x10
        addr_cloud_label = addr_cobs_data + (cobs_entries*66)
        addr_cloud_data = addr_cloud_label + 0x10
        addr_page_label = addr_cloud_data + 0x0
        addr_page_data = addr_page_label + 0x10
        addr_pallet_label = addr_page_data + (page_entries*12)
        addr_pallet_data = addr_pallet_label + 0x10

        # --------------------------------
        # Add Header Data

        header_data[0] = 0x42           # 'B'
        header_data[1] = 0x54           # 'T'
        header_data[2] = 0x50           # 'P'
        header_data[3] = 0x20           # ' '

        add_int(cobs_entries,4,False,0x14,header_data)          # EN Cobjects
        add_int(page_entries,2,False,0x34,header_data)          # EN Pages
        add_int(image_entries,2,False,0x36,header_data)         # EN Pallets

        add_int(addr_cloud_data,4,False,0x1c,header_data)       # AA Cloud Data
        add_int(addr_vram_data,4,False,0x24,header_data)        # AA VRAM Data
        add_int(addr_cobs_data,4,False,0x28,header_data)        # AA Cobject Data
        add_int(addr_page_data,4,False,0x38,header_data)        # AA Page Data
        add_int(addr_pallet_data,4,False,0x3c,header_data)      # AA Pallet Data

        # These are a set of unknown header numbers which may have to be modified manually

        add_int(0x0,4,False,0x4,header_data)
        add_int(0x0,2,False,0x8,header_data)
        add_int(0x0,2,False,0xa,header_data)
        add_int(0x0,4,False,0xc,header_data)
        add_int(0x0,4,False,0x10,header_data)

        add_int(0x0,4,False,0x18,header_data)
        add_int(0x0,4,False,0x20,header_data)
        add_int(0x0,4,False,0x2c,header_data)

        add_int(0x0,2,False,0x30,header_data)
        add_int(0x0,2,False,0x32,header_data)

        # --------------------------------
        # Add Short Data

        for i in range(0x440):
            short_data[(i*2)+0] = 0x01
            short_data[(i*2)+1] = 0x00

        # --------------------------------
        # Add Pallet Data

        # The pallets only contains unique colors, no duplicates
        # First convert the floats to ints

        print('Generating Pallets, this may take a while, 5-60 seconds per image, depending on image size.')

        #for i in range(image_entries):
        for i in range(1):

            #print('before image')
            im = bpy.data.images[image_name[i]]
            px = list(im.pixels)
            #print('after image')

            '''
            px2 = [0] * 1024
            for p in range(1024):
                px2[p] = (px[(p*4)+0],px[(p*4)+1],px[(p*4)+2],px[(p*4)+3])
            '''

            #print('after px2')
            #for p in range(10):
            #    print(px2[p])


            # The first color is usually 00000000
            # Im not sure what order the colors are added

            # Add 00000000

            pallet_data[i][0] = 0x0
            pallet_data[i][1] = 0x0
            pallet_data[i][2] = 0x0
            pallet_data[i][3] = 0x0
            pallet_colors[i] = 1

            # --------------------------------
            # Generate Pallet

            # its pretty slow
            # may be better is add colors to list as 4 byte ints
            # hard to check unless px is also ints

            #pallet_id = 0

            for t in range(im.size[0]*im.size[1]):
            #for t in range(100):

                # 0123 = 2103
                color = [int(px[(t*4)+2]*255),int(px[(t*4)+1]*255),int(px[(t*4)+0]*255),int(px[(t*4)+3]*255)]
                #print(hex(color[0])+'	'+hex(color[1])+'	'+hex(color[2])+'	'+hex(color[3]))

                # Now I have the color time to check it with the .. pallet

                new_color = 1

                for p in range(pallet_colors[i]):

                    if pallet_data[i][(p*4)+0] == color[0] and pallet_data[i][(p*4)+1] == color[1] and pallet_data[i][(p*4)+2] == color[2] and pallet_data[i][(p*4)+3] == color[3]:
                        new_color = 0
                        #pallet_id = p
                        p = pallet_colors[i]

                #if new_color == 1 and pallet_colors[i] > 255:
                #    print('Error: New color cannot be added, too many colors, max colors = 255 + 0x00000000.')

                if new_color == 1 and pallet_colors[i] < 256:

                    pallet_data[i][(pallet_colors[i]*4)+0] = color[0]
                    pallet_data[i][(pallet_colors[i]*4)+1] = color[1]
                    pallet_data[i][(pallet_colors[i]*4)+2] = color[2]
                    pallet_data[i][(pallet_colors[i]*4)+3] = color[3]
                    #pallet_id = pallet_colors[i]
                    pallet_colors[i] += 1

                # --------------------------------
                # Add pallet_id to vram

                # vram_data[i][t]

                # this loop uses image resolution
                # require a new loop using vram resolution

            print(' - Image '+image_name[i]+' has '+str(pallet_colors[i])+' colors.')

        # --------------------------------
        # Add Vram Data

        # this code could be added in loop above, to save time
        # saves time by using the pallet id

        print('Generating VRAM Data, this may take a while.')

        page = 0
        #for i in range(image_entries):
        for i in range(1):

            im = bpy.data.images[image_name[i]]
            px = list(im.pixels)

            start = ((im.size[1]-1)*im.size[0])*4

            for p in range(image_pages[i]):
                for y in range(page_height[page]):
                    for x in range(page_width[page]):

                        color = [0] * 4
                        color[0] = int(px[start+(x*4)+2]*255)
                        color[1] = int(px[start+(x*4)+1]*255)
                        color[2] = int(px[start+(x*4)+0]*255)
                        color[3] = int(px[start+(x*4)+3]*255)

                        for p in range(pallet_colors[i]):
                            if pallet_data[i][(p*4)+0] == color[0] and pallet_data[i][(p*4)+1] == color[1] and pallet_data[i][(p*4)+2] == color[2] and pallet_data[i][(p*4)+3] == color[3]:

                                vram_data[page][(y*page_width[page])+x] = p

                    start -= page_width[page]*4

                page += 1

            print(' - Image '+image_name[i]+' has been added to VRAM Data.')

        # --------------------------------
        # Write data to file

        print('Saving Texture File.')

        f = open('BTP_test', 'wb')

        f.write(bytearray(header_data))
        f.write(bytearray(short_data))
        f.write(bytearray(vram_label))
        for i in range(page_entries):
            f.write(bytearray(vram_data[i]))
        f.write(bytearray(cobs_label))
        f.write(bytearray(cobs_data))
        f.write(bytearray(cloud_label))
        f.write(bytearray(page_label))
        f.write(bytearray(page_data))
        f.write(bytearray(pallet_label))
        for i in range(image_entries):
            f.write(bytearray(pallet_data[i]))

        f.close()

        return {'FINISHED'}

# ----------------------------------------------------------------
# Panel

class PT_STEX(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "BTP Textures"

    def draw(self, context):

        layout = self.layout

        col = layout.column(align=True)
        col.prop(context.scene,"stex_filename",text="")

        col = layout.column(align=True)
        p = col.operator("scene.stex_import_btp", text="Import BTP")
        p.stex_filename = context.scene.stex_filename

        col = layout.column(align=True)
        p = col.operator("scene.stex_export_btp", text="Export BTP")
        p.stex_filename = context.scene.stex_filename

# ----------------------------------------------------------------
# Register

def register():
    bpy.types.Scene.stex_filename = bpy.props.StringProperty(name="Filename", description="File to read or write.", default="filename")
    bpy.utils.register_module(__name__)

def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.stex_filename

if __name__ == "__main__":
    register()

# ----------------------------------------------------------------




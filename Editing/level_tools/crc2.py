



# ----------------------------------------------------------------
# Header

import os

def to_int(data, da):
    return int.from_bytes((data[da], data[da+1], data[da+2], data[da+3]), byteorder='little', signed=False)

def add_bin(oname, fo):
    f = open(oname, 'rb')
    data = f.read()
    f.close()
    fo.write(bytearray(data))
    del data
    
def add_int(vari,size,sign,addr,data):
    
    if size == 1:
        data[addr+0] = vari.to_bytes(length=1,byteorder='little',signed=sign)[0]
    
    if size == 2:
        data[addr+0] = vari.to_bytes(length=2,byteorder='little',signed=sign)[0]
        data[addr+1] = vari.to_bytes(length=2,byteorder='little',signed=sign)[1]
    
    if size == 4:
        data[addr+0] = vari.to_bytes(length=4,byteorder='little',signed=sign)[0]
        data[addr+1] = vari.to_bytes(length=4,byteorder='little',signed=sign)[1]
        data[addr+2] = vari.to_bytes(length=4,byteorder='little',signed=sign)[2]
        data[addr+3] = vari.to_bytes(length=4,byteorder='little',signed=sign)[3]
    
# import os
# size = os.stat('filename').st_size

print('----------------------------------------------------------------')

# ----------------------------------------------------------------
# File Size

segments = 1

# --------------------------------
# segm sizes

size_segm = [0] * segments
size_segm_header = [0] * segments
size_segm_faces = [0] * segments
size_segm_mip1 = [0] * segments
size_segm_mip2 = [0] * segments
size_segm_mip3 = [0] * segments
size_segm_sm1 = [0] * segments
size_segm_sm2 = [0] * segments
size_segm_sm3 = [0] * segments
size_segm_verts = [0] * segments
size_segm_copl = [0] * segments
size_segm_dft1 = [0] * segments
size_segm_dft2 = [0] * segments
size_segm_dft3 = [0] * segments
addr_segm = [0] * segments
addr_segm_header = [0] * segments
addr_segm_faces = [0] * segments
addr_segm_mip1 = [0] * segments
addr_segm_mip2 = [0] * segments
addr_segm_mip3 = [0] * segments
addr_segm_sm1 = [0] * segments
addr_segm_sm2 = [0] * segments
addr_segm_sm3 = [0] * segments
addr_segm_verts = [0] * segments
addr_segm_copl = [0] * segments
addr_segm_dft1 = [0] * segments
addr_segm_dft2 = [0] * segments
addr_segm_dft3 = [0] * segments

for i in range(segments):
    
    size_segm_header[i] = os.stat('_segm_'+str(i)+'_header').st_size
    size_segm_faces[i] = os.stat('_segm_'+str(i)+'_faces').st_size
    size_segm_mip1[i] = os.stat('_segm_'+str(i)+'_mip1').st_size
    size_segm_mip2[i] = os.stat('_segm_'+str(i)+'_mip2').st_size
    size_segm_mip3[i] = os.stat('_segm_'+str(i)+'_mip3').st_size
    size_segm_sm1[i] = os.stat('_segm_'+str(i)+'_sm1').st_size
    size_segm_sm2[i] = os.stat('_segm_'+str(i)+'_sm2').st_size
    size_segm_sm3[i] = os.stat('_segm_'+str(i)+'_sm3').st_size
    size_segm_verts[i] = os.stat('_segm_'+str(i)+'_verts').st_size
    size_segm_copl[i] = 4
    size_segm_dft1[i] = os.stat('_segm_'+str(i)+'_dft1').st_size
    size_segm_dft2[i] = os.stat('_segm_'+str(i)+'_dft2').st_size
    size_segm_dft3[i] = os.stat('_segm_'+str(i)+'_dft3').st_size
    size_segm[i] = size_segm_header[i] + size_segm_faces[i] + size_segm_verts[i] + size_segm_mip1[i] + size_segm_mip2[i] + size_segm_mip3[i] + size_segm_sm1[i] + size_segm_sm2[i] + size_segm_sm3[i] + size_segm_copl[i] + size_segm_dft1[i] + size_segm_dft2[i] + size_segm_dft3[i]

    addr_segm[i] = 320
    if i>0:
        # address = previous address + size
        addr_segm[i] = addr_segm[i-1] + size_segm[i-1]
    
    # addr_segm_[i] = addr_segm[i] + size_segm_
    addr_segm_faces[i] = addr_segm[i] + 96
    addr_segm_mip1[i] = addr_segm_faces[i] + size_segm_faces[i]
    addr_segm_mip2[i] = addr_segm_mip1[i] + size_segm_mip1[i]
    addr_segm_mip3[i] = addr_segm_mip2[i] + size_segm_mip2[i]
    
    addr_segm_sm1[i] = addr_segm_mip3[i] + size_segm_mip3[i]
    addr_segm_sm2[i] = addr_segm_sm1[i] + size_segm_sm1[i]
    addr_segm_sm3[i] = addr_segm_sm2[i] + size_segm_sm2[i]
    addr_segm_verts[i] = addr_segm_sm3[i] + size_segm_sm3[i]
    
    addr_segm_copl[i] = addr_segm_verts[i] + size_segm_verts[i]
    addr_segm_dft1[i] = addr_segm_copl[i] + size_segm_copl[i]
    addr_segm_dft2[i] = addr_segm_dft1[i] + size_segm_dft1[i]
    addr_segm_dft3[i] = addr_segm_dft2[i] + size_segm_dft2[i]
    
    print('size_segm[i]: '+str(size_segm[i]))
    print('size_segm_faces[i]: '+str(size_segm_faces[i]))
    print('size_segm_mip1[i]: '+str(size_segm_mip1[i]))
    print('size_segm_mip2[i]: '+str(size_segm_mip2[i]))
    print('size_segm_mip3[i]: '+str(size_segm_mip3[i]))
    print('size_segm_sm1[i]: '+str(size_segm_sm1[i]))
    print('size_segm_sm2[i]: '+str(size_segm_sm2[i]))
    print('size_segm_sm3[i]: '+str(size_segm_sm3[i]))
    print('size_segm_verts[i]: '+str(size_segm_verts[i]))
    print('size_segm_copl[i]: '+str(size_segm_copl[i]))
    print('size_segm_dft1[i]: '+str(size_segm_dft1[i]))
    print('size_segm_dft2[i]: '+str(size_segm_dft2[i]))
    print('size_segm_dft3[i]: '+str(size_segm_dft3[i]))
    
    print('addr_segm[i]: '+hex(addr_segm[i]))
    print('addr_segm_faces[i]: '+hex(addr_segm_faces[i]))
    print('addr_segm_mip1[i]: '+hex(addr_segm_mip1[i]))
    print('addr_segm_mip2[i]: '+hex(addr_segm_mip2[i]))
    print('addr_segm_mip3[i]: '+hex(addr_segm_mip3[i]))
    print('addr_segm_sm1[i]: '+hex(addr_segm_sm1[i]))
    print('addr_segm_sm2[i]: '+hex(addr_segm_sm2[i]))
    print('addr_segm_sm3[i]: '+hex(addr_segm_sm3[i]))
    print('addr_segm_verts[i]: '+hex(addr_segm_verts[i]))
    print('addr_segm_copl[i]: '+hex(addr_segm_copl[i]))
    print('addr_segm_dft1[i]: '+hex(addr_segm_dft1[i]))
    print('addr_segm_dft2[i]: '+hex(addr_segm_dft2[i]))
    print('addr_segm_dft3[i]: '+hex(addr_segm_dft3[i]))

# --------------------------------
# gfxm data sizes
# segm_dir address

size_gfxm_data1 = os.stat('_gfxm_data1').st_size
size_gfxm_pfl = os.stat('_gfxm_pfl').st_size
size_gfxm_data2 = os.stat('_gfxm_data2').st_size
size_gfxm_data3 = os.stat('_gfxm_data3').st_size

# !!
# will have to replace later
# when segms > 1
# maybe

addr_segm_dir = 320                     # add size gfxm header
for i in range(segments):               # loop segments
    addr_segm_dir += size_segm[i]       # add size segments

addr_gfxm_data1 = addr_segm_dir
addr_gfxm_pfl = addr_gfxm_data1+size_gfxm_data1
addr_gfxm_data2 = addr_gfxm_pfl+size_gfxm_pfl
addr_gfxm_data3 = addr_gfxm_data2+size_gfxm_data2

addr_segm_dir += size_gfxm_data1        # add size data1
addr_segm_dir += size_gfxm_pfl          # add size pfl
addr_segm_dir += size_gfxm_data2        # add size data2
addr_segm_dir += size_gfxm_data3        # add size data3

'''
print('size_gfxm_data1: '+str(size_gfxm_data1))
print('size_gfxm_pfl: '+str(size_gfxm_pfl))
print('size_gfxm_data2: '+str(size_gfxm_data2))
print('size_gfxm_data3: '+str(size_gfxm_data3))

for i in range(segments):
    
    print('size_segm_header[i]: '+str(size_segm_header[i]))
    print('size_segm_faces[i]: '+str(size_segm_faces[i]))
    print(': '+str(size_segm_verts[i]))
    print(': '+str(size_segm_mip1[i]))
    print(': '+str(size_segm_mip2[i]))
    print(': '+str(size_segm_mip3[i]))
    print(': '+str(size_segm_sm1[i]))
    print(': '+str(size_segm_sm2[i]))
    print(': '+str(size_segm_sm3[i]))
    print(': '+str(size_segm_copl[i]))
    print(': '+str(size_segm_dft1[i]))
    print(': '+str(size_segm_dft2[i]))
    print(': '+str(size_segm_dft3[i]))
    print(': '+str(addr_segm[i]))
'''

# --------------------------------
# modl sizes

# have to know size of modl dir + size of all modl files
# I require a format to construct the modl dir
# for this level i will use the modl dir present, so I could just count this
# later a tool can be made to make a new modl dir
# problem is, they have to be added in the correct order
# the decompiled versions are added in order, but no way of knowing what slot they are added to, unless the user has the modl dir
# the modl dir is a set of addresses which will have to be regenerated, based on file sizes
# when i regenerate the modl addresses, i will have to know the slot and the file, then calc in order
# a list of variable size (defined) with a file name or '' will do
# hmm
# doesnt matter, just add models manually

# change modl_dir_slots depending on level

modl_dir_slots = 34
modl_dir_file = [''] * modl_dir_slots
modl_dir_size = [0] * modl_dir_slots
modl_dir_addr = [0] * modl_dir_slots 
size_modl_block = 0

# change these depending on level

modl_dir_file[0] = '_modl_0'
modl_dir_file[1] = ''
modl_dir_file[2] = '_modl_1'
modl_dir_file[3] = '_modl_2'

modl_dir_file[4] = '_modl_3'
modl_dir_file[5] = '_modl_4'
modl_dir_file[6] = '_modl_5'
modl_dir_file[7] = ''

modl_dir_file[8] = ''
modl_dir_file[9] = '_modl_6'
modl_dir_file[10] = '_modl_7'
modl_dir_file[11] = ''

modl_dir_file[12] = ''
modl_dir_file[13] = '_modl_8'
modl_dir_file[14] = '_modl_9'
modl_dir_file[15] = '_modl_10'

modl_dir_file[16] = '_modl_11'
modl_dir_file[17] = '_modl_12'
modl_dir_file[18] = '_modl_13'
modl_dir_file[19] = '_modl_14'

modl_dir_file[20] = '_modl_15'
modl_dir_file[21] = '_modl_16'
modl_dir_file[22] = '_modl_17'
modl_dir_file[23] = '_modl_18'

modl_dir_file[24] = '_modl_19'
modl_dir_file[25] = '_modl_20'
modl_dir_file[26] = '_modl_21'
modl_dir_file[27] = '_modl_22'

modl_dir_file[28] = '_modl_23'
modl_dir_file[29] = '_modl_24'
modl_dir_file[30] = '_modl_25'
modl_dir_file[31] = '_modl_26'

modl_dir_file[32] = '_modl_27'
modl_dir_file[33] = '_modl_28'





models = 0
p_modl = 0
for i in range(modl_dir_slots):
    if modl_dir_file[i] != '':
        modl_dir_size[i] = os.stat(modl_dir_file[i]).st_size
        size_modl_block += modl_dir_size[i]
        
        print('modl_dir_size['+str(i)+']: '+str(modl_dir_size[i]))
        
        if i == 0:
            modl_dir_addr[i] = addr_segm_dir+(segments*4)+(modl_dir_slots*4)
        else:
            modl_dir_addr[i] = modl_dir_addr[p_modl] + modl_dir_size[p_modl]
            
        p_modl = i          # previous valid modl
        models += 1
        
'''
addr_modl_dir = addr_segm_dir+(segments*4)
addr_modls = addr_modl_dir+(modl_dir_slots*4)
'''


'''
for i in range(modl_dir_slots):
    print(str(modl_dir_file[i])+': '+str(modl_dir_size[i]))
'''

# Here I have to write the modl header format, for size and addresses
# I have to open the files, then calculate the addresses

# hmm, most modl addresses are offsets, so I dont have to recalculate them
# they maybe some which are absolute addresses, not sure yet though

# k, for now, lets debug the data

'''

size_modl = [0] * models
size_modl_header = [0] * models
size_modl_en_verts = [0] * models
size_modl_en_faces = [0] * models
size_modl_faces = [0] * models
size_modl_xx1 = [0] * models
size_modl_uv = [0] * models
size_modl_xx2 = [0] * models
size_modl_mip1 = [0] * models
size_modl_mip2 = [0] * models
size_modl_next = [0] * models
size_modl_xx3 = [0] * models
size_modl_verts = [0] * models
size_modl_points = [0] * models
addr_modl = [0] * models
addr_modl_header = [0] * models
#addr_modl_en_verts = [0] * models      # not required
#addr_modl_en_faces = [0] * models      # not required
addr_modl_faces = [0] * models
addr_modl_xx1 = [0] * models
addr_modl_uv = [0] * models
addr_modl_xx2 = [0] * models
addr_modl_mip1 = [0] * models
addr_modl_mip2 = [0] * models
addr_modl_next = [0] * models
addr_modl_xx3 = [0] * models
addr_modl_verts = [0] * models
addr_modl_points = [0] * models

m = 0
for i in range(modl_dir_slots):
    if modl_dir_file[i] != '':
        size_modl[m] = modl_dir_size[i]
        size_modl_header[m] = 80
        
        # --------------------------------
        # Open modl
        
        f = open(modl_dir_file[i], 'rb')
        data = f.read()
        f.close()
        
        # return the original addresses
        # calculate size
        # recalculate addresses
        # hmm, they are all offset, so it doesnt really matter
        
        size_modl_en_verts[m] = data[0x4]
        size_modl_en_faces[m] = data[0x6]
        pa_faces = data[0xc]
        pa_xx1 = data[0x10]
        pa_uv = data[0x14]
        pa_xx2 = data[0x18]
        pa_mip1 = data[0x1c]
        pa_mip2 = data[0x1e]
        pa_next = data[0x30]
        pa_xx3 = data[0x34]
        pa_verts = data[0x40]
        pa_points = data[0x4c]
        
        del data
        
        # --------------------------------
        
        m += 1
'''

# --------------------------------
# gd sizes

size_gf1 = os.stat('_gfxm_gf1').st_size

size_gd1 = os.stat('_gfxm_gd1').st_size
size_gd2 = os.stat('_gfxm_gd2').st_size
size_gd3 = os.stat('_gfxm_gd3').st_size
size_gd4 = os.stat('_gfxm_gd4').st_size
size_gd5 = os.stat('_gfxm_gd5').st_size
size_gd6 = os.stat('_gfxm_gd6').st_size
size_gd7 = os.stat('_gfxm_gd7').st_size
size_gd8 = os.stat('_gfxm_gd8').st_size
size_gd9 = os.stat('_gfxm_gd9').st_size
size_gd10 = os.stat('_gfxm_gd10').st_size
size_gd11 = os.stat('_gfxm_gd11').st_size
size_gd12 = os.stat('_gfxm_gd12').st_size
size_gd13 = os.stat('_gfxm_gd13').st_size
size_gd14 = os.stat('_gfxm_gd14').st_size
size_gd15 = os.stat('_gfxm_gd15').st_size
size_gd16 = os.stat('_gfxm_gd16').st_size
size_gd17 = os.stat('_gfxm_gd17').st_size
size_gd18 = os.stat('_gfxm_gd18').st_size
size_gd19 = os.stat('_gfxm_gd19').st_size
size_gd20 = os.stat('_gfxm_gd20').st_size
size_gd21 = os.stat('_gfxm_gd21').st_size
size_gd22 = os.stat('_gfxm_gd22').st_size
size_gd23 = os.stat('_gfxm_gd23').st_size
size_gd24 = os.stat('_gfxm_gd24').st_size

size_vector = os.stat('_gfxm_vector').st_size
size_normal = os.stat('_gfxm_normal').st_size
size_short1 = os.stat('_gfxm_short1').st_size
size_short2 = os.stat('_gfxm_short2').st_size
size_light = os.stat('_gfxm_light').st_size
size_1111 = os.stat('_gfxm_1111').st_size
size_ffff = os.stat('_gfxm_ffff').st_size
size_pa = os.stat('_gfxm_pa').st_size

# ----------------------------------------------------------------
# Addresses

addr_gfxm_header = 0
                                                    # segm addresses are calculated above
                                                    # addr_segm_dir = address after segments
                                                    # addr_segm_dir+(segments*4) = address after segm dir
                                                    # will have to add segm addresses to segm headers sometime
                                                    # calculated addresses above, can update later
#addr_segm_dir = addr_segm_dir
addr_modl_dir = addr_segm_dir+(segments*4)
addr_modls = addr_modl_dir+(modl_dir_slots*4)
                                                    # modl addresses are not calculated
                                                    # modl_dir_size[i] = modl size
                                                    # size_modl_block = all modl size
                                                    # addr_modls = address after modl dir
                                                    # addr_gd1 = address after modls
                                                    # will have to add modl addresses to modl headers sometime
# addr_ = addr_+size_
addr_gd1 = addr_modls+size_modl_block
addr_gd2 = addr_gd1+size_gd1
addr_gd3 = addr_gd2+size_gd2
addr_gd4 = addr_gd3+size_gd3
addr_vector = addr_gd4+size_gd4
addr_normal = addr_vector+size_vector
addr_short1 = addr_normal+size_normal
addr_short2 = addr_short1+size_short1
addr_light = addr_short2+size_short2
addr_1111 = addr_light+size_light
addr_ffff = addr_1111+size_1111
addr_pa = addr_ffff+size_ffff
addr_gd5 = addr_pa+size_pa
addr_gd6 = addr_gd5+size_gd5
addr_gd7 = addr_gd6+size_gd6
addr_gd8 = addr_gd7+size_gd7
addr_gd9 = addr_gd8+size_gd8
addr_gd10 = addr_gd9+size_gd9
addr_gd11 = addr_gd10+size_gd10
addr_gd12 = addr_gd11+size_gd11
addr_gd13 = addr_gd12+size_gd12
addr_gd14 = addr_gd13+size_gd13
addr_gd15 = addr_gd14+size_gd14
addr_gd16 = addr_gd15+size_gd15
addr_gd17 = addr_gd16+size_gd16
addr_gd18 = addr_gd17+size_gd17
addr_gf1 = addr_gd18+size_gd18
addr_gd19 = addr_gf1+size_gf1
addr_gd20 = addr_gd19+size_gd19
addr_gd21 = addr_gd20+size_gd20
addr_gd22 = addr_gd21+size_gd21
addr_gd23 = addr_gd22+size_gd22

#addr_gd23 += 12                             # INCA

addr_gd24 = addr_gd23+size_gd23

# ----------------------------------------------------------------
# recalculate segment mip texture addresses

# open segm_0_mip_1, read first mip texture address
f = open('_segm_0_mip1', 'rb')
mip1 = f.read()
f.close()
rec_mip1 = to_int(mip1,0x4)
del mip1

# calculate difference with gd23
# if gd23 lower, will decrement T's
# if gd23 higher, will increment T's

rec_state = 0               # gd23 same
rec_value = 0

if addr_gd23 < rec_mip1:
    rec_state = 1           # gd23 lower
    rec_value = rec_mip1 - addr_gd23

if addr_gd23 > rec_mip1:
    rec_state = 2           # gd23 higher
    rec_value = addr_gd23 - rec_mip1

print()
print('rec_mip1: '+hex(rec_mip1))
print('addr_gd23: '+hex(addr_gd23))
print('rec_value: '+hex(rec_value))
print()

# ----------------------------------------------------------------
# Print Check Addresses

# print(': '+hex())
print('addr_gfxm_header: '+hex(addr_gfxm_header))

for i in range(segments):
    print('addr_segm['+str(i)+']: '+hex(addr_segm[i]))

print('addr_gfxm_data1: '+hex(addr_gfxm_data1))
print('addr_gfxm_pfl: '+hex(addr_gfxm_pfl))
print('addr_gfxm_data2: '+hex(addr_gfxm_data2))
print('addr_gfxm_data3: '+hex(addr_gfxm_data3))

print('addr_segm_dir: '+hex(addr_segm_dir))
print('addr_modl_dir: '+hex(addr_modl_dir))

for i in range(modl_dir_slots):
    if modl_dir_file[i] != '':
        print('modl_dir_addr['+str(i)+']: '+hex(modl_dir_addr[i]))

print('addr_gd1: '+hex(addr_gd1))
print('addr_gd2: '+hex(addr_gd2))
print('addr_gd3: '+hex(addr_gd3))
print('addr_gd4: '+hex(addr_gd4))

print('addr_vector: '+hex(addr_vector))
print('addr_normal: '+hex(addr_normal))
print('addr_short1: '+hex(addr_short1))
print('addr_short2: '+hex(addr_short2))
print('addr_light: '+hex(addr_light))
print('addr_1111: '+hex(addr_1111))
print('addr_ffff: '+hex(addr_ffff))
print('addr_pa: '+hex(addr_pa))

print('addr_gd5: '+hex(addr_gd5))
print('addr_gd6: '+hex(addr_gd6))
print('addr_gd7: '+hex(addr_gd7))
print('addr_gd8: '+hex(addr_gd8))
print('addr_gd9: '+hex(addr_gd9))
print('addr_gd10: '+hex(addr_gd10))
print('addr_gd11: '+hex(addr_gd11))
print('addr_gd12: '+hex(addr_gd12))
print('addr_gd13: '+hex(addr_gd13))
print('addr_gd14: '+hex(addr_gd14))
print('addr_gd15: '+hex(addr_gd15))
print('addr_gd16: '+hex(addr_gd16))
print('addr_gd17: '+hex(addr_gd17))
print('addr_gd18: '+hex(addr_gd18))

print('addr_gf1: '+hex(addr_gf1))

print('addr_gd19: '+hex(addr_gd19))
print('addr_gd20: '+hex(addr_gd20))
print('addr_gd21: '+hex(addr_gd21))
print('addr_gd22: '+hex(addr_gd22))
print('addr_gd23: '+hex(addr_gd23))
print('addr_gd24: '+hex(addr_gd24))


# ----------------------------------------------------------------
# Open, update, write files

fo = open('new_blp', 'wb')

# --------------------------------
# update gfxm header

# copy gfxm header
f = open('_gfxm_header', 'rb')
data = f.read()
f.close()
gfxm = list(data)
del data

# update gfxm header

add_int(int(size_gfxm_pfl/12),4,False,0xc,gfxm)             # en gfxm data

add_int(segments,4,False,0x10,gfxm)                         # en segm
add_int(addr_segm_dir,4,False,0x14,gfxm)                    # aa segm dir
add_int(addr_modl_dir,4,False,0x18,gfxm)                    # aa modl dir
add_int(addr_1111,4,False,0x1c,gfxm)                        # aa 1111

add_int(addr_gfxm_data1,4,False,0x20,gfxm)                  # aa data1
add_int(addr_gd16,4,False,0x24,gfxm)                        # aa gd16
add_int(addr_gfxm_data3,4,False,0x28,gfxm)                  # aa data3
# 0x2c

# 0x30
add_int(addr_normal,4,False,0x34,gfxm)                      # aa normal
add_int(int(size_normal/4),4,False,0x38,gfxm)                    # en normal
add_int(addr_short1,4,False,0x3c,gfxm)                      # aa short1

add_int(addr_short2,4,False,0x40,gfxm)                      # aa short2
add_int(addr_gd4,4,False,0x44,gfxm)                         # aa gd4
add_int(int(size_gd4/32),4,False,0x48,gfxm)                      # en gd4
add_int(addr_light,4,False,0x4c,gfxm)                       # aa light

add_int(addr_ffff,4,False,0x50,gfxm)                        # aa ffff
#
#
#

add_int(addr_pa,4,False,0x60,gfxm)                          # aa pa
add_int(addr_gfxm_pfl,4,False,0x68,gfxm)                    # aa pfl
add_int(addr_gfxm_data2,4,False,0x6c,gfxm)                  # aa data2

# add_int(addr_,4,False,0x,gfxm)                 # aa 

add_int(addr_vector,4,False,0x74,gfxm)                      # aa vector
add_int(addr_gd2,4,False,0x7c,gfxm)                         # aa gd2

add_int(addr_gd1,4,False,0x80,gfxm)                 # aa 
add_int(addr_gd5,4,False,0x84,gfxm)                 # aa 
add_int(addr_gd6,4,False,0x88,gfxm)                 # aa 
add_int(addr_gd7,4,False,0x8c,gfxm)                 # aa 

add_int(addr_gd10,4,False,0x90,gfxm)                 # aa 
add_int(addr_gd8,4,False,0x98,gfxm)                 # aa 
add_int(addr_gd3,4,False,0x9c,gfxm)                 # aa 

add_int(addr_gd10,4,False,0xa0,gfxm)                 # aa 
add_int(addr_gd12,4,False,0xa4,gfxm)                 # aa 
add_int(addr_gd13,4,False,0xa8,gfxm)                 # aa 
add_int(addr_gd19,4,False,0xac,gfxm)                 # aa 

add_int(addr_gd14,4,False,0xb0,gfxm)                 # aa 
add_int(addr_gd15,4,False,0xb8,gfxm)                 # aa 
add_int(addr_gd11,4,False,0xbc,gfxm)                 # aa 

add_int(addr_gd9,4,False,0xc0,gfxm)                 # aa 
add_int(addr_gd17,4,False,0xc4,gfxm)                 # aa 
add_int(addr_gd18,4,False,0xcc,gfxm)                 # aa 

add_int(addr_gd22,4,False,0xd0,gfxm)                 # aa 
add_int(addr_gd20,4,False,0xd4,gfxm)                 # aa 
add_int(addr_gf1,4,False,0xd8,gfxm)                 # aa 

add_int(addr_gd20,4,False,0xe4,gfxm)                 # aa 
add_int(addr_gd20,4,False,0xec,gfxm)                 # aa 

add_int(addr_normal,4,False,0xf8,gfxm)                 # aa 

add_int(addr_gd20,4,False,0x100,gfxm)                 # aa 
add_int(addr_gd21,4,False,0x104,gfxm)                 # aa 
add_int(addr_gd20,4,False,0x108,gfxm)                 # aa 
add_int(addr_gd20,4,False,0x10c,gfxm)                 # aa 

add_int(addr_gd24,4,False,0x110,gfxm)                 # aa 
add_int(addr_gd23,4,False,0x114,gfxm)                 # aa 
add_int(addr_gd20,4,False,0x118,gfxm)                 # aa 

add_int(addr_gd21,4,False,0x120,gfxm)                 # aa 
add_int(addr_gd22,4,False,0x124,gfxm)                 # aa 
add_int(addr_gd22,4,False,0x12c,gfxm)                 # aa 

add_int(addr_gd23,4,False,0x130,gfxm)                 # aa 

# write + delete GFXM Header
fo.write(bytearray(gfxm))
del gfxm

# --------------------------------
# add segments

for i in range(segments):
    
    
    #add_bin('_segm_'+str(i)+'_header', fo)
    
    # open segm header, update then write
    f = open('_segm_'+str(i)+'_header', 'rb')
    data = f.read()
    f.close()
    sheader = list(data)
    del data
    
    add_int(addr_segm_faces[i],4,False,0x10,sheader)
    
    add_int(addr_segm_verts[i],4,False,0x20,sheader)
    add_int(addr_segm_sm1[i],4,False,0x28,sheader)
    add_int(addr_segm_sm2[i],4,False,0x2c,sheader)
    add_int(addr_segm_sm3[i],4,False,0x30,sheader)
    
    add_int(addr_segm_dft1[i],4,False,0x3c,sheader)
    
    add_int(addr_segm_dft2[i],4,False,0x40,sheader)
    add_int(addr_segm_dft3[i],4,False,0x44,sheader)
    
    add_int(addr_segm_faces[i],4,False,0x50,sheader)
    add_int(addr_segm_mip1[i],4,False,0x54,sheader)
    add_int(addr_segm_mip2[i],4,False,0x58,sheader)
    add_int(addr_segm_mip3[i],4,False,0x5c,sheader)
    
    # write + delete
    fo.write(bytearray(sheader))
    del sheader
    
    # add segm files
    
    
    
    #add_bin('_segm_'+str(i)+'_faces', fo)              # try increment texture id
    
    # --------------------------------
    # update faces, change texture id
    f = open('_segm_'+str(i)+'_faces', 'rb')
    data = f.read()
    f.close()
    vv = list(data)
    del data
    
    '''
    ho = 0
    ho2 = 0
    
    for ii in range(int(size_segm_faces[i]/8)):
        
        xx = to_int(vv,(ii*8)+4)
                
        if xx == 0:
            xx = 1
        else:
            xx = 0
            
        xx = ho2
        
        ho += 1
        if ho > 15:
            ho2 += 1
            ho = 0
        
        add_int(xx,4,False,(ii*8)+4,vv)
    '''
    
    # write + delete
    fo.write(bytearray(vv))
    del vv
    # --------------------------------
    
    # have to update mip texture addresses
    #add_bin('_segm_'+str(i)+'_mip1', fo)
    #add_bin('_segm_'+str(i)+'_mip2', fo)
    #add_bin('_segm_'+str(i)+'_mip3', fo)
    
    # --------------------------------
    # update mip1 texture addresses
    f = open('_segm_'+str(i)+'_mip1', 'rb')
    data = f.read()
    f.close()
    mip1 = list(data)
    del data
    
    # see here im trying to inc or dec the address
    # problem is I have to read the address before inc or dec
    #for i in range(int(size_segm_mip1[i]/8)):
    #    add_int(/address/,4,False,(i*8)+4,mip1)
    
    if rec_state != 0:
        for ii in range(int(size_segm_mip1[i]/8)):
            
            xx = to_int(mip1,(ii*8)+4)
            
            if rec_state == 1:
                xx -= rec_value
            if rec_state == 2:
                xx += rec_value
            
            add_int(xx,4,False,(ii*8)+4,mip1)
    
    # write + delete
    fo.write(bytearray(mip1))
    del mip1
    # --------------------------------
    
    # --------------------------------
    # update mip2 texture addresses
    f = open('_segm_'+str(i)+'_mip2', 'rb')
    data = f.read()
    f.close()
    mip2 = list(data)
    del data
    
    if rec_state != 0:
        for ii in range(int(size_segm_mip2[i]/8)):
            
            xx = to_int(mip2,(ii*8)+4)
            
            if rec_state == 1:
                xx -= rec_value
            if rec_state == 2:
                xx += rec_value
            
            add_int(xx,4,False,(ii*8)+4,mip2)
    
    # write + delete
    fo.write(bytearray(mip2))
    del mip2
    # --------------------------------
    
    # --------------------------------
    # update mip3 texture addresses
    f = open('_segm_'+str(i)+'_mip3', 'rb')
    data = f.read()
    f.close()
    mip3 = list(data)
    del data
    
    if rec_state != 0:
        for ii in range(int(size_segm_mip3[i]/8)):
            
            xx = to_int(mip3,(ii*8)+4)
            
            if rec_state == 1:
                xx -= rec_value
            if rec_state == 2:
                xx += rec_value
            
            add_int(xx,4,False,(ii*8)+4,mip3)
    
    # write + delete
    fo.write(bytearray(mip3))
    del mip3
    # --------------------------------
    
    add_bin('_segm_'+str(i)+'_sm1', fo)
    add_bin('_segm_'+str(i)+'_sm2', fo)
    add_bin('_segm_'+str(i)+'_sm3', fo)
    
    #add_bin('_segm_'+str(i)+'_verts', fo)                      # have to fix black ice bug, last byte of the segm verts
    
    # --------------------------------
    # update verts, fix black ice bug
    f = open('_segm_'+str(i)+'_verts', 'rb')
    data = f.read()
    f.close()
    vv = list(data)
    del data
    
    for ii in range(int(size_segm_verts[i]/8)):
        
        xx = to_int(vv,(ii*8)+4)
        
        '''
        if rec_state == 1:
            xx -= rec_value
        if rec_state == 2:
            xx += rec_value
        '''
        
        xx &= 0x00ffffff
        
        add_int(xx,4,False,(ii*8)+4,vv)
    
    # write + delete
    fo.write(bytearray(vv))
    del vv
    # --------------------------------
    
    copl = [0x43,0x4f,0x50,0x4c]
    fo.write(bytearray(copl))
    add_bin('_segm_'+str(i)+'_dft1', fo)
    add_bin('_segm_'+str(i)+'_dft2', fo)
    add_bin('_segm_'+str(i)+'_dft3', fo)
    
# --------------------------------
# add gfxm data

add_bin('_gfxm_data1', fo)
add_bin('_gfxm_pfl', fo)
add_bin('_gfxm_data2', fo)
add_bin('_gfxm_data3', fo)

# --------------------------------
# add segmlist
# !! have to update data
# !! data same if segments == 1

#add_bin('_gfxm_segmlist', fo)

new_segm_dir = [0] * (segments*4)

for i in range(segments):
    add_int(addr_segm[i],4,False,i*4,new_segm_dir)

fo.write(bytearray(new_segm_dir))

# --------------------------------
# add modllist
# !! have to update data

#add_bin('_gfxm_modllist', fo)

# open segm header, update then write
f = open('_gfxm_modllist', 'rb')
data = f.read()
f.close()
mdir = list(data)
del data

for i in range(modl_dir_slots):
    if modl_dir_file[i] != '':
        add_int(modl_dir_addr[i],4,False,i*4,mdir)

# write + delete
fo.write(bytearray(mdir))
del mdir

# --------------------------------
# add models

for i in range(models):    
    add_bin('_modl_'+str(i), fo)

# --------------------------------
# add gd data

add_bin('_gfxm_gd1', fo)
add_bin('_gfxm_gd2', fo)
add_bin('_gfxm_gd3', fo)
add_bin('_gfxm_gd4', fo)

add_bin('_gfxm_vector', fo)
add_bin('_gfxm_normal', fo)
add_bin('_gfxm_short1', fo)
add_bin('_gfxm_short2', fo)
add_bin('_gfxm_light', fo)
add_bin('_gfxm_1111', fo)
add_bin('_gfxm_ffff', fo)
add_bin('_gfxm_pa', fo)

add_bin('_gfxm_gd5', fo)
add_bin('_gfxm_gd6', fo)
add_bin('_gfxm_gd7', fo)
add_bin('_gfxm_gd8', fo)
add_bin('_gfxm_gd9', fo)
add_bin('_gfxm_gd10', fo)
add_bin('_gfxm_gd11', fo)
add_bin('_gfxm_gd12', fo)
add_bin('_gfxm_gd13', fo)
add_bin('_gfxm_gd14', fo)
add_bin('_gfxm_gd15', fo)
add_bin('_gfxm_gd16', fo)
add_bin('_gfxm_gd17', fo)
add_bin('_gfxm_gd18', fo)

add_bin('_gfxm_gf1', fo)

add_bin('_gfxm_gd19', fo)
add_bin('_gfxm_gd20', fo)
add_bin('_gfxm_gd21', fo)
add_bin('_gfxm_gd22', fo)

#zero = [0] * 12                                 # INCA
#fo.write(bytearray(zero))                       # INCA

add_bin('_gfxm_gd23', fo)
add_bin('_gfxm_gd24', fo)

# --------------------------------
# Close file

fo.close()

print('')
print('Process Finished')

# ----------------------------------------------------------------



# ----------------------------------------------------------------
# Header

import bpy

def to_int(data, da):
    return int.from_bytes((data[da], data[da+1], data[da+2], data[da+3]), byteorder='little', signed=False)

# --------------------------------
# read file

in_file = 'blp_file'
f = open(in_file, 'rb')
data = f.read()
fsize=len(data)         # used for gd24
f.close()
#xx = list(data)        # used to modify data

ok = 1			# error check
game = 2		# 0=? 1=RC1 2=RC2 3=FB 4=DW
da = 0

print('----------------------------------------------------------------')
print(fsize)

# ----------------------------------------------------------------
# dump files

if ok:
    if game == 2:
        
        # --------------------------------
        # gd check, gf1 = 0
        
        gd_check = [0] * 25
        
        # --------------------------------
        # gfxm addresses
        
        # this is used to dump other formats
        # have to check if some formats are used
        # also some formats have multiple entries
        
        gmen_data = to_int(data,0xc)
        
        gmen_segm = to_int(data,0x10)
        gmaa_segm = to_int(data,0x14)
        gmaa_modl = to_int(data,0x18)
        
        print('MODL Directory: '+hex(gmaa_modl))
        
        gmaa_1111 = to_int(data,0x1c+0x20)
        
        gmaa_data1 = to_int(data,0x20+0x20)
        if gd_check[16] == 0:
            gmaa_gd16 = to_int(data,0x24+0x20)
            if gmaa_gd16 != 0:
                gd_check[16] = 1
        gmaa_data3 = to_int(data,0x28+0x20)
        # 0x2c
        
        # 0x30
        gmaa_normal = to_int(data,0x34+0x20)
        gmen_normal = to_int(data,0x38+0x20)
        gmaa_short1 = to_int(data,0x3c+0x20)
        
        gmaa_short2 = to_int(data,0x40+0x20)
        if gd_check[4] == 0:
            gmaa_gd4 = to_int(data,0x44+0x20)
            if gmaa_gd4 != 0:
                gd_check[4] = 1
        gmen_gd4 = to_int(data,0x48+0x20)
        gmaa_light = to_int(data,0x4c+0x20)
        
        gmaa_ffff = to_int(data,0x50+0x20)
        # 0x54
        # 0x58
        # 0x5c
        
        gmaa_pa = to_int(data,0x60+0x20)
        # 0x64
        gmaa_pfl = to_int(data,0x68+0x20)
        gmaa_data2 = to_int(data,0x6c+0x20)
        
        # 0x70
        gmaa_vector = to_int(data,0x74+0x20)
        # 0x78
        if gd_check[2] == 0:
            gmaa_gd2 = to_int(data,0x7c+0x20)
            if gmaa_gd2 != 0:
                gd_check[2] = 1
        
        if gd_check[1] == 0:
            gmaa_gd1 = to_int(data,0x80+0x20)
            if gmaa_gd1 != 0:
                gd_check[1] = 1
        if gd_check[5] == 0:
            gmaa_gd5 = to_int(data,0x84+0x20)
            if gmaa_gd5 != 0:
                gd_check[5] = 1
        if gd_check[6] == 0:
            gmaa_gd6 = to_int(data,0x88+0x20)
            if gmaa_gd6 != 0:
                gd_check[6] = 1
        if gd_check[7] == 0:
            gmaa_gd7 = to_int(data,0x8c+0x20)
            if gmaa_gd7 != 0:
                gd_check[7] = 1
        
        if gd_check[10] == 0:
            gmaa_gd10 = to_int(data,0x90+0x20)
            if gmaa_gd10 != 0:
                gd_check[10] = 1
        # 0x94
        if gd_check[8] == 0:
            gmaa_gd8 = to_int(data,0x98+0x20)
            if gmaa_gd8 != 0:
                gd_check[8] = 1
        if gd_check[3] == 0:
            gmaa_gd3 = to_int(data,0x9c+0x20)
            if gmaa_gd3 != 0:
                gd_check[3] = 1
        
        if gd_check[10] == 0:
            gmaa_gd10 = to_int(data,0xa0+0x20)
            if gmaa_gd10 != 0:
                gd_check[10] = 1
        if gd_check[12] == 0:
            gmaa_gd12 = to_int(data,0xa4+0x20)
            if gmaa_gd12 != 0:
                gd_check[12] = 1
        if gd_check[13] == 0:
            gmaa_gd13 = to_int(data,0xa8+0x20)
            if gmaa_gd13 != 0:
                gd_check[13] = 1
        if gd_check[19] == 0:
            gmaa_gd19 = to_int(data,0xac+0x20)
            if gmaa_gd19 != 0:
                gd_check[19] = 1
        
        if gd_check[14] == 0:
            gmaa_gd14 = to_int(data,0xb0+0x20)
            if gmaa_gd14 != 0:
                gd_check[14] = 1
        # 0xb4
        if gd_check[15] == 0:
            gmaa_gd15 = to_int(data,0xb8+0x20)
            if gmaa_gd15 != 0:
                gd_check[15] = 1
        if gd_check[11] == 0:
            gmaa_gd11 = to_int(data,0xbc+0x20)
            if gmaa_gd11 != 0:
                gd_check[11] = 1
        
        if gd_check[9] == 0:
            gmaa_gd9 = to_int(data,0xc0+0x20)
            if gmaa_gd9 != 0:
                gd_check[9] = 1
        if gd_check[17] == 0:
            gmaa_gd17 = to_int(data,0xc4+0x20)
            if gmaa_gd17 != 0:
                gd_check[17] = 1
        # 0xc8
        if gd_check[18] == 0:
            gmaa_gd18 = to_int(data,0xcc+0x20)
            if gmaa_gd18 != 0:
                gd_check[18] = 1
        
        if gd_check[22] == 0:
            gmaa_gd22 = to_int(data,0xd0+0x20)
            if gmaa_gd22 != 0:
                gd_check[22] = 1
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0xd4+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        if gd_check[0] == 0:
            gmaa_gf1 = to_int(data,0xd8+0x20)
            if gmaa_gf1 != 0:
                gd_check[0] = 1
        # 0xdc
        
        # 0xe0
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0xe4+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        # 0xe8
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0xec+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        
        # 0xf0
        # 0xf4
        gmaa_normal = to_int(data,0xf8+0x20)
        # 0xfc
        
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0x100+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0x104+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0x108+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0x10c+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        
        if gd_check[24] == 0:
            gmaa_gd24 = to_int(data,0x110+0x20)
            if gmaa_gd24 != 0:
                gd_check[24] = 1
        if gd_check[23] == 0:
            gmaa_gd23 = to_int(data,0x114+0x20)
            if gmaa_gd23 != 0:
                gd_check[23] = 1
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0x118+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        # 0x11c
        
        '''
        if gd_check[] == 0:
            if gmaa_gd != 0:
                gd_check[] = 1
        '''
        
        if gd_check[20] == 0:
            gmaa_gd20 = to_int(data,0x120+0x20)
            if gmaa_gd20 != 0:
                gd_check[20] = 1
        if gd_check[22] == 0:
            gmaa_gd22 = to_int(data,0x124+0x20)
            if gmaa_gd22 != 0:
                gd_check[22] = 1
        # 0x128
        if gd_check[22] == 0:
            gmaa_gd22 = to_int(data,0x12c+0x20)
            if gmaa_gd22 != 0:
                gd_check[22] = 1
        
        if gd_check[23] == 0:
            gmaa_gd23 = to_int(data,0x130+0x20)
            if gmaa_gd23 != 0:
                gd_check[23] = 1
        
        gmaa_gdex = to_int(data,0x134+0x20)
        
        # 0x134
        # 0x138
        # 0x13c
        
        #gmaa_ = to_int(data,0x)
        
        for i in range(25):
            if gd_check[i] == 0 and i != 21 and i !=4:
                ok = 0
        
        if ok == 0:
            for i in range(25):
                if gd_check[i] == 0:
                    print('ERROR: gd zero: gd'+str(i))

        if ok:

            # --------------------------------
            # dump gfxm header
            
            bb = [0] * (0x140)

            for i in range(0x140):
                bb[i] = data[i]
            
            f = open('_gfxm_header', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # loop segms
            
            print('gmen_segm: '+str(gmen_segm))
            print('gmaa_segm: '+hex(gmaa_segm))
            
            for s in range(gmen_segm):
                
                segm_header = to_int(data,gmaa_segm + (s*4))
                print('segm_header: '+hex(segm_header))
                
                # --------------------------------
                # dump segm header
                
                bb = [0] * (0x60)
                
                for i in range(0x60):
                    bb[i] = data[segm_header+i]
                
                f = open('_segm_'+str(s)+'_header', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # segm addresses
                
                smen_width = to_int(data,segm_header+0x8)
                smen_length = to_int(data,segm_header+0xc)
                
                smaa_faces = to_int(data,segm_header+0x10)
                smaa_verts = to_int(data,segm_header+0x20)
                
                smaa_sm1 = to_int(data,segm_header+0x28)
                smaa_sm2 = to_int(data,segm_header+0x2c)
                smaa_sm3 = to_int(data,segm_header+0x30)
                
                smaa_dft1 = to_int(data,segm_header+0x3c)
                smaa_dft2 = to_int(data,segm_header+0x40)
                smaa_dft3 = to_int(data,segm_header+0x44)
                
                #smaa_faces = to_int(data,segm_header+0x50)
                smaa_mip1 = to_int(data,segm_header+0x54)
                smaa_mip2 = to_int(data,segm_header+0x58)
                smaa_mip3 = to_int(data,segm_header+0x5c)
                
                print('smen_width: '+str(smen_width))
                print('smen_length: '+str(smen_length))
                
                smen_faces = smen_width * smen_length
                smen_verts = (smen_width+1) * (smen_length+1)
                
                # --------------------------------
                # dump faces
                
                bb = [0] * (smen_faces * 8)
                
                for i in range(smen_faces * 8):
                    bb[i] = data[smaa_faces+i]
                
                f = open('_segm_'+str(s)+'_faces', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump mip1
                
                bb = [0] * (smaa_mip2 - smaa_mip1)
                
                for i in range(smaa_mip2 - smaa_mip1):
                    bb[i] = data[smaa_mip1+i]
                
                f = open('_segm_'+str(s)+'_mip1', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump mip2
                
                bb = [0] * (smaa_mip3 - smaa_mip2)
                
                for i in range(smaa_mip3 - smaa_mip2):
                    bb[i] = data[smaa_mip2+i]
                
                f = open('_segm_'+str(s)+'_mip2', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump mip3
                
                bb = [0] * (smaa_sm1 - smaa_mip3)
                
                for i in range(smaa_sm1 - smaa_mip3):
                    bb[i] = data[smaa_mip3+i]
                
                f = open('_segm_'+str(s)+'_mip3', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump verts
                
                bb = [0] * (smen_verts * 8)
                
                for i in range(smen_verts * 8):
                    bb[i] = data[smaa_verts+i]
                
                f = open('_segm_'+str(s)+'_verts', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # check smalls
                
                sm1_size = to_int(data,smaa_sm1)
                sm2_size = to_int(data,smaa_sm2)
                sm3_size = to_int(data,smaa_sm3)
                
                # --------------------------------
                # dump sm1
                
                # 1*uint + size*ushorts + 1 ushort if odd number
                # use (size and 1) for 1 or 0
                
                print(sm1_size)
                print((sm1_size&1)*2)
                print(sm2_size)
                print((sm2_size&1)*2)
                print(sm3_size)
                print((sm3_size&1)*2)
                
                # sm1
                
                bb = [0] * (4 + (sm1_size*2) + ((sm1_size&1)*2))
                
                for i in range(4 + (sm1_size*2) + ((sm1_size&1)*2)):
                    bb[i] = data[smaa_sm1+i]
                
                f = open('_segm_'+str(s)+'_sm1', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump sm2
                
                bb = [0] * (4 + (sm2_size*2) + ((sm2_size&1)*2))
                
                for i in range(4 + (sm2_size*2) + ((sm2_size&1)*2)):
                    bb[i] = data[smaa_sm2+i]
                
                f = open('_segm_'+str(s)+'_sm2', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump sm3
                
                bb = [0] * (4 + (sm3_size*2) + ((sm3_size&1)*2))
                
                for i in range(4 + (sm3_size*2) + ((sm3_size&1)*2)):
                    bb[i] = data[smaa_sm3+i]
                
                f = open('_segm_'+str(s)+'_sm3', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump dft1
                
                bb = [0] * (smaa_dft2 - smaa_dft1)
                
                for i in range(smaa_dft2 - smaa_dft1):
                    bb[i] = data[smaa_dft1+i]
                
                f = open('_segm_'+str(s)+'_dft1', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump dft2
                
                bb = [0] * (smaa_dft3 - smaa_dft2)
                
                for i in range(smaa_dft3 - smaa_dft2):
                    bb[i] = data[smaa_dft2+i]
                
                f = open('_segm_'+str(s)+'_dft2', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
                # --------------------------------
                # dump dft3
                
                # this wont work for dft3, if multiple segments, big files
                # be best to do below code for other dfts
                
                '''
                bb = [0] * (gmaa_data1 - smaa_dft3)
                
                for i in range(gmaa_data1 - smaa_dft3):
                    bb[i] = data[smaa_dft3+i]
                
                f = open('_segm_'+str(s)+'_dft3', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                '''
                
                # the dft size is, mip with (small_size - 1) * mip length (divide segm length) /2 (nibbles)
                # the dfts are 4 byte aligned at the end, padded with zeros, so the next format can start on 0x0, 0x4, 0x8 or 0xc.
                # the dft3 is 4 byte aligned, then has nibbles with 8s till 16 byte aligned, so the next format can start on 0x0.
                
                dft1_size = int((int(smen_length /2) * (sm1_size-1)) / 2)
                dft2_size = int((int(smen_length /4) * (sm2_size-1)) / 2)
                dft3_size = int((int(smen_length /8) * (sm3_size-1)) / 2)
                
                '''
                print('....')
                
                print(int(smen_length /2))
                print(int(smen_length /4))
                print(int(smen_length /8))
                
                print((sm1_size-1))
                print((sm2_size-1))
                print((sm3_size-1))
                
                print(dft1_size)
                print(dft2_size)
                print(dft3_size)
                '''
                
                next = int(smaa_dft3 + dft3_size)
                
                print(hex(next))
                
                # increment next till address 0x0
                while(next&0xf):
                    next += 1
                
                print(hex(next))
                    
                bb = [0] * (next - smaa_dft3)
                
                for i in range(next - smaa_dft3):
                    bb[i] = data[smaa_dft3+i]
                
                f = open('_segm_'+str(s)+'_dft3', 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
                
            # --------------------------------
            # dump gfxm data 1
            
            curr = gmaa_data1
            next = gmaa_pfl
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_data1', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gfxm PFL
            
            curr = gmaa_pfl
            next = gmaa_data2
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_pfl', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gfxm data 2
            
            curr = gmaa_data2
            next = gmaa_data3
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_data2', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gfxm data 3
            
            curr = gmaa_data3
            next = gmaa_segm
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_data3', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump segm directory
            
            curr = gmaa_segm
            next = gmaa_segm + (gmen_segm*4)
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_segmlist', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump modl directory
            
            vmods = 28
            
            curr = gmaa_modl
            next = gmaa_modl + (vmods*4)
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_modllist', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # todo
            
            '''
            Next I have the dump the MODLs + MODL data
            Using the difference for the size will save many calculations
            May be able to use MODL dir for difference addresses
            Format after last is gfxm gd 1.
            '''
            
            # --------------------------------
            # compile a list of valid modl addresses
            
            madd_entries = 0
            madd_curr = []
            madd_next = []
            madd_dirr = []
            
            # madd_dirr should be dumped to a different list
            # so the recompiler can add to modls to the correct address
            # its a list of address ids
            
            for i in range(vmods):
                add = to_int(data,gmaa_modl+(i*4))
                if add!=0:
                    madd_curr.append(add)
                    madd_dirr.append(i)
                    madd_entries+=1
            
            for i in range(madd_entries-1):
                madd_next.append(madd_curr[i+1])
                
            madd_next.append(gmaa_gd1)
            
            for i in range(madd_entries):
                print('madd_curr: '+hex(madd_curr[i])+' madd_next: '+hex(madd_next[i]))
            
            # --------------------------------
            # dump modls
            
            for mm in range(madd_entries):
                
                curr = madd_curr[mm]
                next = madd_next[mm]
                
                bb = [0] * (next - curr)
                    
                for i in range(next - curr):
                    bb[i] = data[curr+i]
                
                f = open('_modl_'+str(mm), 'wb')
                f.write(bytearray(bb))
                f.close()
                
                del bb
            
            # --------------------------------
            # dump gd1
            
            curr = gmaa_gd1
            next = gmaa_gd2
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd1', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd2
            
            curr = gmaa_gd2
            next = gmaa_gd3
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd2', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd3
            
            curr = gmaa_gd3
            next = gmaa_vector      # changed for DW
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd3', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd4
            
            # disabled for DW
            
            '''
            curr = gmaa_gd4
            next = gmaa_vector
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd4', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            '''
            
            # --------------------------------
            # dump vector
            
            curr = gmaa_vector
            next = gmaa_normal
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_vector', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump normal
            
            curr = gmaa_normal
            next = gmaa_normal + (gmen_normal*4)
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_normal', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump short1
            
            curr = gmaa_short1
            next = gmaa_short2
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_short1', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump short2
            
            curr = gmaa_short2
            next = gmaa_light
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_short2', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump light
            
            curr = gmaa_light
            next = gmaa_light + 0x1000
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_light', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump 1111
            
            curr = gmaa_1111
            next = gmaa_ffff
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_1111', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump ffff
            
            curr = gmaa_ffff
            next = gmaa_pa
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_ffff', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump pa
            
            curr = gmaa_pa
            next = gmaa_gd5
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_pa', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd5
            
            curr = gmaa_gd5
            next = gmaa_gd6
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd5', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd6
            
            curr = gmaa_gd6
            next = gmaa_gd7
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd6', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd7
            
            curr = gmaa_gd7
            next = gmaa_gd8
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd7', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd8
            
            curr = gmaa_gd8
            next = gmaa_gd9
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd8', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd9
            
            curr = gmaa_gd9
            next = gmaa_gd10
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd9', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd10
            
            curr = gmaa_gd10
            next = gmaa_gd11
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd10', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd11
            
            curr = gmaa_gd11
            next = gmaa_gd12
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd11', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd12
            
            curr = gmaa_gd12
            next = gmaa_gd13
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd12', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd13
            
            curr = gmaa_gd13
            next = gmaa_gd14
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd13', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd14
            
            curr = gmaa_gd14
            next = gmaa_gd15
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd14', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd15
            
            curr = gmaa_gd15
            next = gmaa_gd16
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd15', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd16
            
            curr = gmaa_gd16
            next = gmaa_gd17
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd16', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd17
            
            curr = gmaa_gd17
            next = gmaa_gd18
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd17', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd18
            
            curr = gmaa_gd18
            next = gmaa_gf1
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd18', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gf1
            
            curr = gmaa_gf1
            next = gmaa_gd19
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gf1', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd19
            
            curr = gmaa_gd19
            next = gmaa_gd20
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd19', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd20
            
            curr = gmaa_gd20
            next = gmaa_gd22    # changed for DW
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd20', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd21
            
            # disabled for DW
            
            '''
            curr = gmaa_gd21
            next = gmaa_gd22
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd21', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            '''
            
            # --------------------------------
            # dump gd22
            
            curr = gmaa_gd22
            next = gmaa_gd23
            
            bb = [0] * (next - curr)
                
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd22', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd23
            
            curr = gmaa_gd23
            next = gmaa_gd24
            
            bb = [0] * (next - curr)
            
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd23', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gd24
            
            curr = gmaa_gd24
            next = gmaa_gdex        # changed for DW
            
            bb = [0] * (next - curr)
            
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gd24', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb
            
            # --------------------------------
            # dump gdex
            
            # added for DW
            # have to calc or use end address
            
            curr = gmaa_gdex
            next = fsize
            
            bb = [0] * (next - curr)
            
            for i in range(next - curr):
                bb[i] = data[curr+i]
            
            f = open('_gfxm_gdex', 'wb')
            f.write(bytearray(bb))
            f.close()
            
            del bb

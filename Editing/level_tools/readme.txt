// Level Tools

I was able to add a new set of segments (The roads) to a rollcage 2 level recently, the level was a bit buggy but it worked.
First I made a level with the Ducati level editor, then I converted it to an RC2 level, using the road segments from the ducati level.
Then I tried to fix some texture errors.

I added some zip files below which contains some tools and docs.
I only recommend using these tools if you have experience messing about with files. :)

Making levels this way is pretty odd at the moment, it would probably be better to make better tools before trying to make a good level.
I cant promise the tools will work for all levels, I was pretty suprised when it worked and I only tested the tools on a few types of levels.

I added the mingw/windows version of the idxtool below, I found people found it odd to compile, I hope thats ok.
I hope others can make some discoveries, maybe some better tools could be made in the future.

Enjoy.

// Tools

These are the files included in the level_tools.zip file.
These are the files I used to make a level for Rollcage 2.

I also used Blender 3D 2.72b for the texture addon.
Blender can also execute python scripts from the text editor.
Before running python scripts, the blend file should be saved in the same directory as the level files.

'level_tools.zip'
	readme.txt	This file.
	blpdoc.txt	Some info about the Rollcage 2 level level files.
	123_tiles.png	A picture showing how texture tiles are mapped.
	ducks.png	A picture showing the ducati level working in RC2.
	dduc.py		A python script to extract Ducati World level files.
	drc2.py		A python script to extract Rollcage 2 level files.
	crc2.py		A python script to compile Rollcage 2 level files.
	addon_btp.py	A blender addon to import export btp textures, this can be used to fix some of the texture errors.
	CC_NORMA.blp	A Ducati World level made with lvled.exe
	CC_NORMA.btp	The textures for the Ducati World level.
	rc2level.blp	The Ducati World level (blp file) converted to Rollcage 2 level.

// How to convert a level

Its not easy to do this with these tools, but I wrote it down anyway.

1: Make a level with the ducati level editor (lvled.exe).
   For RC1 there should be 6 car start positions, for RC2 there should be 5 car start positions.
2: Make a new folder 'lvl_duc', then extract the DW level files from the blp file with the 'dduc.py' script.
   First change the filename in the 'dduc.py' script to read level file, look for "in_file = 'blp_file'" near the start.
   Also you will have to set the number of 'modl directory addresses' for the level, check below for how to do this.
3: Extract the rollcage.img file with the IDXTool somewhere.
4: Make a new folder 'lvl_rc2', then Extract a RC2 level file with 'drc2.py'.
   The level files, when opened with a text editor have 'GFXM' at the start, the file after that is the level textures 'BTP'.
   First change the filename in the 'drc2.py' script to read level file, look for "in_file = 'blp_file'" near the start.
   Also you will have to set the number of 'modl directory addresses' for the level, check below for how to do this.
5: Make a new folder 'lvl_new' with with a copy of the 'lvl_rc2' files.
6: Copy the following files from the 'lvl_duc' folder, to the 'lvl_new' folder.
   This will use the most the segment data from the ducati level, and the other data from the RC2 level.

	All files beginning with '_segm'.
	_gfxm_data1
	_gfxm_data2
	_gfxm_data3
	_gfxm_gd16
	_gfxm_gd17
	_gfxm_gd19
	_gfxm_gd23
	_gfxm_normal
	_gfxm_pfl

7: Open 'crc2.py', locate the code near the start 'segments = 1'.
   This code tells the compiler how segments to add, set this to number of segments the new level uses.
   If the level uses _segm_0 to _segm_5, thats six segments.
   When finished, save the python file.
8: Open 'crc2.py', locate the code after '# modl sizes'.
   This code is used to write the MODL Directory to the level.
   It will have to match the MODL Directory you are using for the new level.
   You will have to add the modl file names in sequence, check below for how to do this.
   When finished, save the python file.
9: Compile the new RC2 level with 'crc2.py'.
10: Replace one of the RC2 level files with the new RC2 level 'new_blp'.
11: Compile the rollcage.img with the IDXTool.
12: When testing the level:
    If the game crashes before the level loads, you probably missed a step, or some unknown error happend.
    If the level loads, well done.

// Texture Errors

The texture addon is really slow at exporting textures, so only do this if you really want to.

If you want to fix the textures, with the texture addon, its probably best to start with the rollcage textures.
Many of the rc2 textures are used for menus, models, video fx.
The rc2 segm mip texture can be replaced with the ducati segm mip texture easy.
Other textures are harder to fix because of uv mapping, but it may be possible.

The texture addon is quick to import textures but takes a long time to export textures, you may have to wait 10 minutes before the new BTP is compiled, you can check the python console window for progress while waiting.
The reason the addon is slow to export a texture is the code which adds colors to, and checks the pallet for each pixel.

Before exporting textures you have to modify the '# Texture Image List' for the exporter.
This is so the exporter knows how many, and which images to export from blender.

// Extract MODL Directory

For this youll require a hex editor and some code experience.

When extracting levels, each level has an unknown number of 4 byte addresses for the modl directory.
Some addresses are valid addresses and some are null '00000000'.
The first time you try to extract the level, it will print the address of the MODL Directory in the console.

With a hex editor, youll have to count the number of 4 byte addresses from this address.
The last address is before 4 bytes (4d 4f 44 4c) which = 'MODL' in acsii, this is first MODL entry, not a modl directory address.

Below is an example with 34 addresses, 29 valid, 5 null (00000000).

                        a0 cf 04 00 00 00 00 00 
e0 e7 04 00 34 f2 04 00 90 f4 04 00 30 f8 04 00 
58 fd 04 00 00 00 00 00 00 00 00 00 28 01 05 00 
6c 0c 05 00 00 00 00 00 00 00 00 00 44 15 05 00 
d4 19 05 00 a0 1e 05 00 30 23 05 00 ac 2a 05 00 
28 33 05 00 40 3b 05 00 dc 45 05 00 38 48 05 00 
74 49 05 00 f0 4a 05 00 24 4c 05 00 68 4d 05 00 
b4 4e 05 00 90 50 05 00 b0 54 05 00 8c 58 05 00 
dc 70 05 00 2c 89 05 00 7c a1 05 00 cc b9 05 00 
4d 4f 44 4c                                      <- '4d 4f 44 4c' == 'MODL', not a modl directory address.

When youve counted the number of modl directory addresses, open 'dduc.py' or 'drc2.py', locate the code '# dump modl directory', change the number of 'vmods', so its the same for the level.
Save the python file, then when you run the script again, it should extract the correct number of models.

// Compile MODL Directory

For this youll require a hex editor and some code experience.

Open 'crc2.py', locate the code after '# modl sizes'.
This code is used to write the MODL Directory to the level.
It will have to match the MODL Directory + MODLs you are using for the new level.

First set the number modl directory addresses, change 'modl_dir_slots' to match the number.
This should equal the number of valid and null modl addresses.

Then you will have to add the modl file names in sequence.
First check the modl directory using a hex editor.
The modl file names should be added in sequence, 'modl_0', 'modl_1', 'modl_2', etc.
If the modl directory address was '00000000', then add a null file name ''.

It should look something like this, but different depending on the level.

modl_dir_slots = 7
modl_dir_file = [''] * modl_dir_slots
modl_dir_size = [0] * modl_dir_slots
modl_dir_addr = [0] * modl_dir_slots
size_modl_block = 0

modl_dir_file[0] = '_modl_0'	# valid address, add next modl
modl_dir_file[1] = '_modl_1'	# valid address, add next modl
modl_dir_file[2] = '_modl_2'	# valid address, add next modl
modl_dir_file[3] = ''		# null address, add null string ''
modl_dir_file[4] = ''		# null address, add null string ''
modl_dir_file[5] = '_modl_3'	# valid address, add next modl
modl_dir_file[6] = '_modl_4'	# valid address, add next modl

When finished, save the python file.







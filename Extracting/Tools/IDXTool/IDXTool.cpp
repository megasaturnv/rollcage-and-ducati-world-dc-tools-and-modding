
// Written by Spont and Slinger @ forum.recaged.net
// Modifed to compile for windows with MinGW
// Updated by potterman28wxcv

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <assert.h>

using namespace std;
ofstream listout;
ifstream listin;


class GT20Buffer {
public:
	GT20Buffer(string filename);
	GT20Buffer(unsigned char* filedata, int filesize);
	~GT20Buffer();

	void store(string filename);
private:
	unsigned char* data_in;
	unsigned int   size_in;

	unsigned char* data_out;
	unsigned int   size_out;

	int ptr_in;
	int ptr_out;

	unsigned int ctrlBits;
	int bitsRemaining;

	void init();
	unsigned int getWord(unsigned int offset);
	int getLE32(unsigned int offset);

	bool getNextControlBit();
	int getNextLE32();
	unsigned int getNextWord();
	unsigned char getNextByte();

	void copyFromInput(unsigned int count);
	void copyFromOutput(int offset, unsigned int count);
	void copy(unsigned char* src, unsigned int count);
};


class IMGExtractor {
public:
	IMGExtractor();
	IMGExtractor(string idxfile, string imgfile);
	IMGExtractor(string idxfile, string imgfile, string namefile);
	~IMGExtractor();

private:
	string idx;
	string img;
	string namefile; // File containing a list of names to use for extraction, or a file to create
	bool writeNamefile; // whether namefile should be read or written

	unsigned int size_idx;
	unsigned int size_img;

	void init();

	unsigned int readLE32(ifstream* in);
};


class IDXCreator {
	public:
		IDXCreator(const char *idx, const char *img);
		IDXCreator(const char *idx, const char *img, const char *namefile);

	private:
		ofstream idxout, imgout;
		ifstream listin;

		unsigned int position;

		void appendFile(const char *file);
		void appendIDX(unsigned int position, unsigned int size);
};


int main(int argc, char *argv[])
{
	if (argc == 2&& argv[1][0] == 'x')
	{
		new IMGExtractor();
		return 0;
	}
	else if (argc == 4&& argv[1][0] == 'x')
	{
		new IMGExtractor(argv[2], argv[3]);
		return 0;
	}
	else if (argc == 5&& argv[1][0] == 'x')
	{
		new IMGExtractor(argv[2], argv[3], argv[4]);
		return 0;
	}
	else if (argc == 2&& argv[1][0] == 'c')
	{
		new IDXCreator("rollcage.idx", "rollcage.img");
		return 0;
	}
	else if (argc == 4&& argv[1][0] == 'c')
	{
		new IDXCreator(argv[2], argv[3]);
		return 0;
	}
	else if (argc == 5 && argv[1][0] == 'c') {
		new IDXCreator(argv[2], argv[3], argv[4]);
		return 0;
	}

	cout << "idxtool (IMGhack) Usage:" << endl << endl;
	cout<< "Extract, default filenames:	" << argv[0] << " x" << endl;
	cout<< "Extract, manual filenames:	" << argv[0] << " x FILE.idx FILE.img" << endl;
	cout<< "Extract, manual filenames with namelist:	" << argv[0] << " x FILE.idx FILE.img NAMELIST.txt" << endl;
	cout<< "Create, default filenames:	" << argv[0] << " c" << endl;
	cout<< "Create, manual filenames:	" << argv[0] << " c FILE.idx FILE.img" << endl;
	cout<< "Create, manual filenames, non default namelist:	" << argv[0] << " c FILE.idx FILE.img NAMELIST.txt" << endl;
	return 0;
}

/*
 * IDXCreator
 */

IDXCreator::IDXCreator(const char* idx, const char* img) {
	IDXCreator(idx, img, "list.txt");
}

IDXCreator::IDXCreator(const char *idx, const char *img, const char *namefile)
{
	listin.open(namefile, ios::in);
	if (!listin)
	{
		cerr << "Error: '" << namefile << "' could not be opened" << endl;
		exit(1);
	}

	idxout.open(idx, ios::out | ios::binary);
	if (!idxout)
	{
		cerr << "Error: " << idx << " could not be opened" << endl;
		exit(1);
	}

	imgout.open(img, ios::out | ios::binary);
	if (!imgout)
	{
		cerr << "Error: " << img << " could not be opened" << endl;
		exit(1);
	}

	string name;
	position=0;
	while (listin >> name)
		appendFile(name.c_str());

	listin.close();
	idxout.close();
	imgout.close();
}

void IDXCreator::appendFile(const char *file)
{
	ifstream in(file, ios::binary);
	if (!in)
	{
		cerr << "Error: " << file << " could not be opened" << endl;
		exit(1);
	}

	in.seekg (0, ios::end);
	unsigned int size = in.tellg();
	in.seekg (0, ios::beg);

	char *buffer = new char[size];
	in.read(buffer, size);
	imgout.write(buffer, size);
	delete[] buffer;

	appendIDX(position, size);

	position+=size;
}

void IDXCreator::appendIDX(const unsigned int pos, const unsigned int size)
{
	unsigned int meta[4];

	meta[0]=pos; //offset
	meta[1]=size; //compressed size
	meta[2]=size; //decompressed size
	meta[3]=0; //disable compression

	idxout.write((char*)meta, 4*sizeof(unsigned int));
}

//------------------------------------
// IMGExtractor Methods
//
IMGExtractor::IMGExtractor() {
	idx = "ROLLCAGE.IDX";
	img = "ROLLCAGE.IMG";
	namefile = "list.txt";
	writeNamefile = true;

	init();
}


IMGExtractor::IMGExtractor(string idxfile, string imgfile) {
	idx = idxfile;
	img = imgfile;
	namefile = "list.txt";
	writeNamefile = true;

	init();
}

IMGExtractor::IMGExtractor(string idxfile, string imgfile, string namefile) {
	idx = idxfile;
	img = imgfile;
	this->namefile = namefile;
	writeNamefile = false;

	init();
}

void IMGExtractor::init() {
	ifstream in_idx;

	in_idx.open(idx.c_str(), ios::binary);

	if(!in_idx) {
		cerr << "Error: idx file could not be opened" << endl;
		exit(1);
	}

	in_idx.seekg (0, ios::end);
	size_idx = in_idx.tellg();
	in_idx.seekg (0, ios::beg);


	ifstream in_img;

	in_img.open(img.c_str(), ios::binary);

	if(!in_img) {
		cerr << "Error: img file could not be opened" << endl;
		exit(1);
	}

	in_img.seekg (0, ios::end);
	size_img = in_img.tellg();
	in_img.seekg (0, ios::beg);


	// start reading from index
	int numFiles = size_idx / 16;

	if (writeNamefile)
		listout.open(namefile, ios::out);
	else
		listin.open(namefile, ios::in);

	for ( int i = 0; i < numFiles; i++ ) {
		unsigned int offset = readLE32(&in_idx);
		unsigned int size_in = readLE32(&in_idx);
		unsigned int size_out = readLE32(&in_idx);
		unsigned int additional = readLE32(&in_idx);

		std::ostringstream result;
		if (writeNamefile)
			result << std::setfill('0') << std::setw(3) << i;
		else {
			std::string line;
			listin >> line;
			result << line;
		}

		cout << "Writing:" << result.str() << " Compressed:" << size_in << " Unpacked:" << size_out << " Strange (compression method?):" << additional << endl;
		//listout << "File:" << result.str() << " Strange:" << additional;
		if (writeNamefile)
			listout << result.str() << endl;

		//out.write((char*)data_out, size_out);

		unsigned char* data_in = (unsigned char*)malloc(size_in);
		in_img.read ((char*)data_in,size_in);

		GT20Buffer* b = new GT20Buffer(data_in, size_in);

		b->store(result.str());
		delete b;
	}

	if (writeNamefile)
		listout.close();
	else
		listin.close();

	in_idx.close();
	in_img.close();
}


unsigned int IMGExtractor::readLE32(ifstream* in) {
	return in->get() +
		   (in->get() << 8) +
		   (in->get() << 16) +
		   (in->get() << 24);
}



//------------------------------------
// GT20 Methods
//
GT20Buffer::GT20Buffer(string filename) {
	ifstream indata;

	indata.open(filename.c_str(), ios::binary);

	if(!indata) {
		cerr << "Error: file could not be opened" << endl;
		exit(1);
	}

	indata.seekg (0, ios::end);
	size_in = indata.tellg();
	indata.seekg (0, ios::beg);

	data_in = (unsigned char*)malloc(size_out+1);

	indata.read ((char*)data_in,size_in);
	indata.close();

	init();
}


GT20Buffer::GT20Buffer(unsigned char* filedata, int filesize) {
	data_in = filedata;
	size_in = filesize;

	init();
}


GT20Buffer::~GT20Buffer() {
	free( data_in );

	if ( data_in != data_out ) {
		free( data_out );
	}
}

void GT20Buffer::init() {
	// check for GT20 header
	if ( strncmp((const char*)data_in, "GT20", 4) != 0 ) {
		// Not compressed - point output buffer to input buffer
		data_out = data_in;
		size_out = size_in;
		//listout << " Additional data:" << endl;
		//cout << "not compressed" << endl;
		return;
	}

	unsigned int additional = getLE32(8);

	size_out = getLE32(4);
	data_out = (unsigned char*)malloc(size_out + 3);

	// decompression buffer size (if provided), is at offset(16)
	//
	ptr_in = 16;
	ptr_out = 0;

	bitsRemaining = 0;

	int copyOffset;


	//unsigned int totcount=0;

	while ( ptr_in < size_in - 3 ) {
		if ( getNextControlBit() == false ) {
			// 0 - no compression
			copyFromInput(1);
			//totcount+=1;
		} else {
			// 1 - some compression
			if ( getNextControlBit() == false ) {
				// 10 - direct copy two bytes
				// from earlier in the output buffer
				unsigned char b = getNextByte();

				copyOffset = b-256;
				copyFromOutput(copyOffset, 2);

				//totcount+=2;
				if ( getNextControlBit() == true ) {
					// 101 - and two more
					copyFromOutput(copyOffset, 2);
					//totcount+=2;
				}

				if ( getNextControlBit() == true ) {
					// 1011 - and one more
					copyFromOutput(copyOffset, 1);
					//totcount+=1;
				}

			} else {
				// 11 - get combined copy offset + count
				unsigned int w = getNextWord();
				unsigned int copyBits = ( w & 0xFFF8 )>>3;
				copyOffset = copyBits - 0x2000;

				unsigned int count = w & 7;

				if ( count == 0 ) {
					// extension:
					//  we need to copy > 7 bytes,
					//  and/or we need to copy from earlier in the stream
					unsigned char b = getNextByte();

					if ( b & 128 ) {
						// bit indicates copy from even earlier in stream
						copyOffset -= 0x2000;
					}

					count = (b & 127);

					if ( count == 0 ) {
						// copy count requires 7>16 bits
						count = getNextWord();
					} else {
						count += 2;
					}
				} else {
					count += 2;
				}

				copyFromOutput(copyOffset, count);
				//totcount+=count;
			}
		}
	}



	if (ptr_out != size_out && (ptr_out - size_out != 3)) {
		cout << "in:  " << ptr_in << " of " << size_in << endl;
		cout << "out: " << ptr_out << " of " << size_out << endl;
		cout << "ctl: " << bitsRemaining << endl;
	}


	/*
	unsigned int left = totcount - size_out;
	//listout << " Additional data: ";
	if (left)
	{
		cout << "data left:";
		for (int i=0;i<left;i++){
		    //listout << (int)data_out[size_out+i]<<":";
		    cout << (int)data_out[size_out+i]<<":";
		}
		cout << endl;
	}
	*/
	//listout <<endl;
}


unsigned char GT20Buffer::getNextByte() {
	return data_in[ptr_in++];
}


unsigned int GT20Buffer::getWord(unsigned int offset) {
	assert( offset + 2 <= size_in );

	return data_in[offset] +
			(data_in[offset+1] <<  8);
}


unsigned int GT20Buffer::getNextWord() {
	unsigned int v = getWord(ptr_in);
	ptr_in += 2;
	return v;
}


int GT20Buffer::getLE32(unsigned int offset) {
	assert( offset + 4 <= size_in );

	return data_in[offset] +
			(data_in[offset+1] <<  8) +
			(data_in[offset+2] << 16) +
			(data_in[offset+3] << 24);
}


int GT20Buffer::getNextLE32() {
	int v = getLE32(ptr_in);
	ptr_in += 4;
	return v;
}


bool GT20Buffer::getNextControlBit() {
	if ( bitsRemaining == 0 ) {
		ctrlBits = getNextLE32();
		bitsRemaining = 32;
	}

	bool bit = ctrlBits & (1<<(32-bitsRemaining));
	bitsRemaining--;

	return bit;
}


void GT20Buffer::copyFromInput( unsigned int count) {
	assert(count < (1<<16));
	assert(ptr_in + count <= size_in);

	copy(data_in + ptr_in,count);

	ptr_in += count;

}


void GT20Buffer::copyFromOutput( int offset, unsigned int count) {
	assert(count < (1<<16));
	assert(ptr_out + offset >= 0);
	assert(ptr_out + offset + count <= size_out);

	copy(data_out+ptr_out+offset,count);
}


void GT20Buffer::copy(unsigned char* src, unsigned int count) {
	for ( unsigned int i = 0; i < count; i++ ) {
		unsigned char c = src[i];

		data_out[ptr_out] = c;
		ptr_out++;
	}
}


void GT20Buffer::store(string filename) {
	 ofstream out(filename.c_str(), ios::out | ios::binary);

	 out.write((char*)data_out, size_out);
	 out.close();
}


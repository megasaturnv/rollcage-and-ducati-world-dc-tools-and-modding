# IDXtool

IDXtool was originally written by Spont and Slinger @ forum.recaged.net

It has been updated by potterman28wxcv @ steamcommunity.com

This documentation is written by potterman28wxcv @ steamcommunity.com

## Description

Rollcage games, Ducati, and possibly other ATD titles, have their data inside a .IMG/.IDX file structure. The .IDX contains information about the storage of each file inside the .IMG - the .IMG is pure data.

The file structure supports file compression (via a GT20 algorithm proper to ATD) and also supports a linked list to be able to create duplicate files (similar to a symbolic link).

The IDXTool supports decompression, but it does not support compression. So if you produce a new Rollcage.img out of it, it will be around 250 MB versus the original 75 MB.

The IDXtool has two modes:
- Extracting the files contained in a .idx/.img pair. This, in turn, creates a `file.txt` containing the file handles (integers).
- Importing a list of files described in a `file.txt` text file, to create a new .idx/.img pair, uncompressed, and without symbolic link.

The name of the files is not encoded within the .IMG/.IDX structure.
Instead, these are apparently hardcoded within the game code.

This repository contains a `rclist.txt` containing the file names of the 343 extracted files from the Rollcage.img.
Using it in the extraction command will create the files with the correct extensions and names, instead of them being numbered from 001 to 343 (except 004 to 007, whose content is unknown).

    ./idxtools.exe x ROLLCAGE.idx ROLLCAGE.img rclist.txt

## General usage

    ./idxtools.exe (x|c) [idxfile] [imgfile] [namelistfile]

`x` for extracting from the IDX/IMG all the files

`c` for creating a IDX/IMG out of all the files

`idxfile` is Rollcage.idx by default

`imgfile` is Rollcage.img by default

`namelistfile` is list.txt by default (when used with c)

If no `namelistfile` is provided for `x` command, the tool numbers files from 001 to 343 (or whatever is the number of files present in the IDX), then write the list of filenames into `list.txt`

If there is a `namelistfile`, instead, it will use the list of filenames from `namelistfile` to name the created files accordingly.

## Additional information about IDX and IMG format

The IDX file name should have less than 20 characters

The IDX file is : a bunch of FILEINFO structure, like this :

    i32 fileoffset (offset to the file start in the .IMG)
    i32 filesize (compressed size)
    i32 realsize (uncompressed size)
    i16 gtoverlap (GT compression overlap value)
    i16 duplicateid (duplicate files chain ID --> linked list, 
        follow it until a duplicateid=0 and you have the original)

For Rollcage, the IDX file contains the following file handles (hardcoded in the source code) - if you see $i, it means a number starting at 1 and incrementing.

    0-1        fe_blp, fe_btp
    2-3        fenoz_blp, fenoz_btp
    4-7        no idea
    8          fontpc_rdx
    9          font_bmp
    10-17      load$i_bmp (8 total)
    18         testlvl_bsp
    19         ice_bmp
    20-27      tw$i_bmp
    28         oem_dat
    29         logo_bmp
    30-69      low__lev$i_blp, low__lev$i_btp (20 total)
    70-109     high__lev$i_blp, high__lev$i_btp
    110-149    low__lev$i_bl6, low__lev$i_bt6
    150-189    high__lev$i_bl6, high__lev$i_bt6
    190-211    charanim__amer$i_bmp (22 total)
    212-233    charanim__fren$i_bmp
    234-255    charanim__germ$i_bmp
    256-277    charanim__ital$i_bmp
    278-299    charanim__japg$i_bmp
    300-321    charanim__poli$i_bmp
    322-343    charanim__ukby$i_bmp


As for the tracks `high__lev$i_blp` and such, here is a track list based on the value of `$i`.
They are grouped per league, then the bonus tracks.

    1 : Paradise
    2 : G-Force
    3 : Contact
    4 : Daytona
    5 : Park Life
    6 : Frontier
    7 : Smugglers
    8 : Eruption
    9 : Area 52
    10 : Cross Over
    11 : Skid Pan
    12 : After Shock
    13 : Flood Zone
    14 : Road Works
    15 : Super Bowl
    16 : Neoto Deathmatch
    17 : Saphire Deathmatch
    18 : Harpoon Deathmatch
    19 : Outworld Deathmatch
    20 : Training Track

# rollcage-and-ducati-world-dc-tools-and-modding

A collection of various tools for modding Rollcage, Rollcage Stage II, Ducati World (DC) (and maybe firebugs). Tools for extracting and modifying .IMG and .IDX files. Created by the rollcage community.

Look at the release page for windows binary of IDXTool
https://gitlab.com/megasaturnv/rollcage-and-ducati-world-dc-tools-and-modding/-/releases

Big thanks to the following people:
* orgyia/pocoyo - Blender 1.7x Addons/io_pinp/ 
* Spont and Slinger @ forum.recaged.net - Extracting/Tools/IDXTool/
* Rollcage / ATD community (anyone I may have missed)

Other useful links/resources:
* Broscar's files https://stack.broscar.nl/s/RDkNl36akdxkJxxx (Thank you Broscar)
* Rollcage Archive https://rollcage.broscar.nl/ (Thank you Broscar)
* Rollcage Stage 2 website images: https://imgur.com/a/WHDEn (Thank you potterman28wxcv)
